# CMake generated Testfile for 
# Source directory: /afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs
# Build directory: /afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("HbbISR")
subdirs("ZprimeDM")
subdirs("ZprimeFilelists")
subdirs("xAODAnaHelpers")
