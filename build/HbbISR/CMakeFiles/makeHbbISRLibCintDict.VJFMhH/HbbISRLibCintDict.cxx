// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME HbbISRLibCintDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "HbbISR/HbbHists.h"
#include "HbbISR/HbbISRBtagHistsAlgo.h"
#include "HbbISR/HbbISREffAlgo.h"
#include "HbbISR/HbbISRFatJetHistsAlgo.h"
#include "HbbISR/HbbISRHelperClasses.h"
#include "HbbISR/HbbISRHists.h"
#include "HbbISR/HbbISRHistsBaseAlgo.h"
#include "HbbISR/HbbISRLeadJetHistsAlgo.h"
#include "HbbISR/HbbISRMinTau21HistsAlgo.h"
#include "HbbISR/HbbISRTJetHistsAlgo.h"
#include "HbbISR/HbbISRTightLooseHistsAlgo.h"
#include "HbbISR/HbbISRTruthHistsAlgo.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void delete_HbbISRHistsBaseAlgo(void *p);
   static void deleteArray_HbbISRHistsBaseAlgo(void *p);
   static void destruct_HbbISRHistsBaseAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISRHistsBaseAlgo*)
   {
      ::HbbISRHistsBaseAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISRHistsBaseAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISRHistsBaseAlgo", ::HbbISRHistsBaseAlgo::Class_Version(), "HbbISR/HbbISRHistsBaseAlgo.h", 22,
                  typeid(::HbbISRHistsBaseAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISRHistsBaseAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISRHistsBaseAlgo) );
      instance.SetDelete(&delete_HbbISRHistsBaseAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISRHistsBaseAlgo);
      instance.SetDestructor(&destruct_HbbISRHistsBaseAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISRHistsBaseAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISRHistsBaseAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISRHistsBaseAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HbbISRBtagHistsAlgo(void *p = 0);
   static void *newArray_HbbISRBtagHistsAlgo(Long_t size, void *p);
   static void delete_HbbISRBtagHistsAlgo(void *p);
   static void deleteArray_HbbISRBtagHistsAlgo(void *p);
   static void destruct_HbbISRBtagHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISRBtagHistsAlgo*)
   {
      ::HbbISRBtagHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISRBtagHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISRBtagHistsAlgo", ::HbbISRBtagHistsAlgo::Class_Version(), "HbbISR/HbbISRBtagHistsAlgo.h", 7,
                  typeid(::HbbISRBtagHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISRBtagHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISRBtagHistsAlgo) );
      instance.SetNew(&new_HbbISRBtagHistsAlgo);
      instance.SetNewArray(&newArray_HbbISRBtagHistsAlgo);
      instance.SetDelete(&delete_HbbISRBtagHistsAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISRBtagHistsAlgo);
      instance.SetDestructor(&destruct_HbbISRBtagHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISRBtagHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISRBtagHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISRBtagHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HbbISRTJetHistsAlgo(void *p = 0);
   static void *newArray_HbbISRTJetHistsAlgo(Long_t size, void *p);
   static void delete_HbbISRTJetHistsAlgo(void *p);
   static void deleteArray_HbbISRTJetHistsAlgo(void *p);
   static void destruct_HbbISRTJetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISRTJetHistsAlgo*)
   {
      ::HbbISRTJetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISRTJetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISRTJetHistsAlgo", ::HbbISRTJetHistsAlgo::Class_Version(), "HbbISR/HbbISRTJetHistsAlgo.h", 7,
                  typeid(::HbbISRTJetHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISRTJetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISRTJetHistsAlgo) );
      instance.SetNew(&new_HbbISRTJetHistsAlgo);
      instance.SetNewArray(&newArray_HbbISRTJetHistsAlgo);
      instance.SetDelete(&delete_HbbISRTJetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISRTJetHistsAlgo);
      instance.SetDestructor(&destruct_HbbISRTJetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISRTJetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISRTJetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISRTJetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HbbISRTightLooseHistsAlgo(void *p = 0);
   static void *newArray_HbbISRTightLooseHistsAlgo(Long_t size, void *p);
   static void delete_HbbISRTightLooseHistsAlgo(void *p);
   static void deleteArray_HbbISRTightLooseHistsAlgo(void *p);
   static void destruct_HbbISRTightLooseHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISRTightLooseHistsAlgo*)
   {
      ::HbbISRTightLooseHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISRTightLooseHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISRTightLooseHistsAlgo", ::HbbISRTightLooseHistsAlgo::Class_Version(), "HbbISR/HbbISRTightLooseHistsAlgo.h", 7,
                  typeid(::HbbISRTightLooseHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISRTightLooseHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISRTightLooseHistsAlgo) );
      instance.SetNew(&new_HbbISRTightLooseHistsAlgo);
      instance.SetNewArray(&newArray_HbbISRTightLooseHistsAlgo);
      instance.SetDelete(&delete_HbbISRTightLooseHistsAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISRTightLooseHistsAlgo);
      instance.SetDestructor(&destruct_HbbISRTightLooseHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISRTightLooseHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISRTightLooseHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISRTightLooseHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HbbISRMinTau21HistsAlgo(void *p);
   static void deleteArray_HbbISRMinTau21HistsAlgo(void *p);
   static void destruct_HbbISRMinTau21HistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISRMinTau21HistsAlgo*)
   {
      ::HbbISRMinTau21HistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISRMinTau21HistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISRMinTau21HistsAlgo", ::HbbISRMinTau21HistsAlgo::Class_Version(), "HbbISR/HbbISRMinTau21HistsAlgo.h", 7,
                  typeid(::HbbISRMinTau21HistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISRMinTau21HistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISRMinTau21HistsAlgo) );
      instance.SetDelete(&delete_HbbISRMinTau21HistsAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISRMinTau21HistsAlgo);
      instance.SetDestructor(&destruct_HbbISRMinTau21HistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISRMinTau21HistsAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISRMinTau21HistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISRMinTau21HistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HbbISRLeadJetHistsAlgo(void *p = 0);
   static void *newArray_HbbISRLeadJetHistsAlgo(Long_t size, void *p);
   static void delete_HbbISRLeadJetHistsAlgo(void *p);
   static void deleteArray_HbbISRLeadJetHistsAlgo(void *p);
   static void destruct_HbbISRLeadJetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISRLeadJetHistsAlgo*)
   {
      ::HbbISRLeadJetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISRLeadJetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISRLeadJetHistsAlgo", ::HbbISRLeadJetHistsAlgo::Class_Version(), "HbbISR/HbbISRLeadJetHistsAlgo.h", 7,
                  typeid(::HbbISRLeadJetHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISRLeadJetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISRLeadJetHistsAlgo) );
      instance.SetNew(&new_HbbISRLeadJetHistsAlgo);
      instance.SetNewArray(&newArray_HbbISRLeadJetHistsAlgo);
      instance.SetDelete(&delete_HbbISRLeadJetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISRLeadJetHistsAlgo);
      instance.SetDestructor(&destruct_HbbISRLeadJetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISRLeadJetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISRLeadJetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISRLeadJetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HbbISRFatJetHistsAlgo(void *p = 0);
   static void *newArray_HbbISRFatJetHistsAlgo(Long_t size, void *p);
   static void delete_HbbISRFatJetHistsAlgo(void *p);
   static void deleteArray_HbbISRFatJetHistsAlgo(void *p);
   static void destruct_HbbISRFatJetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISRFatJetHistsAlgo*)
   {
      ::HbbISRFatJetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISRFatJetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISRFatJetHistsAlgo", ::HbbISRFatJetHistsAlgo::Class_Version(), "HbbISR/HbbISRFatJetHistsAlgo.h", 21,
                  typeid(::HbbISRFatJetHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISRFatJetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISRFatJetHistsAlgo) );
      instance.SetNew(&new_HbbISRFatJetHistsAlgo);
      instance.SetNewArray(&newArray_HbbISRFatJetHistsAlgo);
      instance.SetDelete(&delete_HbbISRFatJetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISRFatJetHistsAlgo);
      instance.SetDestructor(&destruct_HbbISRFatJetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISRFatJetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISRFatJetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISRFatJetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HbbISRTruthHistsAlgo(void *p = 0);
   static void *newArray_HbbISRTruthHistsAlgo(Long_t size, void *p);
   static void delete_HbbISRTruthHistsAlgo(void *p);
   static void deleteArray_HbbISRTruthHistsAlgo(void *p);
   static void destruct_HbbISRTruthHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISRTruthHistsAlgo*)
   {
      ::HbbISRTruthHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISRTruthHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISRTruthHistsAlgo", ::HbbISRTruthHistsAlgo::Class_Version(), "HbbISR/HbbISRTruthHistsAlgo.h", 7,
                  typeid(::HbbISRTruthHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISRTruthHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISRTruthHistsAlgo) );
      instance.SetNew(&new_HbbISRTruthHistsAlgo);
      instance.SetNewArray(&newArray_HbbISRTruthHistsAlgo);
      instance.SetDelete(&delete_HbbISRTruthHistsAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISRTruthHistsAlgo);
      instance.SetDestructor(&destruct_HbbISRTruthHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISRTruthHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISRTruthHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISRTruthHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HbbISREffAlgo(void *p = 0);
   static void *newArray_HbbISREffAlgo(Long_t size, void *p);
   static void delete_HbbISREffAlgo(void *p);
   static void deleteArray_HbbISREffAlgo(void *p);
   static void destruct_HbbISREffAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HbbISREffAlgo*)
   {
      ::HbbISREffAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HbbISREffAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HbbISREffAlgo", ::HbbISREffAlgo::Class_Version(), "HbbISR/HbbISREffAlgo.h", 20,
                  typeid(::HbbISREffAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HbbISREffAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::HbbISREffAlgo) );
      instance.SetNew(&new_HbbISREffAlgo);
      instance.SetNewArray(&newArray_HbbISREffAlgo);
      instance.SetDelete(&delete_HbbISREffAlgo);
      instance.SetDeleteArray(&deleteArray_HbbISREffAlgo);
      instance.SetDestructor(&destruct_HbbISREffAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HbbISREffAlgo*)
   {
      return GenerateInitInstanceLocal((::HbbISREffAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HbbISREffAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr HbbISRHistsBaseAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISRHistsBaseAlgo::Class_Name()
{
   return "HbbISRHistsBaseAlgo";
}

//______________________________________________________________________________
const char *HbbISRHistsBaseAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRHistsBaseAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISRHistsBaseAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRHistsBaseAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISRHistsBaseAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRHistsBaseAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISRHistsBaseAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRHistsBaseAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HbbISRBtagHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISRBtagHistsAlgo::Class_Name()
{
   return "HbbISRBtagHistsAlgo";
}

//______________________________________________________________________________
const char *HbbISRBtagHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRBtagHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISRBtagHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRBtagHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISRBtagHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRBtagHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISRBtagHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRBtagHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HbbISRTJetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISRTJetHistsAlgo::Class_Name()
{
   return "HbbISRTJetHistsAlgo";
}

//______________________________________________________________________________
const char *HbbISRTJetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTJetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISRTJetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTJetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISRTJetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTJetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISRTJetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTJetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HbbISRTightLooseHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISRTightLooseHistsAlgo::Class_Name()
{
   return "HbbISRTightLooseHistsAlgo";
}

//______________________________________________________________________________
const char *HbbISRTightLooseHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTightLooseHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISRTightLooseHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTightLooseHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISRTightLooseHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTightLooseHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISRTightLooseHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTightLooseHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HbbISRMinTau21HistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISRMinTau21HistsAlgo::Class_Name()
{
   return "HbbISRMinTau21HistsAlgo";
}

//______________________________________________________________________________
const char *HbbISRMinTau21HistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRMinTau21HistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISRMinTau21HistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRMinTau21HistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISRMinTau21HistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRMinTau21HistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISRMinTau21HistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRMinTau21HistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HbbISRLeadJetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISRLeadJetHistsAlgo::Class_Name()
{
   return "HbbISRLeadJetHistsAlgo";
}

//______________________________________________________________________________
const char *HbbISRLeadJetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRLeadJetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISRLeadJetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRLeadJetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISRLeadJetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRLeadJetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISRLeadJetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRLeadJetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HbbISRFatJetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISRFatJetHistsAlgo::Class_Name()
{
   return "HbbISRFatJetHistsAlgo";
}

//______________________________________________________________________________
const char *HbbISRFatJetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRFatJetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISRFatJetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRFatJetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISRFatJetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRFatJetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISRFatJetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRFatJetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HbbISRTruthHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISRTruthHistsAlgo::Class_Name()
{
   return "HbbISRTruthHistsAlgo";
}

//______________________________________________________________________________
const char *HbbISRTruthHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTruthHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISRTruthHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTruthHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISRTruthHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTruthHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISRTruthHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISRTruthHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HbbISREffAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HbbISREffAlgo::Class_Name()
{
   return "HbbISREffAlgo";
}

//______________________________________________________________________________
const char *HbbISREffAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISREffAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HbbISREffAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HbbISREffAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HbbISREffAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISREffAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HbbISREffAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HbbISREffAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void HbbISRHistsBaseAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISRHistsBaseAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISRHistsBaseAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISRHistsBaseAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HbbISRHistsBaseAlgo(void *p) {
      delete ((::HbbISRHistsBaseAlgo*)p);
   }
   static void deleteArray_HbbISRHistsBaseAlgo(void *p) {
      delete [] ((::HbbISRHistsBaseAlgo*)p);
   }
   static void destruct_HbbISRHistsBaseAlgo(void *p) {
      typedef ::HbbISRHistsBaseAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISRHistsBaseAlgo

//______________________________________________________________________________
void HbbISRBtagHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISRBtagHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISRBtagHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISRBtagHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HbbISRBtagHistsAlgo(void *p) {
      return  p ? new(p) ::HbbISRBtagHistsAlgo : new ::HbbISRBtagHistsAlgo;
   }
   static void *newArray_HbbISRBtagHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::HbbISRBtagHistsAlgo[nElements] : new ::HbbISRBtagHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_HbbISRBtagHistsAlgo(void *p) {
      delete ((::HbbISRBtagHistsAlgo*)p);
   }
   static void deleteArray_HbbISRBtagHistsAlgo(void *p) {
      delete [] ((::HbbISRBtagHistsAlgo*)p);
   }
   static void destruct_HbbISRBtagHistsAlgo(void *p) {
      typedef ::HbbISRBtagHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISRBtagHistsAlgo

//______________________________________________________________________________
void HbbISRTJetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISRTJetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISRTJetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISRTJetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HbbISRTJetHistsAlgo(void *p) {
      return  p ? new(p) ::HbbISRTJetHistsAlgo : new ::HbbISRTJetHistsAlgo;
   }
   static void *newArray_HbbISRTJetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::HbbISRTJetHistsAlgo[nElements] : new ::HbbISRTJetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_HbbISRTJetHistsAlgo(void *p) {
      delete ((::HbbISRTJetHistsAlgo*)p);
   }
   static void deleteArray_HbbISRTJetHistsAlgo(void *p) {
      delete [] ((::HbbISRTJetHistsAlgo*)p);
   }
   static void destruct_HbbISRTJetHistsAlgo(void *p) {
      typedef ::HbbISRTJetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISRTJetHistsAlgo

//______________________________________________________________________________
void HbbISRTightLooseHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISRTightLooseHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISRTightLooseHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISRTightLooseHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HbbISRTightLooseHistsAlgo(void *p) {
      return  p ? new(p) ::HbbISRTightLooseHistsAlgo : new ::HbbISRTightLooseHistsAlgo;
   }
   static void *newArray_HbbISRTightLooseHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::HbbISRTightLooseHistsAlgo[nElements] : new ::HbbISRTightLooseHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_HbbISRTightLooseHistsAlgo(void *p) {
      delete ((::HbbISRTightLooseHistsAlgo*)p);
   }
   static void deleteArray_HbbISRTightLooseHistsAlgo(void *p) {
      delete [] ((::HbbISRTightLooseHistsAlgo*)p);
   }
   static void destruct_HbbISRTightLooseHistsAlgo(void *p) {
      typedef ::HbbISRTightLooseHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISRTightLooseHistsAlgo

//______________________________________________________________________________
void HbbISRMinTau21HistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISRMinTau21HistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISRMinTau21HistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISRMinTau21HistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HbbISRMinTau21HistsAlgo(void *p) {
      delete ((::HbbISRMinTau21HistsAlgo*)p);
   }
   static void deleteArray_HbbISRMinTau21HistsAlgo(void *p) {
      delete [] ((::HbbISRMinTau21HistsAlgo*)p);
   }
   static void destruct_HbbISRMinTau21HistsAlgo(void *p) {
      typedef ::HbbISRMinTau21HistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISRMinTau21HistsAlgo

//______________________________________________________________________________
void HbbISRLeadJetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISRLeadJetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISRLeadJetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISRLeadJetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HbbISRLeadJetHistsAlgo(void *p) {
      return  p ? new(p) ::HbbISRLeadJetHistsAlgo : new ::HbbISRLeadJetHistsAlgo;
   }
   static void *newArray_HbbISRLeadJetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::HbbISRLeadJetHistsAlgo[nElements] : new ::HbbISRLeadJetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_HbbISRLeadJetHistsAlgo(void *p) {
      delete ((::HbbISRLeadJetHistsAlgo*)p);
   }
   static void deleteArray_HbbISRLeadJetHistsAlgo(void *p) {
      delete [] ((::HbbISRLeadJetHistsAlgo*)p);
   }
   static void destruct_HbbISRLeadJetHistsAlgo(void *p) {
      typedef ::HbbISRLeadJetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISRLeadJetHistsAlgo

//______________________________________________________________________________
void HbbISRFatJetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISRFatJetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISRFatJetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISRFatJetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HbbISRFatJetHistsAlgo(void *p) {
      return  p ? new(p) ::HbbISRFatJetHistsAlgo : new ::HbbISRFatJetHistsAlgo;
   }
   static void *newArray_HbbISRFatJetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::HbbISRFatJetHistsAlgo[nElements] : new ::HbbISRFatJetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_HbbISRFatJetHistsAlgo(void *p) {
      delete ((::HbbISRFatJetHistsAlgo*)p);
   }
   static void deleteArray_HbbISRFatJetHistsAlgo(void *p) {
      delete [] ((::HbbISRFatJetHistsAlgo*)p);
   }
   static void destruct_HbbISRFatJetHistsAlgo(void *p) {
      typedef ::HbbISRFatJetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISRFatJetHistsAlgo

//______________________________________________________________________________
void HbbISRTruthHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISRTruthHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISRTruthHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISRTruthHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HbbISRTruthHistsAlgo(void *p) {
      return  p ? new(p) ::HbbISRTruthHistsAlgo : new ::HbbISRTruthHistsAlgo;
   }
   static void *newArray_HbbISRTruthHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::HbbISRTruthHistsAlgo[nElements] : new ::HbbISRTruthHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_HbbISRTruthHistsAlgo(void *p) {
      delete ((::HbbISRTruthHistsAlgo*)p);
   }
   static void deleteArray_HbbISRTruthHistsAlgo(void *p) {
      delete [] ((::HbbISRTruthHistsAlgo*)p);
   }
   static void destruct_HbbISRTruthHistsAlgo(void *p) {
      typedef ::HbbISRTruthHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISRTruthHistsAlgo

//______________________________________________________________________________
void HbbISREffAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class HbbISREffAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HbbISREffAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(HbbISREffAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HbbISREffAlgo(void *p) {
      return  p ? new(p) ::HbbISREffAlgo : new ::HbbISREffAlgo;
   }
   static void *newArray_HbbISREffAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::HbbISREffAlgo[nElements] : new ::HbbISREffAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_HbbISREffAlgo(void *p) {
      delete ((::HbbISREffAlgo*)p);
   }
   static void deleteArray_HbbISREffAlgo(void *p) {
      delete [] ((::HbbISREffAlgo*)p);
   }
   static void destruct_HbbISREffAlgo(void *p) {
      typedef ::HbbISREffAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HbbISREffAlgo

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 339,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace {
  void TriggerDictionaryInitialization_libHbbISRLib_Impl() {
    static const char* headers[] = {
"HbbISR/HbbHists.h",
"HbbISR/HbbISRBtagHistsAlgo.h",
"HbbISR/HbbISREffAlgo.h",
"HbbISR/HbbISRFatJetHistsAlgo.h",
"HbbISR/HbbISRHelperClasses.h",
"HbbISR/HbbISRHists.h",
"HbbISR/HbbISRHistsBaseAlgo.h",
"HbbISR/HbbISRLeadJetHistsAlgo.h",
"HbbISR/HbbISRMinTau21HistsAlgo.h",
"HbbISR/HbbISRTJetHistsAlgo.h",
"HbbISR/HbbISRTightLooseHistsAlgo.h",
"HbbISR/HbbISRTruthHistsAlgo.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/HbbISR",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/HbbISR",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/xAODAnaHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCutFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTriggerCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/AssociationUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/IsolationSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PMGTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/IsolationCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonShowerShapeFudgeTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetMissingEtID/JetSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/JetAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/TauID/TauAnalysisTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/tauRecTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetAnalysisTools/JetTileCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetCPInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetCalibTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetJvtEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetMomentTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetEDM",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetRec",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/EventShapes/EventShapeInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetResolution",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetSubStructureUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetUncertainties",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MET/METInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MET/METUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeFilelists",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/HbbISR",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/HbbISR",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/xAODAnaHelpers",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/xAODAnaHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetCalibTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetMissingEtID/JetSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/egamma/egammaMVACalib",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODHIEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/IsolationSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/IsolationCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonShowerShapeFudgeTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/JetAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MET/METUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetResolution",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/tauRecTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MET/METInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/TauID/TauAnalysisTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/AssociationUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetEDM",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetUncertainties",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetCPInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCutFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetMomentTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetRec",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/EventShapes/EventShapeInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetJvtEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PMGTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetSubStructureUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetAnalysisTools/JetTileCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/BoostedJetTaggers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTriggerCnv",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeFilelists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/HbbISR/CMakeFiles/makeHbbISRLibCintDict.VJFMhH/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libHbbISRLib dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISRHistsBaseAlgo.h")))  HbbISRHistsBaseAlgo;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISRBtagHistsAlgo.h")))  HbbISRBtagHistsAlgo;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISRTJetHistsAlgo.h")))  HbbISRTJetHistsAlgo;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISRTightLooseHistsAlgo.h")))  HbbISRTightLooseHistsAlgo;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISRMinTau21HistsAlgo.h")))  HbbISRMinTau21HistsAlgo;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISRLeadJetHistsAlgo.h")))  HbbISRLeadJetHistsAlgo;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISRFatJetHistsAlgo.h")))  HbbISRFatJetHistsAlgo;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISRTruthHistsAlgo.h")))  HbbISRTruthHistsAlgo;
class __attribute__((annotate("$clingAutoload$HbbISR/HbbISREffAlgo.h")))  HbbISREffAlgo;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libHbbISRLib dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "HbbISR-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ HbbISR-00-00-00
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif
#ifndef NO_SHOWERDECONSTRUCTION
  #define NO_SHOWERDECONSTRUCTION 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "HbbISR/HbbHists.h"
#include "HbbISR/HbbISRBtagHistsAlgo.h"
#include "HbbISR/HbbISREffAlgo.h"
#include "HbbISR/HbbISRFatJetHistsAlgo.h"
#include "HbbISR/HbbISRHelperClasses.h"
#include "HbbISR/HbbISRHists.h"
#include "HbbISR/HbbISRHistsBaseAlgo.h"
#include "HbbISR/HbbISRLeadJetHistsAlgo.h"
#include "HbbISR/HbbISRMinTau21HistsAlgo.h"
#include "HbbISR/HbbISRTJetHistsAlgo.h"
#include "HbbISR/HbbISRTightLooseHistsAlgo.h"
#include "HbbISR/HbbISRTruthHistsAlgo.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"HbbISRBtagHistsAlgo", payloadCode, "@",
"HbbISREffAlgo", payloadCode, "@",
"HbbISRFatJetHistsAlgo", payloadCode, "@",
"HbbISRHistsBaseAlgo", payloadCode, "@",
"HbbISRLeadJetHistsAlgo", payloadCode, "@",
"HbbISRMinTau21HistsAlgo", payloadCode, "@",
"HbbISRTJetHistsAlgo", payloadCode, "@",
"HbbISRTightLooseHistsAlgo", payloadCode, "@",
"HbbISRTruthHistsAlgo", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libHbbISRLib",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libHbbISRLib_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libHbbISRLib_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libHbbISRLib() {
  TriggerDictionaryInitialization_libHbbISRLib_Impl();
}
