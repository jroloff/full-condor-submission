// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME ZprimeDMLibCintDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "ZprimeDM/BCIDBugChecker.h"
#include "ZprimeDM/CutflowHists.h"
#include "ZprimeDM/DebugHists.h"
#include "ZprimeDM/DijetISREvent.h"
#include "ZprimeDM/DijetISRHists.h"
#include "ZprimeDM/DiphotonHists.h"
#include "ZprimeDM/DiphotonHistsAlgo.h"
#include "ZprimeDM/ElectronHists.h"
#include "ZprimeDM/EventHists.h"
#include "ZprimeDM/FatJetHists.h"
#include "ZprimeDM/GammaJetHists.h"
#include "ZprimeDM/GammaJetHistsAlgo.h"
#include "ZprimeDM/JMRUncertaintyAlgo.h"
#include "ZprimeDM/JetHists.h"
#include "ZprimeDM/JetPartonHistsAlgo.h"
#include "ZprimeDM/JetStudiesAlgo.h"
#include "ZprimeDM/MiniTreeEventSelection.h"
#include "ZprimeDM/MuonHists.h"
#include "ZprimeDM/PartonJetHists.h"
#include "ZprimeDM/PhotonHists.h"
#include "ZprimeDM/PhotonTriggerHists.h"
#include "ZprimeDM/ProcessTrigStudy.h"
#include "ZprimeDM/ResonanceHists.h"
#include "ZprimeDM/SortAlgo.h"
#include "ZprimeDM/TruthHists.h"
#include "ZprimeDM/TruthPhotonJetOR.h"
#include "ZprimeDM/ZprimeAlgorithm.h"
#include "ZprimeDM/ZprimeDijetHistsAlgo.h"
#include "ZprimeDM/ZprimeGammaJetJetHistsAlgo.h"
#include "ZprimeDM/ZprimeHelperClasses.h"
#include "ZprimeDM/ZprimeHistsBaseAlgo.h"
#include "ZprimeDM/ZprimeISRBoostHists.h"
#include "ZprimeDM/ZprimeISRBoostHistsAlgo.h"
#include "ZprimeDM/ZprimeISRHists.h"
#include "ZprimeDM/ZprimeJetHistsAlgo.h"
#include "ZprimeDM/ZprimeMGTrijet2DHists.h"
#include "ZprimeDM/ZprimeMGTrijetAlgo.h"
#include "ZprimeDM/ZprimeMGTrijetHists.h"
#include "ZprimeDM/ZprimeMGTrijetPickAlgo.h"
#include "ZprimeDM/ZprimeMiniTree.h"
#include "ZprimeDM/ZprimeNtupler.h"
#include "ZprimeDM/ZprimeNtuplerContainer.h"
#include "ZprimeDM/ZprimeResonanceHists.h"
#include "ZprimeDM/ZprimeTrijetHistsAlgo.h"
#include "ZprimeDM/ZprimeTruthHists.h"
#include "ZprimeDM/ZprimeTruthResonanceHists.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void delete_ZprimeAlgorithm(void *p);
   static void deleteArray_ZprimeAlgorithm(void *p);
   static void destruct_ZprimeAlgorithm(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeAlgorithm*)
   {
      ::ZprimeAlgorithm *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeAlgorithm >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeAlgorithm", ::ZprimeAlgorithm::Class_Version(), "ZprimeDM/ZprimeAlgorithm.h", 9,
                  typeid(::ZprimeAlgorithm), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeAlgorithm::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeAlgorithm) );
      instance.SetDelete(&delete_ZprimeAlgorithm);
      instance.SetDeleteArray(&deleteArray_ZprimeAlgorithm);
      instance.SetDestructor(&destruct_ZprimeAlgorithm);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeAlgorithm*)
   {
      return GenerateInitInstanceLocal((::ZprimeAlgorithm*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeAlgorithm*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TruthPhotonJetOR(void *p = 0);
   static void *newArray_TruthPhotonJetOR(Long_t size, void *p);
   static void delete_TruthPhotonJetOR(void *p);
   static void deleteArray_TruthPhotonJetOR(void *p);
   static void destruct_TruthPhotonJetOR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TruthPhotonJetOR*)
   {
      ::TruthPhotonJetOR *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TruthPhotonJetOR >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TruthPhotonJetOR", ::TruthPhotonJetOR::Class_Version(), "ZprimeDM/TruthPhotonJetOR.h", 9,
                  typeid(::TruthPhotonJetOR), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TruthPhotonJetOR::Dictionary, isa_proxy, 4,
                  sizeof(::TruthPhotonJetOR) );
      instance.SetNew(&new_TruthPhotonJetOR);
      instance.SetNewArray(&newArray_TruthPhotonJetOR);
      instance.SetDelete(&delete_TruthPhotonJetOR);
      instance.SetDeleteArray(&deleteArray_TruthPhotonJetOR);
      instance.SetDestructor(&destruct_TruthPhotonJetOR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TruthPhotonJetOR*)
   {
      return GenerateInitInstanceLocal((::TruthPhotonJetOR*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TruthPhotonJetOR*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetPartonHistsAlgo(void *p = 0);
   static void *newArray_JetPartonHistsAlgo(Long_t size, void *p);
   static void delete_JetPartonHistsAlgo(void *p);
   static void deleteArray_JetPartonHistsAlgo(void *p);
   static void destruct_JetPartonHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetPartonHistsAlgo*)
   {
      ::JetPartonHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetPartonHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetPartonHistsAlgo", ::JetPartonHistsAlgo::Class_Version(), "ZprimeDM/JetPartonHistsAlgo.h", 14,
                  typeid(::JetPartonHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::JetPartonHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JetPartonHistsAlgo) );
      instance.SetNew(&new_JetPartonHistsAlgo);
      instance.SetNewArray(&newArray_JetPartonHistsAlgo);
      instance.SetDelete(&delete_JetPartonHistsAlgo);
      instance.SetDeleteArray(&deleteArray_JetPartonHistsAlgo);
      instance.SetDestructor(&destruct_JetPartonHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetPartonHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::JetPartonHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::JetPartonHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ZprimeMGTrijetAlgo(void *p = 0);
   static void *newArray_ZprimeMGTrijetAlgo(Long_t size, void *p);
   static void delete_ZprimeMGTrijetAlgo(void *p);
   static void deleteArray_ZprimeMGTrijetAlgo(void *p);
   static void destruct_ZprimeMGTrijetAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeMGTrijetAlgo*)
   {
      ::ZprimeMGTrijetAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeMGTrijetAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeMGTrijetAlgo", ::ZprimeMGTrijetAlgo::Class_Version(), "ZprimeDM/ZprimeMGTrijetAlgo.h", 25,
                  typeid(::ZprimeMGTrijetAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeMGTrijetAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeMGTrijetAlgo) );
      instance.SetNew(&new_ZprimeMGTrijetAlgo);
      instance.SetNewArray(&newArray_ZprimeMGTrijetAlgo);
      instance.SetDelete(&delete_ZprimeMGTrijetAlgo);
      instance.SetDeleteArray(&deleteArray_ZprimeMGTrijetAlgo);
      instance.SetDestructor(&destruct_ZprimeMGTrijetAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeMGTrijetAlgo*)
   {
      return GenerateInitInstanceLocal((::ZprimeMGTrijetAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeMGTrijetAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ZprimeMGTrijetPickAlgo(void *p = 0);
   static void *newArray_ZprimeMGTrijetPickAlgo(Long_t size, void *p);
   static void delete_ZprimeMGTrijetPickAlgo(void *p);
   static void deleteArray_ZprimeMGTrijetPickAlgo(void *p);
   static void destruct_ZprimeMGTrijetPickAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeMGTrijetPickAlgo*)
   {
      ::ZprimeMGTrijetPickAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeMGTrijetPickAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeMGTrijetPickAlgo", ::ZprimeMGTrijetPickAlgo::Class_Version(), "ZprimeDM/ZprimeMGTrijetPickAlgo.h", 22,
                  typeid(::ZprimeMGTrijetPickAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeMGTrijetPickAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeMGTrijetPickAlgo) );
      instance.SetNew(&new_ZprimeMGTrijetPickAlgo);
      instance.SetNewArray(&newArray_ZprimeMGTrijetPickAlgo);
      instance.SetDelete(&delete_ZprimeMGTrijetPickAlgo);
      instance.SetDeleteArray(&deleteArray_ZprimeMGTrijetPickAlgo);
      instance.SetDestructor(&destruct_ZprimeMGTrijetPickAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeMGTrijetPickAlgo*)
   {
      return GenerateInitInstanceLocal((::ZprimeMGTrijetPickAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeMGTrijetPickAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_ZprimeHistsBaseAlgo(void *p);
   static void deleteArray_ZprimeHistsBaseAlgo(void *p);
   static void destruct_ZprimeHistsBaseAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeHistsBaseAlgo*)
   {
      ::ZprimeHistsBaseAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeHistsBaseAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeHistsBaseAlgo", ::ZprimeHistsBaseAlgo::Class_Version(), "ZprimeDM/ZprimeHistsBaseAlgo.h", 32,
                  typeid(::ZprimeHistsBaseAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeHistsBaseAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeHistsBaseAlgo) );
      instance.SetDelete(&delete_ZprimeHistsBaseAlgo);
      instance.SetDeleteArray(&deleteArray_ZprimeHistsBaseAlgo);
      instance.SetDestructor(&destruct_ZprimeHistsBaseAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeHistsBaseAlgo*)
   {
      return GenerateInitInstanceLocal((::ZprimeHistsBaseAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeHistsBaseAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ZprimeGammaJetJetHistsAlgo(void *p = 0);
   static void *newArray_ZprimeGammaJetJetHistsAlgo(Long_t size, void *p);
   static void delete_ZprimeGammaJetJetHistsAlgo(void *p);
   static void deleteArray_ZprimeGammaJetJetHistsAlgo(void *p);
   static void destruct_ZprimeGammaJetJetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeGammaJetJetHistsAlgo*)
   {
      ::ZprimeGammaJetJetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeGammaJetJetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeGammaJetJetHistsAlgo", ::ZprimeGammaJetJetHistsAlgo::Class_Version(), "ZprimeDM/ZprimeGammaJetJetHistsAlgo.h", 9,
                  typeid(::ZprimeGammaJetJetHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeGammaJetJetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeGammaJetJetHistsAlgo) );
      instance.SetNew(&new_ZprimeGammaJetJetHistsAlgo);
      instance.SetNewArray(&newArray_ZprimeGammaJetJetHistsAlgo);
      instance.SetDelete(&delete_ZprimeGammaJetJetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_ZprimeGammaJetJetHistsAlgo);
      instance.SetDestructor(&destruct_ZprimeGammaJetJetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeGammaJetJetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::ZprimeGammaJetJetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeGammaJetJetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ZprimeDijetHistsAlgo(void *p = 0);
   static void *newArray_ZprimeDijetHistsAlgo(Long_t size, void *p);
   static void delete_ZprimeDijetHistsAlgo(void *p);
   static void deleteArray_ZprimeDijetHistsAlgo(void *p);
   static void destruct_ZprimeDijetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeDijetHistsAlgo*)
   {
      ::ZprimeDijetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeDijetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeDijetHistsAlgo", ::ZprimeDijetHistsAlgo::Class_Version(), "ZprimeDM/ZprimeDijetHistsAlgo.h", 7,
                  typeid(::ZprimeDijetHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeDijetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeDijetHistsAlgo) );
      instance.SetNew(&new_ZprimeDijetHistsAlgo);
      instance.SetNewArray(&newArray_ZprimeDijetHistsAlgo);
      instance.SetDelete(&delete_ZprimeDijetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_ZprimeDijetHistsAlgo);
      instance.SetDestructor(&destruct_ZprimeDijetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeDijetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::ZprimeDijetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeDijetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ZprimeTrijetHistsAlgo(void *p = 0);
   static void *newArray_ZprimeTrijetHistsAlgo(Long_t size, void *p);
   static void delete_ZprimeTrijetHistsAlgo(void *p);
   static void deleteArray_ZprimeTrijetHistsAlgo(void *p);
   static void destruct_ZprimeTrijetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeTrijetHistsAlgo*)
   {
      ::ZprimeTrijetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeTrijetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeTrijetHistsAlgo", ::ZprimeTrijetHistsAlgo::Class_Version(), "ZprimeDM/ZprimeTrijetHistsAlgo.h", 7,
                  typeid(::ZprimeTrijetHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeTrijetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeTrijetHistsAlgo) );
      instance.SetNew(&new_ZprimeTrijetHistsAlgo);
      instance.SetNewArray(&newArray_ZprimeTrijetHistsAlgo);
      instance.SetDelete(&delete_ZprimeTrijetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_ZprimeTrijetHistsAlgo);
      instance.SetDestructor(&destruct_ZprimeTrijetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeTrijetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::ZprimeTrijetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeTrijetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_DiphotonHistsAlgo(void *p = 0);
   static void *newArray_DiphotonHistsAlgo(Long_t size, void *p);
   static void delete_DiphotonHistsAlgo(void *p);
   static void deleteArray_DiphotonHistsAlgo(void *p);
   static void destruct_DiphotonHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DiphotonHistsAlgo*)
   {
      ::DiphotonHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::DiphotonHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("DiphotonHistsAlgo", ::DiphotonHistsAlgo::Class_Version(), "ZprimeDM/DiphotonHistsAlgo.h", 30,
                  typeid(::DiphotonHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::DiphotonHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::DiphotonHistsAlgo) );
      instance.SetNew(&new_DiphotonHistsAlgo);
      instance.SetNewArray(&newArray_DiphotonHistsAlgo);
      instance.SetDelete(&delete_DiphotonHistsAlgo);
      instance.SetDeleteArray(&deleteArray_DiphotonHistsAlgo);
      instance.SetDestructor(&destruct_DiphotonHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DiphotonHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::DiphotonHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DiphotonHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_GammaJetHistsAlgo(void *p = 0);
   static void *newArray_GammaJetHistsAlgo(Long_t size, void *p);
   static void delete_GammaJetHistsAlgo(void *p);
   static void deleteArray_GammaJetHistsAlgo(void *p);
   static void destruct_GammaJetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::GammaJetHistsAlgo*)
   {
      ::GammaJetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::GammaJetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("GammaJetHistsAlgo", ::GammaJetHistsAlgo::Class_Version(), "ZprimeDM/GammaJetHistsAlgo.h", 32,
                  typeid(::GammaJetHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::GammaJetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::GammaJetHistsAlgo) );
      instance.SetNew(&new_GammaJetHistsAlgo);
      instance.SetNewArray(&newArray_GammaJetHistsAlgo);
      instance.SetDelete(&delete_GammaJetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_GammaJetHistsAlgo);
      instance.SetDestructor(&destruct_GammaJetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::GammaJetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::GammaJetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::GammaJetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *ZprimeNtuplerContainer_Dictionary();
   static void ZprimeNtuplerContainer_TClassManip(TClass*);
   static void *new_ZprimeNtuplerContainer(void *p = 0);
   static void *newArray_ZprimeNtuplerContainer(Long_t size, void *p);
   static void delete_ZprimeNtuplerContainer(void *p);
   static void deleteArray_ZprimeNtuplerContainer(void *p);
   static void destruct_ZprimeNtuplerContainer(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeNtuplerContainer*)
   {
      ::ZprimeNtuplerContainer *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ZprimeNtuplerContainer));
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeNtuplerContainer", "ZprimeDM/ZprimeNtuplerContainer.h", 6,
                  typeid(::ZprimeNtuplerContainer), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ZprimeNtuplerContainer_Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeNtuplerContainer) );
      instance.SetNew(&new_ZprimeNtuplerContainer);
      instance.SetNewArray(&newArray_ZprimeNtuplerContainer);
      instance.SetDelete(&delete_ZprimeNtuplerContainer);
      instance.SetDeleteArray(&deleteArray_ZprimeNtuplerContainer);
      instance.SetDestructor(&destruct_ZprimeNtuplerContainer);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeNtuplerContainer*)
   {
      return GenerateInitInstanceLocal((::ZprimeNtuplerContainer*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeNtuplerContainer*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ZprimeNtuplerContainer_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ZprimeNtuplerContainer*)0x0)->GetClass();
      ZprimeNtuplerContainer_TClassManip(theClass);
   return theClass;
   }

   static void ZprimeNtuplerContainer_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_ZprimeNtupler(void *p = 0);
   static void *newArray_ZprimeNtupler(Long_t size, void *p);
   static void delete_ZprimeNtupler(void *p);
   static void deleteArray_ZprimeNtupler(void *p);
   static void destruct_ZprimeNtupler(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeNtupler*)
   {
      ::ZprimeNtupler *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeNtupler >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeNtupler", ::ZprimeNtupler::Class_Version(), "ZprimeDM/ZprimeNtupler.h", 18,
                  typeid(::ZprimeNtupler), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeNtupler::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeNtupler) );
      instance.SetNew(&new_ZprimeNtupler);
      instance.SetNewArray(&newArray_ZprimeNtupler);
      instance.SetDelete(&delete_ZprimeNtupler);
      instance.SetDeleteArray(&deleteArray_ZprimeNtupler);
      instance.SetDestructor(&destruct_ZprimeNtupler);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeNtupler*)
   {
      return GenerateInitInstanceLocal((::ZprimeNtupler*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeNtupler*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ZprimeJetHistsAlgo(void *p = 0);
   static void *newArray_ZprimeJetHistsAlgo(Long_t size, void *p);
   static void delete_ZprimeJetHistsAlgo(void *p);
   static void deleteArray_ZprimeJetHistsAlgo(void *p);
   static void destruct_ZprimeJetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeJetHistsAlgo*)
   {
      ::ZprimeJetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeJetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeJetHistsAlgo", ::ZprimeJetHistsAlgo::Class_Version(), "ZprimeDM/ZprimeJetHistsAlgo.h", 30,
                  typeid(::ZprimeJetHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeJetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeJetHistsAlgo) );
      instance.SetNew(&new_ZprimeJetHistsAlgo);
      instance.SetNewArray(&newArray_ZprimeJetHistsAlgo);
      instance.SetDelete(&delete_ZprimeJetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_ZprimeJetHistsAlgo);
      instance.SetDestructor(&destruct_ZprimeJetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeJetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::ZprimeJetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeJetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetStudiesAlgo(void *p = 0);
   static void *newArray_JetStudiesAlgo(Long_t size, void *p);
   static void delete_JetStudiesAlgo(void *p);
   static void deleteArray_JetStudiesAlgo(void *p);
   static void destruct_JetStudiesAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetStudiesAlgo*)
   {
      ::JetStudiesAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetStudiesAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetStudiesAlgo", ::JetStudiesAlgo::Class_Version(), "ZprimeDM/JetStudiesAlgo.h", 28,
                  typeid(::JetStudiesAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::JetStudiesAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JetStudiesAlgo) );
      instance.SetNew(&new_JetStudiesAlgo);
      instance.SetNewArray(&newArray_JetStudiesAlgo);
      instance.SetDelete(&delete_JetStudiesAlgo);
      instance.SetDeleteArray(&deleteArray_JetStudiesAlgo);
      instance.SetDestructor(&destruct_JetStudiesAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetStudiesAlgo*)
   {
      return GenerateInitInstanceLocal((::JetStudiesAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::JetStudiesAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_SortAlgo(void *p = 0);
   static void *newArray_SortAlgo(Long_t size, void *p);
   static void delete_SortAlgo(void *p);
   static void deleteArray_SortAlgo(void *p);
   static void destruct_SortAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SortAlgo*)
   {
      ::SortAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SortAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SortAlgo", ::SortAlgo::Class_Version(), "ZprimeDM/SortAlgo.h", 11,
                  typeid(::SortAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::SortAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::SortAlgo) );
      instance.SetNew(&new_SortAlgo);
      instance.SetNewArray(&newArray_SortAlgo);
      instance.SetDelete(&delete_SortAlgo);
      instance.SetDeleteArray(&deleteArray_SortAlgo);
      instance.SetDestructor(&destruct_SortAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SortAlgo*)
   {
      return GenerateInitInstanceLocal((::SortAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::SortAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MiniTreeEventSelection(void *p = 0);
   static void *newArray_MiniTreeEventSelection(Long_t size, void *p);
   static void delete_MiniTreeEventSelection(void *p);
   static void deleteArray_MiniTreeEventSelection(void *p);
   static void destruct_MiniTreeEventSelection(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MiniTreeEventSelection*)
   {
      ::MiniTreeEventSelection *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MiniTreeEventSelection >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MiniTreeEventSelection", ::MiniTreeEventSelection::Class_Version(), "ZprimeDM/MiniTreeEventSelection.h", 22,
                  typeid(::MiniTreeEventSelection), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::MiniTreeEventSelection::Dictionary, isa_proxy, 4,
                  sizeof(::MiniTreeEventSelection) );
      instance.SetNew(&new_MiniTreeEventSelection);
      instance.SetNewArray(&newArray_MiniTreeEventSelection);
      instance.SetDelete(&delete_MiniTreeEventSelection);
      instance.SetDeleteArray(&deleteArray_MiniTreeEventSelection);
      instance.SetDestructor(&destruct_MiniTreeEventSelection);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MiniTreeEventSelection*)
   {
      return GenerateInitInstanceLocal((::MiniTreeEventSelection*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::MiniTreeEventSelection*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JMRUncertaintyAlgo(void *p = 0);
   static void *newArray_JMRUncertaintyAlgo(Long_t size, void *p);
   static void delete_JMRUncertaintyAlgo(void *p);
   static void deleteArray_JMRUncertaintyAlgo(void *p);
   static void destruct_JMRUncertaintyAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JMRUncertaintyAlgo*)
   {
      ::JMRUncertaintyAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JMRUncertaintyAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JMRUncertaintyAlgo", ::JMRUncertaintyAlgo::Class_Version(), "ZprimeDM/JMRUncertaintyAlgo.h", 21,
                  typeid(::JMRUncertaintyAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::JMRUncertaintyAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JMRUncertaintyAlgo) );
      instance.SetNew(&new_JMRUncertaintyAlgo);
      instance.SetNewArray(&newArray_JMRUncertaintyAlgo);
      instance.SetDelete(&delete_JMRUncertaintyAlgo);
      instance.SetDeleteArray(&deleteArray_JMRUncertaintyAlgo);
      instance.SetDestructor(&destruct_JMRUncertaintyAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JMRUncertaintyAlgo*)
   {
      return GenerateInitInstanceLocal((::JMRUncertaintyAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::JMRUncertaintyAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ProcessTrigStudy(void *p = 0);
   static void *newArray_ProcessTrigStudy(Long_t size, void *p);
   static void delete_ProcessTrigStudy(void *p);
   static void deleteArray_ProcessTrigStudy(void *p);
   static void destruct_ProcessTrigStudy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ProcessTrigStudy*)
   {
      ::ProcessTrigStudy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ProcessTrigStudy >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ProcessTrigStudy", ::ProcessTrigStudy::Class_Version(), "ZprimeDM/ProcessTrigStudy.h", 27,
                  typeid(::ProcessTrigStudy), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ProcessTrigStudy::Dictionary, isa_proxy, 4,
                  sizeof(::ProcessTrigStudy) );
      instance.SetNew(&new_ProcessTrigStudy);
      instance.SetNewArray(&newArray_ProcessTrigStudy);
      instance.SetDelete(&delete_ProcessTrigStudy);
      instance.SetDeleteArray(&deleteArray_ProcessTrigStudy);
      instance.SetDestructor(&destruct_ProcessTrigStudy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ProcessTrigStudy*)
   {
      return GenerateInitInstanceLocal((::ProcessTrigStudy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ProcessTrigStudy*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ZprimeISRBoostHistsAlgo(void *p = 0);
   static void *newArray_ZprimeISRBoostHistsAlgo(Long_t size, void *p);
   static void delete_ZprimeISRBoostHistsAlgo(void *p);
   static void deleteArray_ZprimeISRBoostHistsAlgo(void *p);
   static void destruct_ZprimeISRBoostHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZprimeISRBoostHistsAlgo*)
   {
      ::ZprimeISRBoostHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ZprimeISRBoostHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ZprimeISRBoostHistsAlgo", ::ZprimeISRBoostHistsAlgo::Class_Version(), "ZprimeDM/ZprimeISRBoostHistsAlgo.h", 16,
                  typeid(::ZprimeISRBoostHistsAlgo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ZprimeISRBoostHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ZprimeISRBoostHistsAlgo) );
      instance.SetNew(&new_ZprimeISRBoostHistsAlgo);
      instance.SetNewArray(&newArray_ZprimeISRBoostHistsAlgo);
      instance.SetDelete(&delete_ZprimeISRBoostHistsAlgo);
      instance.SetDeleteArray(&deleteArray_ZprimeISRBoostHistsAlgo);
      instance.SetDestructor(&destruct_ZprimeISRBoostHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZprimeISRBoostHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::ZprimeISRBoostHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZprimeISRBoostHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr ZprimeAlgorithm::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeAlgorithm::Class_Name()
{
   return "ZprimeAlgorithm";
}

//______________________________________________________________________________
const char *ZprimeAlgorithm::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeAlgorithm*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeAlgorithm::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeAlgorithm*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeAlgorithm::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeAlgorithm*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeAlgorithm::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeAlgorithm*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TruthPhotonJetOR::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TruthPhotonJetOR::Class_Name()
{
   return "TruthPhotonJetOR";
}

//______________________________________________________________________________
const char *TruthPhotonJetOR::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TruthPhotonJetOR*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TruthPhotonJetOR::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TruthPhotonJetOR*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TruthPhotonJetOR::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TruthPhotonJetOR*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TruthPhotonJetOR::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TruthPhotonJetOR*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetPartonHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetPartonHistsAlgo::Class_Name()
{
   return "JetPartonHistsAlgo";
}

//______________________________________________________________________________
const char *JetPartonHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetPartonHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetPartonHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetPartonHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetPartonHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetPartonHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetPartonHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetPartonHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeMGTrijetAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeMGTrijetAlgo::Class_Name()
{
   return "ZprimeMGTrijetAlgo";
}

//______________________________________________________________________________
const char *ZprimeMGTrijetAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeMGTrijetAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeMGTrijetAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeMGTrijetAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeMGTrijetAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeMGTrijetAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeMGTrijetAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeMGTrijetAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeMGTrijetPickAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeMGTrijetPickAlgo::Class_Name()
{
   return "ZprimeMGTrijetPickAlgo";
}

//______________________________________________________________________________
const char *ZprimeMGTrijetPickAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeMGTrijetPickAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeMGTrijetPickAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeMGTrijetPickAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeMGTrijetPickAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeMGTrijetPickAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeMGTrijetPickAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeMGTrijetPickAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeHistsBaseAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeHistsBaseAlgo::Class_Name()
{
   return "ZprimeHistsBaseAlgo";
}

//______________________________________________________________________________
const char *ZprimeHistsBaseAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeHistsBaseAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeHistsBaseAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeHistsBaseAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeHistsBaseAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeHistsBaseAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeHistsBaseAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeHistsBaseAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeGammaJetJetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeGammaJetJetHistsAlgo::Class_Name()
{
   return "ZprimeGammaJetJetHistsAlgo";
}

//______________________________________________________________________________
const char *ZprimeGammaJetJetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeGammaJetJetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeGammaJetJetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeGammaJetJetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeGammaJetJetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeGammaJetJetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeGammaJetJetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeGammaJetJetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeDijetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeDijetHistsAlgo::Class_Name()
{
   return "ZprimeDijetHistsAlgo";
}

//______________________________________________________________________________
const char *ZprimeDijetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeDijetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeDijetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeDijetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeDijetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeDijetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeDijetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeDijetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeTrijetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeTrijetHistsAlgo::Class_Name()
{
   return "ZprimeTrijetHistsAlgo";
}

//______________________________________________________________________________
const char *ZprimeTrijetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeTrijetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeTrijetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeTrijetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeTrijetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeTrijetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeTrijetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeTrijetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr DiphotonHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *DiphotonHistsAlgo::Class_Name()
{
   return "DiphotonHistsAlgo";
}

//______________________________________________________________________________
const char *DiphotonHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::DiphotonHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int DiphotonHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::DiphotonHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *DiphotonHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::DiphotonHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *DiphotonHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::DiphotonHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr GammaJetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *GammaJetHistsAlgo::Class_Name()
{
   return "GammaJetHistsAlgo";
}

//______________________________________________________________________________
const char *GammaJetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::GammaJetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int GammaJetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::GammaJetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *GammaJetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::GammaJetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *GammaJetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::GammaJetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeNtupler::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeNtupler::Class_Name()
{
   return "ZprimeNtupler";
}

//______________________________________________________________________________
const char *ZprimeNtupler::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeNtupler*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeNtupler::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeNtupler*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeNtupler::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeNtupler*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeNtupler::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeNtupler*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeJetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeJetHistsAlgo::Class_Name()
{
   return "ZprimeJetHistsAlgo";
}

//______________________________________________________________________________
const char *ZprimeJetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeJetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeJetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeJetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeJetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeJetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeJetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeJetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetStudiesAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetStudiesAlgo::Class_Name()
{
   return "JetStudiesAlgo";
}

//______________________________________________________________________________
const char *JetStudiesAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetStudiesAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetStudiesAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetStudiesAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetStudiesAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetStudiesAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetStudiesAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetStudiesAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr SortAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *SortAlgo::Class_Name()
{
   return "SortAlgo";
}

//______________________________________________________________________________
const char *SortAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SortAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int SortAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SortAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *SortAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SortAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *SortAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SortAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr MiniTreeEventSelection::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MiniTreeEventSelection::Class_Name()
{
   return "MiniTreeEventSelection";
}

//______________________________________________________________________________
const char *MiniTreeEventSelection::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MiniTreeEventSelection*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MiniTreeEventSelection::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MiniTreeEventSelection*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MiniTreeEventSelection::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MiniTreeEventSelection*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MiniTreeEventSelection::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MiniTreeEventSelection*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JMRUncertaintyAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JMRUncertaintyAlgo::Class_Name()
{
   return "JMRUncertaintyAlgo";
}

//______________________________________________________________________________
const char *JMRUncertaintyAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JMRUncertaintyAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JMRUncertaintyAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JMRUncertaintyAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JMRUncertaintyAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JMRUncertaintyAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JMRUncertaintyAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JMRUncertaintyAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ProcessTrigStudy::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ProcessTrigStudy::Class_Name()
{
   return "ProcessTrigStudy";
}

//______________________________________________________________________________
const char *ProcessTrigStudy::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ProcessTrigStudy*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ProcessTrigStudy::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ProcessTrigStudy*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ProcessTrigStudy::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ProcessTrigStudy*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ProcessTrigStudy::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ProcessTrigStudy*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ZprimeISRBoostHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ZprimeISRBoostHistsAlgo::Class_Name()
{
   return "ZprimeISRBoostHistsAlgo";
}

//______________________________________________________________________________
const char *ZprimeISRBoostHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeISRBoostHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ZprimeISRBoostHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ZprimeISRBoostHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ZprimeISRBoostHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeISRBoostHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ZprimeISRBoostHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ZprimeISRBoostHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void ZprimeAlgorithm::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeAlgorithm.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeAlgorithm::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeAlgorithm::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ZprimeAlgorithm(void *p) {
      delete ((::ZprimeAlgorithm*)p);
   }
   static void deleteArray_ZprimeAlgorithm(void *p) {
      delete [] ((::ZprimeAlgorithm*)p);
   }
   static void destruct_ZprimeAlgorithm(void *p) {
      typedef ::ZprimeAlgorithm current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeAlgorithm

//______________________________________________________________________________
void TruthPhotonJetOR::Streamer(TBuffer &R__b)
{
   // Stream an object of class TruthPhotonJetOR.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TruthPhotonJetOR::Class(),this);
   } else {
      R__b.WriteClassBuffer(TruthPhotonJetOR::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TruthPhotonJetOR(void *p) {
      return  p ? new(p) ::TruthPhotonJetOR : new ::TruthPhotonJetOR;
   }
   static void *newArray_TruthPhotonJetOR(Long_t nElements, void *p) {
      return p ? new(p) ::TruthPhotonJetOR[nElements] : new ::TruthPhotonJetOR[nElements];
   }
   // Wrapper around operator delete
   static void delete_TruthPhotonJetOR(void *p) {
      delete ((::TruthPhotonJetOR*)p);
   }
   static void deleteArray_TruthPhotonJetOR(void *p) {
      delete [] ((::TruthPhotonJetOR*)p);
   }
   static void destruct_TruthPhotonJetOR(void *p) {
      typedef ::TruthPhotonJetOR current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TruthPhotonJetOR

//______________________________________________________________________________
void JetPartonHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetPartonHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetPartonHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetPartonHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetPartonHistsAlgo(void *p) {
      return  p ? new(p) ::JetPartonHistsAlgo : new ::JetPartonHistsAlgo;
   }
   static void *newArray_JetPartonHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JetPartonHistsAlgo[nElements] : new ::JetPartonHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetPartonHistsAlgo(void *p) {
      delete ((::JetPartonHistsAlgo*)p);
   }
   static void deleteArray_JetPartonHistsAlgo(void *p) {
      delete [] ((::JetPartonHistsAlgo*)p);
   }
   static void destruct_JetPartonHistsAlgo(void *p) {
      typedef ::JetPartonHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetPartonHistsAlgo

//______________________________________________________________________________
void ZprimeMGTrijetAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeMGTrijetAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeMGTrijetAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeMGTrijetAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeMGTrijetAlgo(void *p) {
      return  p ? new(p) ::ZprimeMGTrijetAlgo : new ::ZprimeMGTrijetAlgo;
   }
   static void *newArray_ZprimeMGTrijetAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeMGTrijetAlgo[nElements] : new ::ZprimeMGTrijetAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeMGTrijetAlgo(void *p) {
      delete ((::ZprimeMGTrijetAlgo*)p);
   }
   static void deleteArray_ZprimeMGTrijetAlgo(void *p) {
      delete [] ((::ZprimeMGTrijetAlgo*)p);
   }
   static void destruct_ZprimeMGTrijetAlgo(void *p) {
      typedef ::ZprimeMGTrijetAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeMGTrijetAlgo

//______________________________________________________________________________
void ZprimeMGTrijetPickAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeMGTrijetPickAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeMGTrijetPickAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeMGTrijetPickAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeMGTrijetPickAlgo(void *p) {
      return  p ? new(p) ::ZprimeMGTrijetPickAlgo : new ::ZprimeMGTrijetPickAlgo;
   }
   static void *newArray_ZprimeMGTrijetPickAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeMGTrijetPickAlgo[nElements] : new ::ZprimeMGTrijetPickAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeMGTrijetPickAlgo(void *p) {
      delete ((::ZprimeMGTrijetPickAlgo*)p);
   }
   static void deleteArray_ZprimeMGTrijetPickAlgo(void *p) {
      delete [] ((::ZprimeMGTrijetPickAlgo*)p);
   }
   static void destruct_ZprimeMGTrijetPickAlgo(void *p) {
      typedef ::ZprimeMGTrijetPickAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeMGTrijetPickAlgo

//______________________________________________________________________________
void ZprimeHistsBaseAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeHistsBaseAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeHistsBaseAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeHistsBaseAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ZprimeHistsBaseAlgo(void *p) {
      delete ((::ZprimeHistsBaseAlgo*)p);
   }
   static void deleteArray_ZprimeHistsBaseAlgo(void *p) {
      delete [] ((::ZprimeHistsBaseAlgo*)p);
   }
   static void destruct_ZprimeHistsBaseAlgo(void *p) {
      typedef ::ZprimeHistsBaseAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeHistsBaseAlgo

//______________________________________________________________________________
void ZprimeGammaJetJetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeGammaJetJetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeGammaJetJetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeGammaJetJetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeGammaJetJetHistsAlgo(void *p) {
      return  p ? new(p) ::ZprimeGammaJetJetHistsAlgo : new ::ZprimeGammaJetJetHistsAlgo;
   }
   static void *newArray_ZprimeGammaJetJetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeGammaJetJetHistsAlgo[nElements] : new ::ZprimeGammaJetJetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeGammaJetJetHistsAlgo(void *p) {
      delete ((::ZprimeGammaJetJetHistsAlgo*)p);
   }
   static void deleteArray_ZprimeGammaJetJetHistsAlgo(void *p) {
      delete [] ((::ZprimeGammaJetJetHistsAlgo*)p);
   }
   static void destruct_ZprimeGammaJetJetHistsAlgo(void *p) {
      typedef ::ZprimeGammaJetJetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeGammaJetJetHistsAlgo

//______________________________________________________________________________
void ZprimeDijetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeDijetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeDijetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeDijetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeDijetHistsAlgo(void *p) {
      return  p ? new(p) ::ZprimeDijetHistsAlgo : new ::ZprimeDijetHistsAlgo;
   }
   static void *newArray_ZprimeDijetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeDijetHistsAlgo[nElements] : new ::ZprimeDijetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeDijetHistsAlgo(void *p) {
      delete ((::ZprimeDijetHistsAlgo*)p);
   }
   static void deleteArray_ZprimeDijetHistsAlgo(void *p) {
      delete [] ((::ZprimeDijetHistsAlgo*)p);
   }
   static void destruct_ZprimeDijetHistsAlgo(void *p) {
      typedef ::ZprimeDijetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeDijetHistsAlgo

//______________________________________________________________________________
void ZprimeTrijetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeTrijetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeTrijetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeTrijetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeTrijetHistsAlgo(void *p) {
      return  p ? new(p) ::ZprimeTrijetHistsAlgo : new ::ZprimeTrijetHistsAlgo;
   }
   static void *newArray_ZprimeTrijetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeTrijetHistsAlgo[nElements] : new ::ZprimeTrijetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeTrijetHistsAlgo(void *p) {
      delete ((::ZprimeTrijetHistsAlgo*)p);
   }
   static void deleteArray_ZprimeTrijetHistsAlgo(void *p) {
      delete [] ((::ZprimeTrijetHistsAlgo*)p);
   }
   static void destruct_ZprimeTrijetHistsAlgo(void *p) {
      typedef ::ZprimeTrijetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeTrijetHistsAlgo

//______________________________________________________________________________
void DiphotonHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class DiphotonHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(DiphotonHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(DiphotonHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_DiphotonHistsAlgo(void *p) {
      return  p ? new(p) ::DiphotonHistsAlgo : new ::DiphotonHistsAlgo;
   }
   static void *newArray_DiphotonHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::DiphotonHistsAlgo[nElements] : new ::DiphotonHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_DiphotonHistsAlgo(void *p) {
      delete ((::DiphotonHistsAlgo*)p);
   }
   static void deleteArray_DiphotonHistsAlgo(void *p) {
      delete [] ((::DiphotonHistsAlgo*)p);
   }
   static void destruct_DiphotonHistsAlgo(void *p) {
      typedef ::DiphotonHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DiphotonHistsAlgo

//______________________________________________________________________________
void GammaJetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class GammaJetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(GammaJetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(GammaJetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_GammaJetHistsAlgo(void *p) {
      return  p ? new(p) ::GammaJetHistsAlgo : new ::GammaJetHistsAlgo;
   }
   static void *newArray_GammaJetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::GammaJetHistsAlgo[nElements] : new ::GammaJetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_GammaJetHistsAlgo(void *p) {
      delete ((::GammaJetHistsAlgo*)p);
   }
   static void deleteArray_GammaJetHistsAlgo(void *p) {
      delete [] ((::GammaJetHistsAlgo*)p);
   }
   static void destruct_GammaJetHistsAlgo(void *p) {
      typedef ::GammaJetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::GammaJetHistsAlgo

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeNtuplerContainer(void *p) {
      return  p ? new(p) ::ZprimeNtuplerContainer : new ::ZprimeNtuplerContainer;
   }
   static void *newArray_ZprimeNtuplerContainer(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeNtuplerContainer[nElements] : new ::ZprimeNtuplerContainer[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeNtuplerContainer(void *p) {
      delete ((::ZprimeNtuplerContainer*)p);
   }
   static void deleteArray_ZprimeNtuplerContainer(void *p) {
      delete [] ((::ZprimeNtuplerContainer*)p);
   }
   static void destruct_ZprimeNtuplerContainer(void *p) {
      typedef ::ZprimeNtuplerContainer current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeNtuplerContainer

//______________________________________________________________________________
void ZprimeNtupler::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeNtupler.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeNtupler::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeNtupler::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeNtupler(void *p) {
      return  p ? new(p) ::ZprimeNtupler : new ::ZprimeNtupler;
   }
   static void *newArray_ZprimeNtupler(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeNtupler[nElements] : new ::ZprimeNtupler[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeNtupler(void *p) {
      delete ((::ZprimeNtupler*)p);
   }
   static void deleteArray_ZprimeNtupler(void *p) {
      delete [] ((::ZprimeNtupler*)p);
   }
   static void destruct_ZprimeNtupler(void *p) {
      typedef ::ZprimeNtupler current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeNtupler

//______________________________________________________________________________
void ZprimeJetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeJetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeJetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeJetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeJetHistsAlgo(void *p) {
      return  p ? new(p) ::ZprimeJetHistsAlgo : new ::ZprimeJetHistsAlgo;
   }
   static void *newArray_ZprimeJetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeJetHistsAlgo[nElements] : new ::ZprimeJetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeJetHistsAlgo(void *p) {
      delete ((::ZprimeJetHistsAlgo*)p);
   }
   static void deleteArray_ZprimeJetHistsAlgo(void *p) {
      delete [] ((::ZprimeJetHistsAlgo*)p);
   }
   static void destruct_ZprimeJetHistsAlgo(void *p) {
      typedef ::ZprimeJetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeJetHistsAlgo

//______________________________________________________________________________
void JetStudiesAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetStudiesAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetStudiesAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetStudiesAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetStudiesAlgo(void *p) {
      return  p ? new(p) ::JetStudiesAlgo : new ::JetStudiesAlgo;
   }
   static void *newArray_JetStudiesAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JetStudiesAlgo[nElements] : new ::JetStudiesAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetStudiesAlgo(void *p) {
      delete ((::JetStudiesAlgo*)p);
   }
   static void deleteArray_JetStudiesAlgo(void *p) {
      delete [] ((::JetStudiesAlgo*)p);
   }
   static void destruct_JetStudiesAlgo(void *p) {
      typedef ::JetStudiesAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetStudiesAlgo

//______________________________________________________________________________
void SortAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class SortAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SortAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(SortAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SortAlgo(void *p) {
      return  p ? new(p) ::SortAlgo : new ::SortAlgo;
   }
   static void *newArray_SortAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::SortAlgo[nElements] : new ::SortAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_SortAlgo(void *p) {
      delete ((::SortAlgo*)p);
   }
   static void deleteArray_SortAlgo(void *p) {
      delete [] ((::SortAlgo*)p);
   }
   static void destruct_SortAlgo(void *p) {
      typedef ::SortAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SortAlgo

//______________________________________________________________________________
void MiniTreeEventSelection::Streamer(TBuffer &R__b)
{
   // Stream an object of class MiniTreeEventSelection.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MiniTreeEventSelection::Class(),this);
   } else {
      R__b.WriteClassBuffer(MiniTreeEventSelection::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MiniTreeEventSelection(void *p) {
      return  p ? new(p) ::MiniTreeEventSelection : new ::MiniTreeEventSelection;
   }
   static void *newArray_MiniTreeEventSelection(Long_t nElements, void *p) {
      return p ? new(p) ::MiniTreeEventSelection[nElements] : new ::MiniTreeEventSelection[nElements];
   }
   // Wrapper around operator delete
   static void delete_MiniTreeEventSelection(void *p) {
      delete ((::MiniTreeEventSelection*)p);
   }
   static void deleteArray_MiniTreeEventSelection(void *p) {
      delete [] ((::MiniTreeEventSelection*)p);
   }
   static void destruct_MiniTreeEventSelection(void *p) {
      typedef ::MiniTreeEventSelection current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MiniTreeEventSelection

//______________________________________________________________________________
void JMRUncertaintyAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JMRUncertaintyAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JMRUncertaintyAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JMRUncertaintyAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JMRUncertaintyAlgo(void *p) {
      return  p ? new(p) ::JMRUncertaintyAlgo : new ::JMRUncertaintyAlgo;
   }
   static void *newArray_JMRUncertaintyAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JMRUncertaintyAlgo[nElements] : new ::JMRUncertaintyAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JMRUncertaintyAlgo(void *p) {
      delete ((::JMRUncertaintyAlgo*)p);
   }
   static void deleteArray_JMRUncertaintyAlgo(void *p) {
      delete [] ((::JMRUncertaintyAlgo*)p);
   }
   static void destruct_JMRUncertaintyAlgo(void *p) {
      typedef ::JMRUncertaintyAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JMRUncertaintyAlgo

//______________________________________________________________________________
void ProcessTrigStudy::Streamer(TBuffer &R__b)
{
   // Stream an object of class ProcessTrigStudy.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ProcessTrigStudy::Class(),this);
   } else {
      R__b.WriteClassBuffer(ProcessTrigStudy::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ProcessTrigStudy(void *p) {
      return  p ? new(p) ::ProcessTrigStudy : new ::ProcessTrigStudy;
   }
   static void *newArray_ProcessTrigStudy(Long_t nElements, void *p) {
      return p ? new(p) ::ProcessTrigStudy[nElements] : new ::ProcessTrigStudy[nElements];
   }
   // Wrapper around operator delete
   static void delete_ProcessTrigStudy(void *p) {
      delete ((::ProcessTrigStudy*)p);
   }
   static void deleteArray_ProcessTrigStudy(void *p) {
      delete [] ((::ProcessTrigStudy*)p);
   }
   static void destruct_ProcessTrigStudy(void *p) {
      typedef ::ProcessTrigStudy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ProcessTrigStudy

//______________________________________________________________________________
void ZprimeISRBoostHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ZprimeISRBoostHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ZprimeISRBoostHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ZprimeISRBoostHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ZprimeISRBoostHistsAlgo(void *p) {
      return  p ? new(p) ::ZprimeISRBoostHistsAlgo : new ::ZprimeISRBoostHistsAlgo;
   }
   static void *newArray_ZprimeISRBoostHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ZprimeISRBoostHistsAlgo[nElements] : new ::ZprimeISRBoostHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ZprimeISRBoostHistsAlgo(void *p) {
      delete ((::ZprimeISRBoostHistsAlgo*)p);
   }
   static void deleteArray_ZprimeISRBoostHistsAlgo(void *p) {
      delete [] ((::ZprimeISRBoostHistsAlgo*)p);
   }
   static void destruct_ZprimeISRBoostHistsAlgo(void *p) {
      typedef ::ZprimeISRBoostHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZprimeISRBoostHistsAlgo

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 339,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlEZprimeNtuplerContainergR_Dictionary();
   static void vectorlEZprimeNtuplerContainergR_TClassManip(TClass*);
   static void *new_vectorlEZprimeNtuplerContainergR(void *p = 0);
   static void *newArray_vectorlEZprimeNtuplerContainergR(Long_t size, void *p);
   static void delete_vectorlEZprimeNtuplerContainergR(void *p);
   static void deleteArray_vectorlEZprimeNtuplerContainergR(void *p);
   static void destruct_vectorlEZprimeNtuplerContainergR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ZprimeNtuplerContainer>*)
   {
      vector<ZprimeNtuplerContainer> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ZprimeNtuplerContainer>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ZprimeNtuplerContainer>", -2, "vector", 339,
                  typeid(vector<ZprimeNtuplerContainer>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEZprimeNtuplerContainergR_Dictionary, isa_proxy, 0,
                  sizeof(vector<ZprimeNtuplerContainer>) );
      instance.SetNew(&new_vectorlEZprimeNtuplerContainergR);
      instance.SetNewArray(&newArray_vectorlEZprimeNtuplerContainergR);
      instance.SetDelete(&delete_vectorlEZprimeNtuplerContainergR);
      instance.SetDeleteArray(&deleteArray_vectorlEZprimeNtuplerContainergR);
      instance.SetDestructor(&destruct_vectorlEZprimeNtuplerContainergR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ZprimeNtuplerContainer> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<ZprimeNtuplerContainer>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEZprimeNtuplerContainergR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ZprimeNtuplerContainer>*)0x0)->GetClass();
      vectorlEZprimeNtuplerContainergR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEZprimeNtuplerContainergR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEZprimeNtuplerContainergR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ZprimeNtuplerContainer> : new vector<ZprimeNtuplerContainer>;
   }
   static void *newArray_vectorlEZprimeNtuplerContainergR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ZprimeNtuplerContainer>[nElements] : new vector<ZprimeNtuplerContainer>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEZprimeNtuplerContainergR(void *p) {
      delete ((vector<ZprimeNtuplerContainer>*)p);
   }
   static void deleteArray_vectorlEZprimeNtuplerContainergR(void *p) {
      delete [] ((vector<ZprimeNtuplerContainer>*)p);
   }
   static void destruct_vectorlEZprimeNtuplerContainergR(void *p) {
      typedef vector<ZprimeNtuplerContainer> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ZprimeNtuplerContainer>

namespace ROOT {
   static TClass *vectorlEZprimeISRBoostHistsmUgR_Dictionary();
   static void vectorlEZprimeISRBoostHistsmUgR_TClassManip(TClass*);
   static void *new_vectorlEZprimeISRBoostHistsmUgR(void *p = 0);
   static void *newArray_vectorlEZprimeISRBoostHistsmUgR(Long_t size, void *p);
   static void delete_vectorlEZprimeISRBoostHistsmUgR(void *p);
   static void deleteArray_vectorlEZprimeISRBoostHistsmUgR(void *p);
   static void destruct_vectorlEZprimeISRBoostHistsmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ZprimeISRBoostHists*>*)
   {
      vector<ZprimeISRBoostHists*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ZprimeISRBoostHists*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ZprimeISRBoostHists*>", -2, "vector", 339,
                  typeid(vector<ZprimeISRBoostHists*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEZprimeISRBoostHistsmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<ZprimeISRBoostHists*>) );
      instance.SetNew(&new_vectorlEZprimeISRBoostHistsmUgR);
      instance.SetNewArray(&newArray_vectorlEZprimeISRBoostHistsmUgR);
      instance.SetDelete(&delete_vectorlEZprimeISRBoostHistsmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEZprimeISRBoostHistsmUgR);
      instance.SetDestructor(&destruct_vectorlEZprimeISRBoostHistsmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ZprimeISRBoostHists*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<ZprimeISRBoostHists*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEZprimeISRBoostHistsmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ZprimeISRBoostHists*>*)0x0)->GetClass();
      vectorlEZprimeISRBoostHistsmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEZprimeISRBoostHistsmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEZprimeISRBoostHistsmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ZprimeISRBoostHists*> : new vector<ZprimeISRBoostHists*>;
   }
   static void *newArray_vectorlEZprimeISRBoostHistsmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ZprimeISRBoostHists*>[nElements] : new vector<ZprimeISRBoostHists*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEZprimeISRBoostHistsmUgR(void *p) {
      delete ((vector<ZprimeISRBoostHists*>*)p);
   }
   static void deleteArray_vectorlEZprimeISRBoostHistsmUgR(void *p) {
      delete [] ((vector<ZprimeISRBoostHists*>*)p);
   }
   static void destruct_vectorlEZprimeISRBoostHistsmUgR(void *p) {
      typedef vector<ZprimeISRBoostHists*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ZprimeISRBoostHists*>

namespace ROOT {
   static TClass *vectorlEDijetISRHistsmUgR_Dictionary();
   static void vectorlEDijetISRHistsmUgR_TClassManip(TClass*);
   static void *new_vectorlEDijetISRHistsmUgR(void *p = 0);
   static void *newArray_vectorlEDijetISRHistsmUgR(Long_t size, void *p);
   static void delete_vectorlEDijetISRHistsmUgR(void *p);
   static void deleteArray_vectorlEDijetISRHistsmUgR(void *p);
   static void destruct_vectorlEDijetISRHistsmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DijetISRHists*>*)
   {
      vector<DijetISRHists*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DijetISRHists*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DijetISRHists*>", -2, "vector", 339,
                  typeid(vector<DijetISRHists*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEDijetISRHistsmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<DijetISRHists*>) );
      instance.SetNew(&new_vectorlEDijetISRHistsmUgR);
      instance.SetNewArray(&newArray_vectorlEDijetISRHistsmUgR);
      instance.SetDelete(&delete_vectorlEDijetISRHistsmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDijetISRHistsmUgR);
      instance.SetDestructor(&destruct_vectorlEDijetISRHistsmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DijetISRHists*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<DijetISRHists*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDijetISRHistsmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DijetISRHists*>*)0x0)->GetClass();
      vectorlEDijetISRHistsmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDijetISRHistsmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDijetISRHistsmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<DijetISRHists*> : new vector<DijetISRHists*>;
   }
   static void *newArray_vectorlEDijetISRHistsmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<DijetISRHists*>[nElements] : new vector<DijetISRHists*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDijetISRHistsmUgR(void *p) {
      delete ((vector<DijetISRHists*>*)p);
   }
   static void deleteArray_vectorlEDijetISRHistsmUgR(void *p) {
      delete [] ((vector<DijetISRHists*>*)p);
   }
   static void destruct_vectorlEDijetISRHistsmUgR(void *p) {
      typedef vector<DijetISRHists*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DijetISRHists*>

namespace {
  void TriggerDictionaryInitialization_libZprimeDMLib_Impl() {
    static const char* headers[] = {
"ZprimeDM/BCIDBugChecker.h",
"ZprimeDM/CutflowHists.h",
"ZprimeDM/DebugHists.h",
"ZprimeDM/DijetISREvent.h",
"ZprimeDM/DijetISRHists.h",
"ZprimeDM/DiphotonHists.h",
"ZprimeDM/DiphotonHistsAlgo.h",
"ZprimeDM/ElectronHists.h",
"ZprimeDM/EventHists.h",
"ZprimeDM/FatJetHists.h",
"ZprimeDM/GammaJetHists.h",
"ZprimeDM/GammaJetHistsAlgo.h",
"ZprimeDM/JMRUncertaintyAlgo.h",
"ZprimeDM/JetHists.h",
"ZprimeDM/JetPartonHistsAlgo.h",
"ZprimeDM/JetStudiesAlgo.h",
"ZprimeDM/MiniTreeEventSelection.h",
"ZprimeDM/MuonHists.h",
"ZprimeDM/PartonJetHists.h",
"ZprimeDM/PhotonHists.h",
"ZprimeDM/PhotonTriggerHists.h",
"ZprimeDM/ProcessTrigStudy.h",
"ZprimeDM/ResonanceHists.h",
"ZprimeDM/SortAlgo.h",
"ZprimeDM/TruthHists.h",
"ZprimeDM/TruthPhotonJetOR.h",
"ZprimeDM/ZprimeAlgorithm.h",
"ZprimeDM/ZprimeDijetHistsAlgo.h",
"ZprimeDM/ZprimeGammaJetJetHistsAlgo.h",
"ZprimeDM/ZprimeHelperClasses.h",
"ZprimeDM/ZprimeHistsBaseAlgo.h",
"ZprimeDM/ZprimeISRBoostHists.h",
"ZprimeDM/ZprimeISRBoostHistsAlgo.h",
"ZprimeDM/ZprimeISRHists.h",
"ZprimeDM/ZprimeJetHistsAlgo.h",
"ZprimeDM/ZprimeMGTrijet2DHists.h",
"ZprimeDM/ZprimeMGTrijetAlgo.h",
"ZprimeDM/ZprimeMGTrijetHists.h",
"ZprimeDM/ZprimeMGTrijetPickAlgo.h",
"ZprimeDM/ZprimeMiniTree.h",
"ZprimeDM/ZprimeNtupler.h",
"ZprimeDM/ZprimeNtuplerContainer.h",
"ZprimeDM/ZprimeResonanceHists.h",
"ZprimeDM/ZprimeTrijetHistsAlgo.h",
"ZprimeDM/ZprimeTruthHists.h",
"ZprimeDM/ZprimeTruthResonanceHists.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/xAODAnaHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCutFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTriggerCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/AssociationUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/IsolationSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PMGTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/IsolationCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonShowerShapeFudgeTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetMissingEtID/JetSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/JetAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/TauID/TauAnalysisTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/tauRecTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetAnalysisTools/JetTileCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetCPInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetCalibTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetJvtEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetMomentTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetEDM",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetRec",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/EventShapes/EventShapeInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetResolution",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetSubStructureUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetUncertainties",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MET/METInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MET/METUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeFilelists",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeFilelists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/xAODAnaHelpers",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/xAODAnaHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetCalibTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetMissingEtID/JetSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/egamma/egammaMVACalib",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODHIEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/IsolationSelection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/TrackVertexAssociationTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/IsolationCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonShowerShapeFudgeTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/JetAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MET/METUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetResolution",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/tauRecTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/MET/METInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/TauID/TauAnalysisTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/AssociationUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetEDM",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetUncertainties",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetCPInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCutFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetMomentTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetRec",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/EventShapes/EventShapeInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetJvtEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PMGTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetSubStructureUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/JetAnalysisTools/JetTileCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Reconstruction/Jet/BoostedJetTaggers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTriggerCnv",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.93/InstallArea/x86_64-centos7-gcc8-opt/include",
"/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/ZprimeDM/CMakeFiles/makeZprimeDMLibCintDict.oeyOgf/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libZprimeDMLib dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeAlgorithm.h")))  ZprimeAlgorithm;
class __attribute__((annotate("$clingAutoload$ZprimeDM/TruthPhotonJetOR.h")))  TruthPhotonJetOR;
class __attribute__((annotate("$clingAutoload$ZprimeDM/JetPartonHistsAlgo.h")))  JetPartonHistsAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeMGTrijetAlgo.h")))  ZprimeMGTrijetAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeMGTrijetPickAlgo.h")))  ZprimeMGTrijetPickAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeHistsBaseAlgo.h")))  ZprimeHistsBaseAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeGammaJetJetHistsAlgo.h")))  ZprimeGammaJetJetHistsAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeDijetHistsAlgo.h")))  ZprimeDijetHistsAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeTrijetHistsAlgo.h")))  ZprimeTrijetHistsAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/DiphotonHistsAlgo.h")))  DiphotonHistsAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/GammaJetHistsAlgo.h")))  GammaJetHistsAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeNtuplerContainer.h")))  ZprimeNtuplerContainer;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeNtupler.h")))  ZprimeNtupler;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeJetHistsAlgo.h")))  ZprimeJetHistsAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/JetStudiesAlgo.h")))  JetStudiesAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/SortAlgo.h")))  SortAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/MiniTreeEventSelection.h")))  MiniTreeEventSelection;
class __attribute__((annotate("$clingAutoload$ZprimeDM/JMRUncertaintyAlgo.h")))  JMRUncertaintyAlgo;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ProcessTrigStudy.h")))  ProcessTrigStudy;
class __attribute__((annotate("$clingAutoload$ZprimeDM/ZprimeISRBoostHistsAlgo.h")))  ZprimeISRBoostHistsAlgo;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libZprimeDMLib dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "ZprimeDM-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ ZprimeDM-00-00-00
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif
#ifndef NO_SHOWERDECONSTRUCTION
  #define NO_SHOWERDECONSTRUCTION 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "ZprimeDM/BCIDBugChecker.h"
#include "ZprimeDM/CutflowHists.h"
#include "ZprimeDM/DebugHists.h"
#include "ZprimeDM/DijetISREvent.h"
#include "ZprimeDM/DijetISRHists.h"
#include "ZprimeDM/DiphotonHists.h"
#include "ZprimeDM/DiphotonHistsAlgo.h"
#include "ZprimeDM/ElectronHists.h"
#include "ZprimeDM/EventHists.h"
#include "ZprimeDM/FatJetHists.h"
#include "ZprimeDM/GammaJetHists.h"
#include "ZprimeDM/GammaJetHistsAlgo.h"
#include "ZprimeDM/JMRUncertaintyAlgo.h"
#include "ZprimeDM/JetHists.h"
#include "ZprimeDM/JetPartonHistsAlgo.h"
#include "ZprimeDM/JetStudiesAlgo.h"
#include "ZprimeDM/MiniTreeEventSelection.h"
#include "ZprimeDM/MuonHists.h"
#include "ZprimeDM/PartonJetHists.h"
#include "ZprimeDM/PhotonHists.h"
#include "ZprimeDM/PhotonTriggerHists.h"
#include "ZprimeDM/ProcessTrigStudy.h"
#include "ZprimeDM/ResonanceHists.h"
#include "ZprimeDM/SortAlgo.h"
#include "ZprimeDM/TruthHists.h"
#include "ZprimeDM/TruthPhotonJetOR.h"
#include "ZprimeDM/ZprimeAlgorithm.h"
#include "ZprimeDM/ZprimeDijetHistsAlgo.h"
#include "ZprimeDM/ZprimeGammaJetJetHistsAlgo.h"
#include "ZprimeDM/ZprimeHelperClasses.h"
#include "ZprimeDM/ZprimeHistsBaseAlgo.h"
#include "ZprimeDM/ZprimeISRBoostHists.h"
#include "ZprimeDM/ZprimeISRBoostHistsAlgo.h"
#include "ZprimeDM/ZprimeISRHists.h"
#include "ZprimeDM/ZprimeJetHistsAlgo.h"
#include "ZprimeDM/ZprimeMGTrijet2DHists.h"
#include "ZprimeDM/ZprimeMGTrijetAlgo.h"
#include "ZprimeDM/ZprimeMGTrijetHists.h"
#include "ZprimeDM/ZprimeMGTrijetPickAlgo.h"
#include "ZprimeDM/ZprimeMiniTree.h"
#include "ZprimeDM/ZprimeNtupler.h"
#include "ZprimeDM/ZprimeNtuplerContainer.h"
#include "ZprimeDM/ZprimeResonanceHists.h"
#include "ZprimeDM/ZprimeTrijetHistsAlgo.h"
#include "ZprimeDM/ZprimeTruthHists.h"
#include "ZprimeDM/ZprimeTruthResonanceHists.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DiphotonHistsAlgo", payloadCode, "@",
"GammaJetHistsAlgo", payloadCode, "@",
"JMRUncertaintyAlgo", payloadCode, "@",
"JetPartonHistsAlgo", payloadCode, "@",
"JetStudiesAlgo", payloadCode, "@",
"MiniTreeEventSelection", payloadCode, "@",
"ProcessTrigStudy", payloadCode, "@",
"SortAlgo", payloadCode, "@",
"TruthPhotonJetOR", payloadCode, "@",
"ZprimeAlgorithm", payloadCode, "@",
"ZprimeDijetHistsAlgo", payloadCode, "@",
"ZprimeGammaJetJetHistsAlgo", payloadCode, "@",
"ZprimeHistsBaseAlgo", payloadCode, "@",
"ZprimeISRBoostHistsAlgo", payloadCode, "@",
"ZprimeJetHistsAlgo", payloadCode, "@",
"ZprimeMGTrijetAlgo", payloadCode, "@",
"ZprimeMGTrijetPickAlgo", payloadCode, "@",
"ZprimeNtupler", payloadCode, "@",
"ZprimeNtuplerContainer", payloadCode, "@",
"ZprimeTrijetHistsAlgo", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libZprimeDMLib",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libZprimeDMLib_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libZprimeDMLib_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libZprimeDMLib() {
  TriggerDictionaryInitialization_libZprimeDMLib_Impl();
}
