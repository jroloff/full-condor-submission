# Install script for directory: /afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/InstallArea/x86_64-centos7-gcc8-opt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/src/ZprimeDM" TYPE DIRECTORY FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE REGEX "/\\.git$" EXCLUDE REGEX "/[^/]*\\~$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/lib/libZprimeDMLib_rdict.pcm")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
      -E make_directory
      $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
         -E create_symlink ../src/ZprimeDM/ZprimeDM
         $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include/ZprimeDM )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/lib/libZprimeDMLib.so.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY OPTIONAL FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/lib/libZprimeDMLib.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libZprimeDMLib.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libZprimeDMLib.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libZprimeDMLib.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE FILE OPTIONAL FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/bin/applyTreeWeight.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/bin/applyTreeWeight.exe")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/bin/applyTreeWeight")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/applyTreeWeight" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/applyTreeWeight")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/applyTreeWeight")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/ZprimeDM" TYPE FILE RENAME "__init__.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/python/__init__.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/ZprimeDM" TYPE FILE RENAME "commonconfig.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/python/commonconfig.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/ZprimeDM" TYPE FILE RENAME "commonconfigR20p7.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/python/commonconfigR20p7.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/ZprimeDM" TYPE FILE RENAME "ntupler.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/python/ntupler.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/ZprimeDM" TYPE FILE FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/python/ZprimeDM/__init__.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/ZprimeDM" TYPE FILE FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/python/ZprimeDM/commonconfig.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/ZprimeDM" TYPE FILE FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/python/ZprimeDM/commonconfigR20p7.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/ZprimeDM" TYPE FILE FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/build/x86_64-centos7-gcc8-opt/python/ZprimeDM/ntupler.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "applyMiniTreeEventCountWeight.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/scripts/applyMiniTreeEventCountWeight.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "librun.sh" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/scripts/librun.sh")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "mergeCampaignHists.sh" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/scripts/mergeCampaignHists.sh")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "runone.sh" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/scripts/runone.sh")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "updateHists.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/scripts/updateHists.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE DIRECTORY FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/MistimedEvents" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_dijet_treelevel.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_dijet_treelevel.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_dijet_trigger.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_dijet_trigger.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_dijetgamma.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_dijetgamma.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_dijetgamma_ntuple.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_dijetgamma_ntuple.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_dijetgamma_ntuple_truth.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_dijetgamma_ntuple_truth.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_dijetgamma_truth.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_dijetgamma_truth.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_diphoton.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_diphoton.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_diphoton_ntuple.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_diphoton_ntuple.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_gammajet.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_gammajet.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_jetJet.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_jetJet.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_jetJet.py~" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_jetJet.py~")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_jetJet_drloop.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_jetJet_drloop.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_jetJet_drloop.py~" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_jetJet_drloop.py~")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_jetJet_zero.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_jetJet_zero.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_jetJet_zero.py~" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_jetJet_zero.py~")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_jet_partons.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_jet_partons.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_jetstudy.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_jetstudy.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_photonJet.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_photonJet.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_photonJetv2.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_photonJetv2.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_photonJetv2.py~" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_photonJetv2.py~")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_photontrigger.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_photontrigger.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_photontrigger_ntuple.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_photontrigger_ntuple.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_simpledijet.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_simpledijet.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_trijet.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_trijet.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_trijet_ntuple.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_trijet_ntuple.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_trijet_ntuple_truth.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_trijet_ntuple_truth.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_trijet_truth.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_trijet_truth.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_validation_gammajetjet.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_validation_gammajetjet.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "config_zmumujetjet.py" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/config_zmumujetjet.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "miniNTupleVersions.txt" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/miniNTupleVersions.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/ZprimeDM" TYPE FILE RENAME "prwconfig.root" FILES "/afs/cern.ch/user/n/nlopezca/hbbisr_projv2_afs/ZprimeDM/data/prwconfig.root")
endif()

