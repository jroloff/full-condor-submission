import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig

c = Config()

jetDetailStr="kinematic clean"
if args.is_MC: jetDetailStr+=" truth"

fatjetDetailStr="kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet_GhostVR30Rmax4Rmin02TrackJet"
if args.is_MC: fatjetDetailStr+=" bosonCount"
fatjetDetailStrSyst="kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet_GhostVR30Rmax4Rmin02TrackJet"

subjetDetailStr="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr())
muonDetailStr="kinematic isolationKinematics"
elDetailStr="kinematic"

#btaggers =commonconfig.btaggers  if args.is_MC else ['MV2c10']
#btagmodes=commonconfig.btagmodes if args.is_MC else ['FixedCutBEff']
#btagWPs  =commonconfig.btagWPs   if args.is_MC else [85,77]

btaggers = []
btagmodes = []
btagWPs = []

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 triggerSelection='HLT_j[0-9]+|HLT_j.*_a10_lcw_L1J[0-9]+|HLT_j.*_a10r_L1J[0-9]+|HLT_j.*_a10_lcw_subjes_L1J[0-9]+|HLT_j.*_a10_lcw_sub_L1J[0-9]+|HLT_j.*_a10t_lcw_jes_L1J[0-9]+|HLT_j[0-9]+_a10t_lcw_jes_[0-9]+smcINF_L1SC111|HLT_j[0-9]+_a10t_lcw_jes_[0-9]+smcINF_L1J100|HLT_j260_320eta490|HLT_j225_gsc.*_boffperf_split|HLT_2j[0-9]+_a10t_lcw_jes_[0-9]+smcINF_L1SC111|HLT_2j[0-9]+_a10t_lcw_jes_[0-9]+smcINF_L1J100|HLT_j[0-9]+_a10t_lcw_jes_[0-9]+smcINF_j[0-9]+_a10t_lcw_jes_L1SC111',
                                 GRLset='ALLGOOD',
                                 doJets=True,doFatJets=True,doTrackJets=False,doPhotons=False,doMuons=True,doElectrons=False,
                                 #fatJetBtaggers =commonconfig.btaggers  if args.is_MC else ['MV2c10'],
                                 #fatJetBtagmodes=commonconfig.btagmodes if args.is_MC else ['FixedCutBEff'],
                                 #fatJetBtagWPs  =commonconfig.btagWPs   if args.is_MC else [85,77],
                                 #fatJetBtaggers = [],
                                 #fatJetBtagmodes = [],
                                 #fatJetBtag
                                 args=args.extra_options)

SelectFatJets=commonconfig.findAlgo(c,"SelectFatJets")
SelectFatJets.m_pass_min=1

containers=ROOT.vector('ZprimeNtuplerContainer')()

containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET     ,'SignalJets'                                 ,'jet'     ,jetDetailStr     ,'kinematic clean'))

containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.FATJET  ,'SignalFatJets'                              ,'fatjet'  ,fatjetDetailStr  ,fatjetDetailStrSyst))
containers.back().m_subjetDetailStr=subjetDetailStr
containers.back().m_subjetDetailStrSyst="kinematic {}".format(commonconfig.generate_btag_detailstr(btaggers=['MV2c10'], btagmodes=['FixedCutBEff'], btagWPs=[85,77]))

containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.MUON    ,'SignalMuons'                                ,'muon'    ,muonDetailStr    ,'kinematic'))

c.algorithm("ZprimeNtupler",       { "m_inputAlgo"           : "SignalFatJets_Algo",
                                     "m_containers"          : containers,
                                     "m_eventDetailStr"      : "pileup weightsSys",
                                     "m_trigDetailStr"       : "passTriggers passTrigBits"
                                     } )
