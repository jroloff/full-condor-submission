import ROOT
from xAODAnaHelpers import Config

c = Config()

c.algorithm("BasicEventSelection",          { "m_truthLevelOnly"      : True,
                                              "m_useMetaData"         : False
                                              } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "AntiKt10TruthTrimmedPtFrac5SmallR20Jets",
                                       "m_outContainerName"        :  "AntiKt10TruthTrimmedPtFrac5SmallR20JetsSort"
                                       } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "AntiKt4TruthDressedWZJets",
                                       "m_outContainerName"        :  "AntiKt4TruthDressedWZJetsSort"
                                       } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "TruthPhotons",
                                       "m_outContainerName"        :  "TruthPhotonsSort"
                                       } )

c.algorithm("JetSelector",                  { "m_inContainerName"         :  "AntiKt10TruthTrimmedPtFrac5SmallR20JetsSort",
                                              "m_outContainerName"        :  "SignalFatJets",
                                              "m_decorateSelectedObjects" :  False, 
                                              "m_createSelectedContainer" :  True, 
                                              "m_pT_min"                  :  250e3,
                                              "m_eta_max"                 :  2.0
                                              } )

c.algorithm("JetSelector",                  { "m_inContainerName"         :  "AntiKt4TruthDressedWZJetsSort",
                                              "m_outContainerName"        :  "SignalJets",
                                              "m_decorateSelectedObjects" :  False, 
                                              "m_createSelectedContainer" :  True, 
                                              "m_pT_min"                  :  25e3,
                                              "m_eta_max"                 :  2.5
                                              } )

c.algorithm("TruthSelector",                { "m_inContainerName"         :  "TruthPhotonsSort",
                                              "m_outContainerName"        :  "SignalPhotons",
                                              "m_decorateSelectedObjects" :  False,
                                              "m_createSelectedContainer" :  True,
                                              "m_pT_min"                  :  25e3,
                                              "m_eta_max"                 :  2.8
                                              } )

test=ROOT.ZprimeNtuplerContainer()

containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJets'      ,'jet','kinematic'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.FATJET,'SignalFatJets'   ,'fatjet','kinematic'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.TRUTH ,'SignalPhotons'   ,'ph'    ,'kinematic'))

c.algorithm("ZprimeNtupler",                { "m_truthLevelOnly" : True,
                                              "m_eventDetailStr" : "weightsSys",
                                              "m_containers"     : containers
                                              } )
