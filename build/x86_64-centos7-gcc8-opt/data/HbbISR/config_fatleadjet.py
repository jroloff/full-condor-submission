import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

import itertools

isSys=False
histDetailStr    ="mass"
jetDetailStr     ="kinematic clean"
if args.is_MC: jetDetailStr+=" truth"
fatjetDetailStr  ="kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet_GhostVR30Rmax4Rmin02TrackJet"
if args.is_MC: fatjetDetailStr+=" bosonCount"
subjetDetailStr  ="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr())
muonDetailStr    ="kinematic"
electronDetailStr="kinematic"

nameSuffix=""
if args.treeName!="outTree":
    isSys=True

    histDetailStr  =""
    jetDetailStr   ="kinematic clean"
    photonDetailStr="kinematic"

    nameSuffix="/sys"+args.treeName[7:]
else:
   isSys=False

c = Config()

commonsel={"m_msgLevel"               : ROOT.MSG.INFO,
           "m_mc"                     : args.is_MC,
           "m_histDetailStr"          : histDetailStr,
           "m_jetDetailStr"           : jetDetailStr,
           "m_fatjetDetailStr"        : fatjetDetailStr,
           "m_subjetDetailStr"        : subjetDetailStr,
           "m_muonDetailStr"          : muonDetailStr,
           #"m_electronDetailStr"      : electronDetailStr,
           "m_doPUReweight"           : False,
           "m_doTrigger"              : True,
           "m_trigger"                : "HLT_j360_a10_lcw_sub_L1J100,HLT_j420_a10_lcw_L1J100,HLT_j460_a10t_lcw_jes_L1J100",
           "m_doCleaning"             : True
           }

#
# Process Ntuple
#
c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_msgLevel"               : ROOT.MSG.INFO,
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : jetDetailStr,
                                        "m_fatjetDetailStr"        : fatjetDetailStr,
                                        "m_subjetDetailStr"        : subjetDetailStr,
                                        "m_muonDetailStr"          : muonDetailStr,
                                        #"m_electronDetailStr"      : electronDetailStr
                                        } )

#
# Different selections
fjpts             =[250]
hpts              =['fjpt']
hms               =[0]

#
# b-tag the two leading jets
for fjpt,hpt,hm in itertools.product(fjpts,hpts,hms):
    if hpt=='fjpt': hpt=fjpt

    name=['leadhbbisr']
    if fjpt>0:
        name.append('fj0pt%d'%fjpt)
    name.append('fj1pt250')
    name.append('ptsort0')
    if(hpt>0):
        name.append('hpt%d'%hpt)
    if(hm>0):
        name.append('hm%d'%hm)
    namestr='_'.join(name)
    config=commonsel.copy()
    config.update({"m_name"              : namestr,
                   "m_fatjet0PtCut"      : fjpt,
                   "m_fatjet1PtCut"      : 250,
                   "m_HcandPtCut"        : hpt,
                   "m_HcandMCut"         : hm
                   })
    c.algorithm("HbbISRLeadJetHistsAlgo", config )
