import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig
import itertools

doDetails   =True
histDetailStr  = "" #2d" #2d debug"
jetDetailStr   = "kinematic clean energy layer trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr())
if args.is_MC: jetDetailStr+=" truth"
fatjetDetailStr= "kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet_GhostVR30Rmax4Rmin02TrackJet VTags"
if args.is_MC: fatjetDetailStr+=" bosonCount"
subjetDetailStr="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr())

nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False

    histDetailStr  =""
    jetDetailStr   ="kinematic clean"
    photonDetailStr="kinematic"

    nameSuffix="/sys"+args.treeName[7:]
else:
   isSys=False

c = Config()

commonsel={"m_msgLevel"               : ROOT.MSG.INFO,
           "m_mc"                     : args.is_MC,
           "m_jetDetailStr"           : jetDetailStr,
           "m_fatjetDetailStr"        : fatjetDetailStr,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           }

#
# Process Ntuple
#
c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_msgLevel"               : ROOT.MSG.INFO,
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : jetDetailStr,
                                        "m_fatjetDetailStr"        : fatjetDetailStr,
                                        "m_subjetDetailStr"        : subjetDetailStr
                                        } )

#
# Different selections
config=commonsel.copy()
config.update({"m_name": 'higgseff',
               "m_fatjet0PtCut"   : 450,
               "m_trkjet0_MV2c10" : -2,
               "m_trkjet1_MV2c10" : -2,
               })
c.algorithm("HbbISREffAlgo", config )

config=commonsel.copy()
config.update({"m_name": 'higgseff_tjet77',
               "m_fatjet0PtCut"   : 450,
               "m_trkjet0_MV2c10" : 0.577664315700531,
               "m_trkjet1_MV2c10" : 0.577664315700531,
               })
c.algorithm("HbbISREffAlgo", config )
