import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

import itertools
import argparse
import shlex

###########################################
## Fine-grained user control
## Note: if absolutely nothing here
## is turned on you get zero histograms.
## This is probably not what you wanted.

extraargs=args.extra_options

config_parser = argparse.ArgumentParser()
config_parser.add_argument("--isSystTree", action='store_true', help='Treat this as a systematic tree')
config_parser.add_argument("--doSystsInsideTree", action='store_true', help='Look for systematics to run inside this tree')
conargs=config_parser.parse_args(args=shlex.split(extraargs))

# Is this a systematic tree?
isSys=conargs.isSystTree

# Is this the main tree, and we want to
# make systematically varied histograms
# like b-tagging SF's?
doSys=conargs.doSystsInsideTree

# If this is true you get extra trijet
# histograms in 50 GeV mjj slices.
# Also need it to get photon ID uncertainty hists though.
doDetails = False
if doSys :
  doDetails = True

# Add items you want to doHist list.
# Each individually documented.
# Impacts ZprimeResonanceHists 
# (things including our two jets)
# and ZprimeISRHists
# (things including all 3 objects,
# two resonance hists + ISR)

# If YOU want to do a study
# in one of these cases (our Z' 
# candidate or the 3 objects),
# those files are where you need to 
# add it, and you can add a switch
# here to control it (set the options
# in ZprimeHelperClasses.cxx).

doEventHists    = [
	"mass",           # mjj/mISRjj alone! your basic friends
        "kinematic",      # pT, eta, phi of jj/ISRjj system
#        "deltas",         # angles between jets or Z' and ISR etc
#        "rapidities",     # rapidity-related quantities
#        "asymmetries",    # various pT asymmetries etc
#        "softerjets",     # info on jets 3 and 4
#        "2d"              # whole lotta 2d plots
]


# Anything you turn on here you will get
# for all 4 leading jets.
doJetHists     = [
	"kinematic",       # Jet pT, eta, phi
#	"clean",           # Jet cleaning variables
#        "layer",          # Calo layer quantities per jet
#        "trackPV",        # Track width, pTtrk, JVT/JVF, etc
#        "energy",         # Energy v calorimeter layer info
#        "flavorTag",      # value of tagging quantities for jet
#        "charge",         # jet charge
#        "truth"           # various truth quantities
]

# B jet working points to look at.
# If you leave the list empty it'll be taken from commonconfig.
# If you explicitly say None no b-tagging will be performed
doTaggers = ["DL1"] #None #["DL1","MV2c10"]

# B taggers to look at.
# If you leave the list empty it'll be taken from commonconfig.
# If you explicitly say None no b-tagging will be performed
doWPs = [77] #None

# Hybrid or fixed efficiency working points, or both
# If you leave the list empty it'll be taken from commonconfig.
# If you explicitly say None no b-tagging will be performed
doModes = ['FixedCutBEff'] # None

# Anything you turn on here you will get
# for the leading photon.
doPhotonHists     = [
	"kinematic",       # Photon pT, eta, phi
#	"isolation",       # Photon isolation variables
#       "purity",          # Photon purity information
#       "PID",             # Photon ID
#       "effSF"            # Photon ID scale factors
]


###########################################
## Turn that into running something

doTagAnalysis = True
if doTaggers is None or doWPs is None or doModes is None :
  doTagAnalysis = False

if doTaggers :
  usebtags = doTaggers
else :
  usebtags = commonconfig.btaggers

if doWPs :
  useWPs = doWPs
else :
  useWPs = commonconfig.btagWPs

if doModes :
  useModes = doModes
else :
  useModes = commonconfig.btagmodes

histDetailStr = ""
for item in doEventHists :
  histDetailStr = histDetailStr + item + " "

jetDetailStr = ""
for item in doJetHists :
  jetDetailStr = jetDetailStr + item + " "

photonDetailStr = ""
for item in doPhotonHists :
  photonDetailStr = photonDetailStr + item + " "

#
# Load all of the branches in the TTree
jetDetailTreeStr="kinematic clean trackPV flavorTag"
if not isSys :
  jetDetailTreeStr = jetDetailTreeStr + "energy layer "
if doTagAnalysis :
  for btagger in usebtags:
    for btagmode in useModes :
      jetDetailTreeStr+=' jetBTag_{btagger}_{mode}_{btagWP}'.format(btagger=btagger,mode=btagmode,btagWP=''.join(['%d'%btagWP for btagWP in useWPs]))
# Don't have truth info in data or in systematically varied trees
if args.is_MC and not isSys: jetDetailTreeStr+=" truth"

photonDetailTreeStr='kinematic isolation PID effSF' # purity

nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False

    histDetailStr  ="mass"
    jetDetailStr   ="kinematic"
    photonDetailStr="kinematic"

    nameSuffix="/sys"+args.treeName[7:]

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetailStr,
           "m_jetDetailStr"           : jetDetailStr,
           "m_photonDetailStr"        : photonDetailStr,
           "m_doPUReweight"           : True,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True,
           "m_doSysts"                : doSys
           }

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : jetDetailTreeStr,
                                        "m_photonDetailStr"        : photonDetailTreeStr
                                        } )

triggers=[]
#triggers.append(('HLT_g60_loose',85,(25,)))
triggers.append(('HLT_g75_tight_3j50noL1_L1EM22VHI',95,(65,),''))
triggers.append(('HLT_g85_tight_L1EM22VHI_3j50noL1',95,(65,),''))
triggers.append(('HLT_g140_loose',150,(25,),''))

mjjCuts = {"HLT_g140_loose" : 169,
           "HLT_g75_tight_3j50noL1_L1EM22VHI" : 335,
           "HLT_g85_tight_L1EM22VHI_3j50noL1" : 335}

for trigger, photonPt, jetPts, tag in triggers:
    for jetPt,nbtags,btagWP,btagMode,btagger in itertools.product(jetPts,[0,1,2,3],useWPs,useModes,usebtags):
        if nbtags==3 and (btagWP!=useWPs[0] or btagger!=usebtags[0] or btagMode!=useModes[0]): continue

        btagstr='%s_%s_%02d'%(btagger,btagMode,btagWP)
        btagobj=getattr(ROOT.xAH.Jet,btagstr) if nbtags<3 else ROOT.xAH.Jet.None

        commonsel['m_trigger']     =trigger
        commonsel['m_reso0PtCut']  =jetPt
        commonsel['m_reso1PtCut']  =jetPt
        commonsel['m_nresoBtag']   =nbtags
        commonsel['m_resoBtagAlgo']=btagobj
        commonsel['m_minPhotonPt'] =photonPt
        
        # New things for photon ID
        # 0 = FIXEDCUTLOOSE
        # 1 = FIXEDCUTTIGHTCALOONLY
        # 2 = FIXEDCUTTIGHT
        commonsel['m_isolation']   = 2

        basename='dijetgamma_trig%s_g%d_2j%d'%(trigger,photonPt,jetPt)
        if(nbtags!=3): basename+='_nbtag{0}_{1}'.format(nbtags,btagstr)

        config=commonsel.copy()
        config.update({"m_name"                   : "%s%s"%(basename,nameSuffix+tag),
                       "m_doBCIDCheck"            : not args.is_MC,
                       "m_ystarCut"               : 0.75,
                       "m_mjjCut"                 : mjjCuts[trigger]})
        c.algorithm("ZprimeGammaJetJetHistsAlgo", config )

        # config=commonsel.copy()
        # config.update({"m_name"                   : "%s_noprw%s"%(basename,nameSuffix),
        #                "m_doPUReweight"           : False,
        #                "m_trigger"                : trigger,
        #                "m_ystarCut"               : 0.8,
        #                "m_dRISRclosejCut"         : 0.85,
        #                "m_mjjCut"                 : 160})
        # c.algorithm("ZprimeGammaJetJetHistsAlgo", config )

        # config=commonsel.copy()
        # config.update({"m_name"                   : "%s_nomjj%s"%(basename,nameSuffix),
        #                "m_doBCIDCheck"            : not args.is_MC,
        #                "m_ystarCut"               : 0.8,
        #                "m_dRISRclosejCut"         : 0.85})
        # c.algorithm("ZprimeGammaJetJetHistsAlgo", config )

        # config=commonsel.copy()
        # config.update({"m_name"                   : "%s_nocuts%s"%(basename,nameSuffix),
        #                "m_ystarCut"               : 99})
        # c.algorithm("ZprimeGammaJetJetHistsAlgo", config )

        # config=commonsel.copy()
        # config.update({"m_name"                   : "%s_ystar%s"%(basename,nameSuffix),
        #                "m_ystarCut"               : 0.8})
        # c.algorithm("ZprimeGammaJetJetHistsAlgo", config )

        # config=commonsel.copy()
        # config.update({"m_name"                   : "%s_ystar_dRISRclosej%s"%(basename,nameSuffix),
        #                "m_ystarCut"               : 0.8,
        #                "m_dRISRclosejCut"         : 0.85})
        # c.algorithm("ZprimeGammaJetJetHistsAlgo", config )

        # config=commonsel.copy()
        # config.update({"m_name"                   : "%s_dijetystar%s"%(basename,nameSuffix),
        #                "m_ystarCut"               : 0.6})
        # c.algorithm("ZprimeGammaJetJetHistsAlgo", config )
