import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

c = Config()

#usebtags = commonconfig.btaggers
usebtags = ["DL1"]

# For jets in nominal tree
jetDetailStr="kinematic clean trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr(btaggers=usebtags))
if args.is_MC: jetDetailStr+=" truth"
else : jetDetailStr+="energy layer "
# For jets in systematically varied trees: need enough to run full analysis. Don't need truth.
jetDetailStrSyst = "kinematic clean trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr(btaggers=usebtags))

# Lowest unprescaled single photon triggers:
# - HLT_g120_loose (2015)
# - HLT_g140_loose (2016)
# - HLT_g140_loose (2017)
# Lowest unprescaled photon + 3j (no overlap removal) triggers:
# - HLT_g20_loose_L1EM18VH_2j40_0eta490_3j25_0eta490_invm700 (2015) (yuck)
# - HLT_g75_tight_3j50noL1_L1EM22VHI (2016)
# - HLT_g85_tight_L1EM22VHI_3j50noL1 (2017)
# Prescaled single photon triggers also included for trigger turn-on studies.
commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 triggerSelection='HLT_g[0-9]+_loose|HLT_g[0-9]+_tight|HLT_g75_tight_3j50noL1_L1EM22VHI|HLT_g85_tight_L1EM22VHI_3j50noL1|HLT_g75_tight_3j[0-9]+noL1_L1EM22VHI|HLT_g20_loose_L1EM18VH_2j40_0eta490_3j25_0eta490_invm700',
                                 doJets=True,doPhotons=True,
                                 doSyst=False,
                                 btaggers=usebtags,btagmodes=commonconfig.btagmodes,btagWPs=commonconfig.btagWPs,
                                 args=args.extra_options)

SelectPhotons=commonconfig.findAlgo(c,"SelectPhotons")
SelectPhotons.m_pT_min=30

containers=ROOT.vector('ZprimeNtuplerContainer')()
# Second string is for systematics! If you want them to be the same as for nominal, need to say so.
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.PHOTON,'SignalPhotons','ph' ,'kinematic PID effSF isolation','kinematic PID effSF isolation'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJets'   ,'jet',jetDetailStr                          ,jetDetailStrSyst))

c.algorithm("ZprimeNtupler",          { "m_name"                : "AnalysisAlgo",
                                        "m_inputAlgo"           : "ORAlgo_Syst",
                                        "m_containers"          : containers,
                                        "m_eventDetailStr"      : "pileup",
                                        "m_trigDetailStr"       : "passTriggers passTrigBits"
                                        } )
