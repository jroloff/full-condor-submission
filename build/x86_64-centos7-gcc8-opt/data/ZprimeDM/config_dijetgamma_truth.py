import ROOT
from xAODAnaHelpers import Config

doDetails=True
histDetail="" #2d" #2d debug"

c = Config()

commonsel={"m_mc"                     : True,
           "m_debug"                  : False,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetail,
           "m_jetDetailStr"           : "kinematic",
           "m_photonDetailStr"        : "kinematic",
           "m_doPUReweight"           : False,
           "m_doCleaning"             : False,
           "m_doTrigger"              : False
           }

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "",
                                        "m_debug"                  : False,
                                        "m_mc"                     : True,
                                        "m_applyGRL"               : False,
                                        "m_doTruthOnly"            : True,
                                        "m_jetDetailStr"           : "kinematic",
                                        "m_photonDetailStr"        : "kinematic"
                                        } )

for photonPt in [85, 150]:
    for jetPt in [25, 50, 65, 75]:
        config=commonsel.copy()
        config.update({"m_name"                   : "dijetgamma_g%d_2j%d"%(photonPt,jetPt),
                       "m_reso0PtCut"             : jetPt,
                       "m_reso1PtCut"             : jetPt,
                       "m_minPhotonPt"            : photonPt,
                       "m_ystarCut"               : 0.8,
                       "m_dRISRclosejCut"         : 0.85,
                       "m_mjjCut"                 : 160})
        c.algorithm("ZprimeGammaJetJetHistsAlgo", config )

        config=commonsel.copy()
        config.update({"m_name"                   : "dijetgamma_g%d_2j%d_nomjj"%(photonPt,jetPt),
                       "m_reso0PtCut"             : jetPt,
                       "m_reso1PtCut"             : jetPt,
                       "m_minPhotonPt"            : photonPt,
                       "m_ystarCut"               : 0.8,
                       "m_dRISRclosejCut"         : 0.85})
        c.algorithm("ZprimeGammaJetJetHistsAlgo", config )

        config=commonsel.copy()
        config.update({"m_name"                   : "dijetgamma_g%d_2j%d_nocuts"%(photonPt,jetPt),
                       "m_reso0PtCut"             : jetPt,
                       "m_reso1PtCut"             : jetPt,
                       "m_minPhotonPt"            : photonPt})
        c.algorithm("ZprimeGammaJetJetHistsAlgo", config )
