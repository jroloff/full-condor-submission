from xAODAnaHelpers import Config
from ZprimeDM import commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

c = Config()

jetDetailStr="kinematic clean energy layer trackPV"
if args.is_MC: jetDetailStr+=" truth"

commonconfig.apply_common_config(c,isMC=args.is_MC,isAFII=args.is_AFII,triggerSelection='HLT_g35_loose_g25_loose',derivation='EXOT10',
                                 doJets=True,doPhotons=True)

SelectJets=commonconfig.findAlgo(c, "SelectJets")
SelectJets.m_pass_min=0

SelectPhotons=commonconfig.findAlgo(c,"SelectPhotons")
SelectPhotons.m_pT_min=30
SelectPhotons.m_pass_min=2

c.algorithm("ZprimeNtupler",          { "m_jetContainerName"    : "SignalJets",
                                        "m_photonContainerName" : "SignalPhotons",
                                        "m_inputAlgo"           : "OR_Algo",
                                        "m_eventDetailStr"      : "pileup",
                                        "m_trigDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"        : jetDetailStr,
                                        "m_jetDetailStrSyst"    : "kinematic clean",
                                        "m_photonDetailStr"     : "kinematic isolation PID effSF",
                                        "m_photonDetailStrSyst" : "kinematic"
                                        } )
