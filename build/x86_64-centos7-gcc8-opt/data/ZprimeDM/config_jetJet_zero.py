import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

import itertools

###########################################
## Fine-grained user control
## Note: if absolutely nothing here
## is turned on you get zero histograms.
## This is probably not what you wanted.

# Do systematics?
isSys=True
isSys=False

# If this is true you get extra trijet
# histograms in 50 GeV mjj slices
doDetails = False

# Add items you want to doHist list.
# Each individually documented.
# Impacts ZprimeResonanceHists 
# (things including our two jets)
# and ZprimeISRHists
# (things including all 3 objects,
# two resonance hists + ISR)

# If YOU want to do a study
# in one of these cases (our Z' 
# candidate or the 3 objects),
# those files are where you need to 
# add it, and you can add a switch
# here to control it (set the options
# in ZprimeHelperClasses.cxx).

doEventHists    = [
	"mass",           # mjj/mISRjj alone! your basic friends
        "kinematic",      # pT, eta, phi of jj/ISRjj system
        "deltas",         # angles between jets or Z' and ISR etc
        "rapidities",     # rapidity-related quantities
        "asymmetries",    # various pT asymmetries etc
#        "softerjets",     # info on jets 3 and 4
        "2d"              # whole lotta 2d plots
]


# Anything you turn on here you will get
# for all 4 leading jets.
doJetHists     = [
	"kinematic",       # Jet pT, eta, phi
#	"clean",           # Jet cleaning variables
#        "layer",          # Calo layer quantities per jet
#        "trackPV",        # Track width, pTtrk, JVT/JVF, etc
#        "energy",         # Energy v calorimeter layer info
#        "flavorTag",      # value of tagging quantities for jet
#        "charge",         # jet charge
#        "truth"           # various truth quantities
]

doFatJetHists     = [
	"kinematic",       # Jet pT, eta, phi
        "substructure"
]


# B taggers to look at.
# If you don't specify anything it'll be taken from commonconfig.
doTaggers = [""] #None
# B jet working points to look at.
# If you don't specify anything it'll be taken from commonconfig.
doWPs = [] #None
# Hybrid or fixed efficiency working points, or both
#doModes = ['FixedCutBEff','HybBEff'] # None
doModes = ['']
###########################################
## Turn that into running something

#doANNwps = [0, 5, 7]
doANNwps = [0]

dodR = [0, 12]

doMcut = [0]

if doTaggers :
  usebtags = doTaggers
else :
  usebtags = commonconfig.btaggers

if doWPs :
  useWPs = doWPs
else :
  useWPs = commonconfig.btagWPs

if doModes :
  useModes = doModes
else :
  useModes = commonconfig.btagmodes

histDetailStr = ""
for item in doEventHists :
  histDetailStr = histDetailStr + item + " "

jetDetailStr = ""
for item in doJetHists :
  jetDetailStr = jetDetailStr + item + " "

fatjetDetailStr = ""
for item in doFatJetHists :
  fatjetDetailStr = fatjetDetailStr + item + " "

for btagger in usebtags:
  for btagmode in useModes :
    jetDetailStr+=' jetBTag_{btagger}_{mode}_{btagWP}'.format(btagger=btagger,mode=btagmode,btagWP=''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))

# Load all of the branches in the TTree
jetDetailTreeStr="kinematic clean layer trackPV flavorTag"
jetDetailTreeStr="kinematic clean"
for btagger in commonconfig.btaggers:
  for btagmode in useModes :
    jetDetailTreeStr+=' jetBTag_{btagger}_{mode}_{btagWP}'.format(btagger=btagger,mode=btagmode,btagWP=''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))
if args.is_MC: jetDetailTreeStr+=" truth"

nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False

    jetDetailTreeStr   ="kinematic clean trackPV flavorTag"

    histDetailStr  ="mass"
    jetDetailStr   ="kinematic"

    nameSuffix="/sys"+args.treeName[7:]

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetailStr,
           "m_jetDetailStr"           : jetDetailStr,
           "m_fatjetDetailStr"        : fatjetDetailStr,
           "m_doPUReweight"           : False,
           "m_doCleaning"             : False,
           "m_jetPtCleaningCut"       : 0,
           "m_doTrigger"              : False,
           "m_doFatJets"              : True,
           "m_fatjetPtCleaningCut"    : 0,
#           "m_msgLevel"            : ROOT.MSG.DEBUG,
           }

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : jetDetailStr,
                                        "m_fatjetDetailStr"        : fatjetDetailStr,
#                                        "m_msgLevel"            : ROOT.MSG.DEBUG,
                                        } )

selections=[]
selections.append(('HLT_j380', 420, 0,''))
#selections.append(('HLT_j400',450, 450,''))
#selections.append(('HLT_j225_gsc400_boffperf_split',300, 300,''))
selections.append(('HLT_j380', 420, 0,''))
#selections.append(('HLT_j380',0, 0,''))
#selections.append(('HLT_j440_a10t_lcw_jes_L1J100',430, 450,''))

for trigger,jetPt, fatjetPt, tag in selections:
  for ANNwp in doANNwps:
    #    if ANNwp > 0:
    ANNtagstr='_ANNwp{0}'.format(ANNwp)
    #   else:
    #     ANNtagstr=''

    for dR in dodR:
      #     if dR > 0:
      dRtagstr='_mindRjJ{0}'.format(dR)
      #    else:
      #      dRtagstr=''
    
      commonsel['m_trigger']           =trigger
      commonsel['m_minLeadingJetPt']   =jetPt
      commonsel['m_minLeadingFatJetPt']=fatjetPt
      commonsel['m_minANNwp']=ANNwp/10.
      commonsel['m_mindRjJ']= dR/10.
      basename='jJ_trig{0}_j{1}_J{2}_{3}_{4}'.format(trigger,jetPt, fatjetPt, dRtagstr, ANNtagstr)

      jJ=commonsel.copy()
      jJ.update({"m_name"            : "{0}{1}".format(basename,nameSuffix+tag)} )
      
      c.algorithm("ZprimeISRBoostHistsAlgo", jJ )

  if trigger=='HLT_j380' and fatjetPt==0:
    for Mcut in doMcut:
      Mcuttagstr='_mJCut{0}'.format(Mcut)
      ANNtagstr='_ANNwp0'
      dRtagstr='_mindRjJ{0}'.format(12)
      #    else:                                                                                                                                       
      #      dRtagstr=''                                                                                                                      
      commonsel['m_trigger']           =trigger
      commonsel['m_minLeadingJetPt']   =jetPt
      commonsel['m_minLeadingFatJetPt']=fatjetPt
      commonsel['m_minANNwp']=0./10.
      commonsel['m_mindRjJ']= 12/10.
      commonsel['m_mJCut']= Mcut
      basename='jJ_trig{0}_j{1}_J{2}_{3}_{4}_{5}'.format(trigger,jetPt, fatjetPt, dRtagstr, ANNtagstr, Mcuttagstr)
      jJ=commonsel.copy()
      jJ.update({"m_name"            : "{0}{1}".format(basename,nameSuffix+tag)} )
      c.algorithm("ZprimeISRBoostHistsAlgo", jJ )
