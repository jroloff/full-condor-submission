import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

import itertools

###########################################
## Fine-grained user control
## Note: if absolutely nothing here
## is turned on you get zero histograms.
## This is probably not what you wanted.

# Do systematics?
isSys=True

# If this is true you get extra trijet
# histograms in 50 GeV mjj slices
doDetails = False

# Add items you want to doHist list.
# Each individually documented.
# Impacts ZprimeResonanceHists 
# (things including our two jets)
# and ZprimeISRHists
# (things including all 3 objects,
# two resonance hists + ISR)

# If YOU want to do a study
# in one of these cases (our Z' 
# candidate or the 3 objects),
# those files are where you need to 
# add it, and you can add a switch
# here to control it (set the options
# in ZprimeHelperClasses.cxx).

doEventHists    = [
	"mass",           # mjj/mISRjj alone! your basic friends
        "kinematic",      # pT, eta, phi of jj/ISRjj system
#        "deltas",         # angles between jets or Z' and ISR etc
#        "rapidities",     # rapidity-related quantities
#        "asymmetries",    # various pT asymmetries etc
#        "softerjets",     # info on jets 3 and 4
#        "2d"              # whole lotta 2d plots
]


# Anything you turn on here you will get
# for all 4 leading jets.
doJetHists     = [
	"kinematic",       # Jet pT, eta, phi
#	"clean",           # Jet cleaning variables
#        "layer",          # Calo layer quantities per jet
#        "trackPV",        # Track width, pTtrk, JVT/JVF, etc
#        "energy",         # Energy v calorimeter layer info
#        "flavorTag",      # value of tagging quantities for jet
#        "charge",         # jet charge
#        "truth"           # various truth quantities
]


# B taggers to look at.
# If you don't specify anything it'll be taken from commonconfig.
doTaggers = ["DL1"] #None

# B jet working points to look at.
# If you don't specify anything it'll be taken from commonconfig.
doWPs = [77] #None

# Hybrid or fixed efficiency working points, or both
doModes = ['FixedCutBEff','HybBEff'] # None

###########################################
## Turn that into running something

if doTaggers :
  usebtags = doTaggers
else :
  usebtags = commonconfig.btaggers

if doWPs :
  useWPs = doWPs
else :
  useWPs = commonconfig.btagWPs

if doModes :
  useModes = doModes
else :
  useModes = commonconfig.btagmodes

histDetailStr = ""
for item in doEventHists :
  histDetailStr = histDetailStr + item + " "

jetDetailStr = ""
for item in doJetHists :
  jetDetailStr = jetDetailStr + item + " "

for btagger in usebtags:
  for btagmode in useModes :
    jetDetailStr+=' jetBTag_{btagger}_{mode}_{btagWP}'.format(btagger=btagger,mode=btagmode,btagWP=''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))

# Load all of the branches in the TTree
jetDetailTreeStr="kinematic clean layer trackPV flavorTag"
for btagger in commonconfig.btaggers:
  for btagmode in useModes :
    jetDetailTreeStr+=' jetBTag_{btagger}_{mode}_{btagWP}'.format(btagger=btagger,mode=btagmode,btagWP=''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))
if args.is_MC: jetDetailTreeStr+=" truth"

nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False

    jetDetailTreeStr   ="kinematic clean trackPV flavorTag"

    histDetailStr  ="mass"
    jetDetailStr   ="kinematic"

    nameSuffix="/sys"+args.treeName[7:]

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetailStr,
           "m_jetDetailStr"           : jetDetailStr,
           "m_doPUReweight"           : True,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True,
#           "m_msgLevel"            : ROOT.MSG.DEBUG,
           }

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : jetDetailStr,
#                                        "m_msgLevel"            : ROOT.MSG.DEBUG,
                                        } )

selections=[]
selections.append(('HLT_j380',430,''))
selections.append(('HLT_j225_gsc400_boffperf_split',430,''))
#selections.append(('HLT_j400',450,''))
#selections.append(('HLT_3j200',50,'_3j200'))
#selections.append(('HLT_ht1000_L1J100',50,'_ht1000'))
#selections.append(('HLT_j100_2j55_bmv2c2060_split',50,'_j100_2j55_bmv2c2060'))
#selections.append(('HLT_2j55_bmv2c2060_split_ht300_L14J15',50,'_2j55_bmv2c2060_split_ht300'))

for trigger,jetPt,tag in selections:
    for nbtags,btagWP,btagMode,btagger in itertools.product([0,1,2,3],useWPs,useModes,usebtags):
        if nbtags==3 and (btagWP!=useWPs[0] or btagger!=usebtags[0] or btagMode!=useModes[0]): continue

        btagstr='%s_%s_%02d'%(btagger,btagMode,btagWP)
        btagobj=getattr(ROOT.xAH.Jet,btagstr) if nbtags<3 else ROOT.xAH.Jet.None

        commonsel['m_trigger']        =trigger
        commonsel['m_reso0PtCut']     =25
        commonsel['m_reso1PtCut']     =25
        commonsel['m_nresoBtag']      =nbtags
        commonsel['m_resoBtagAlgo']   =btagobj
        commonsel['m_minLeadingJetPt']=jetPt

        basename='trijet_trig{0}_j{1}_2j25'.format(trigger,jetPt)
        if(nbtags!=3): basename+='_nbtag{0}_{1}'.format(nbtags,btagstr)

        trijet23=commonsel.copy()
        trijet23.update({"m_name"            : "{0}{1}".format(basename,nameSuffix+tag),
                         "m_ystarCut"        : 0.75})
        c.algorithm("ZprimeTrijetHistsAlgo", trijet23 )

