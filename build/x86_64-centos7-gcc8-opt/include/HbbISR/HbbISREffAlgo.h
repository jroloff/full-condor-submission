#ifndef HbbISR_HbbISREffAlgo_H
#define HbbISR_HbbISREffAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>
#include <ZprimeDM/CutflowHists.h>

#include <HbbISR/HbbISRHists.h>

#include <sstream>
#include <vector>

class HbbISREffAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
  bool m_debug;
  bool m_mc;

  // switches
  std::string m_jetDetailStr;
  std::string m_fatjetDetailStr;

  bool m_doCleaning;
  float m_jetPtCleaningCut;
  float m_fatjet0PtCut;

  // Kinematic selection
  std::string m_trkJet;
  float m_trkjet0_MV2c10;
  float m_trkjet1_MV2c10;

private:

  //
  // Cutflow
  int m_cf_cleaning;
  int m_cf_fatjet0;

  //
  // Histograms
  ZprimeDM::FatJetHists *h_hjet; //!
  ZprimeDM::JetHists *h_bjet0; //!
  ZprimeDM::JetHists *h_bjet1; //!

  TH1 *h_hjet_idx; //!
  TH1 *h_hjet_2trkidx; //!
  TH2 *h_bjet_idx; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  HbbISREffAlgo ();

  // these are the functions inherited from Algorithm
  EL::StatusCode histInitialize ();
  EL::StatusCode execute ();
  EL::StatusCode histFill (float eventWeight);
  EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISREffAlgo, 1);
};

#endif // HbbISR_HbbISREffAlgo_H
