#ifndef HbbISR_HBBISRHELPERCLASSES_H
#define HbbISR_HBBISRHELPERCLASSES_H

#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODAnaHelpers/FatJet.h>

namespace HbbISRHelperClasses 
{

  /*! \brief Settings for dijet+ISR histograms.
   */
  class HbbISRInfoSwitch : public HelperClasses::InfoSwitch 
  {
  public:
    bool m_mass    =false;
    bool m_pt      =false;
    bool m_truthtag=false;
    HbbISRInfoSwitch(const std::string& configStr) : InfoSwitch(configStr) { initialize(); };
  protected:
    void initialize();
  };

  class FatJetSorter 
  {
  public:
    enum Mode {Pt, D2};

    FatJetSorter(Mode mode);

    bool operator()(const xAH::FatJet* a, const xAH::FatJet* b);

  private:
    Mode m_mode;

  public:
    static FatJetSorter ptsort;
    static FatJetSorter d2sort;
  };


} // close namespace HbbISRHelperClasses

# endif
