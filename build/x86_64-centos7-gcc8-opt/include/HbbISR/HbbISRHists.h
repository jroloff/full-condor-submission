#ifndef HbbISR_HbbISRHists_H
#define HbbISR_HbbISRHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/EventHists.h>
#include <ZprimeDM/JetHists.h>
#include <ZprimeDM/MuonHists.h>
#include <ZprimeDM/ElectronHists.h>
#include <ZprimeDM/FatJetHists.h>
#include <ZprimeDM/ZprimeResonanceHists.h>

#include <HbbISR/HbbHists.h>

class HbbISRHists : public EventHists
{
public:

  HbbISRHists(const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& muonDetailStr, const std::string& electronDetailStr, const std::string& fatjetDetailStr, const std::string& trkJet, const std::string& subjetDetailStr);
  virtual ~HbbISRHists() ;

  virtual void record(EL::IWorker *wk);

  StatusCode initialize();
  StatusCode execute(const DijetISREvent& event, const xAH::FatJet *Hcand, const xAH::FatJet *nonHcand, float eventWeight);
  StatusCode finalize();
  using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

private:
  //
  // histograms
  std::string m_detailStr;

  TH1 *h_fatjetmass_leading;
  TH1 *h_fatjetmass_tau21;
  TH1 *h_fatjetmass_btag;

  TH1F* h_HcandIdx = nullptr;
  ZprimeResonanceHists *m_vbf;
  HbbHists *m_hbb;

  TH1F*     h_Zprime_pt;

  std::string m_jetDetailStr;
  TH1F*     h_nJet;
  ZprimeDM::JetHists *m_jet0;
  ZprimeDM::JetHists *m_jet1;
  ZprimeDM::JetHists *m_jet2;
  ZprimeDM::JetHists *m_jet3;

  std::string m_muonDetailStr;
  TH1F*     h_nMuon;
  ZprimeDM::MuonHists *m_muon0;
  ZprimeDM::MuonHists *m_muon1;
  ZprimeDM::MuonHists *m_muon2;

  std::string m_electronDetailStr;
  TH1F*     h_nElectron;
  ZprimeDM::ElectronHists *m_electron0;
  ZprimeDM::ElectronHists *m_electron1;
  ZprimeDM::ElectronHists *m_electron2;

  std::string m_fatjetDetailStr;
  TH1F*     h_nFatJet              =nullptr;
  ZprimeDM::FatJetHists *m_Hcand;
  ZprimeDM::FatJetHists *m_nonHcand;
  ZprimeDM::FatJetHists *m_fatjet0 =nullptr;
  ZprimeDM::FatJetHists *m_fatjet1 =nullptr;
  ZprimeDM::FatJetHists *m_fatjet2 =nullptr;

  std::string m_trkJet;
  std::string m_subjetDetailStr;
  TH1F*     h_nTrkJet;
  ZprimeDM::JetHists *m_trkjet0;
  ZprimeDM::JetHists *m_trkjet1;
  ZprimeDM::JetHists *m_trkjet2;

  TH1F*     h_Hcandmuon0_dPhi =nullptr;
};

#endif // HbbISR_HbbISRHists_H
