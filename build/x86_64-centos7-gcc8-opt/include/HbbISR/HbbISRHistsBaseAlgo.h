#ifndef HbbISR_HbbISRHistsBaseAlgo_H
#define HbbISR_HbbISRHistsBaseAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>
#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/JMRUncertaintyAlgo.h>

#include <HbbISR/HbbISRHelperClasses.h>
#include <HbbISR/HbbISRHists.h>

#include <sstream>
#include <vector>

class HbbISRHistsBaseAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  enum BTaggerAlgo {
    None,
    MV2c10,MV2c10mu,MV2c10rnn,
    DL1,DL1mu,DL1rnn
  };

  //configuration variables
  bool m_debug;
  bool m_mc;

  // switches
  std::string m_histDetailStr;
  std::string m_jetDetailStr;
  std::string m_fatjetDetailStr;
  std::string m_subjetDetailStr;
  std::string m_muonDetailStr;
  std::string m_electronDetailStr;

  JMRUncertaintyAlgo::VARIATIONS m_JMRuncert = JMRUncertaintyAlgo::NOMINAL;
  bool m_doPUReweight;
  bool m_doCleaning = false;
  float m_jetPtCleaningCut = 25.;

  // trigger config
  bool m_doTrigger = false;
  /** @brief Comma-separated list of triggers that must pass (OR) */
  std::string m_trigger;
  /** @brief Reference trigger to trigger turn on curves.

      Added to m_trigger as AND. Also requires that none of the triggers in m_trigger
      have been prescaled away.
   */
  std::string m_refTrigger;
  /** @brief Make denominator for trigger efficiency curve
   *
   * Ignores the decision of m_trigger, but still requires that none of them have been
   * prescaled away.
   */
  bool m_trigEffDen;

  // Kinematic selection
  float m_jet0PtCut;
  float m_fatjet0PtCut;
  float m_fatjet1PtCut;

  std::string m_trkJet; // Name of the track jet collection

  float m_HcandIdxCut=-1; // Index of Z' candidate in large R jet collection
  float m_HcandPtCut = 0; // pT cut on Z' candidate 
  float m_HcandMCut  = 0; // m cut on Z' candidate
  bool m_doBoostCut; // Enforce 2*m/pt<1

  bool m_blindTTbar; // blind 150-200 GeV

  bool m_doTTbarMuonCR  =false; // Muon Requirement for ttbar control region
  bool m_vetoTTbarMuonCR=false; // Veto Muon Requirement for ttbar control region
  float m_muonpT    =40; // (ttbarCR) pt requirment on muon associated with fatjet opposite Hcand
  float m_nBjet_opp = 1; // (ttbarCR) number of b-jets we expect to find in fatjet opposite Hcand

private:
  // Function that returns decision of whether or not the given trackjet passes the tight b-tagging requirement
  bool btag_muon(const xAH::Jet* trkjet) const;

  //
  // Cutflow
  int m_cf_trigger;
  int m_cf_cleaning;
  int m_cf_jet0;
  int m_cf_fatjet0;
  int m_cf_fatjet1;

  // ttbar CR
  int m_cf_TTbar_muonPhi; //Muon phi Requirement for ttbar control region
  int m_cf_TTbar_muonPt; //Muon pt Requirement for ttbar control region
  int m_cf_TTbar_muonDeltaR; //Muon muon-ISRjet deltaR Requirement for ttbar control region
  int m_cf_TTbar_muonFatJetPt; // require FatJet near muon to have greater than 25GeV
  int m_cf_TTbar_FatJetHiggsPhi; // require fatjet_opp and Hcand are in opposite hemispheres
  int m_cf_TTbar_SufficientNFatJets; // require at least 1 fatjet near the muon
  int m_cf_TTbar_SufficientNbJets; // cut on number of b-jets we want to find in the FatJet opposite the Hcand

  // ttbar CR veto
  int m_cf_TTbar_muon; //Muon pt/phi Requirement for ttbar control region

  // Hcand selection cuts
  int m_cf_hcandidx;
  int m_cf_hcandpt;
  int m_cf_hcandm;
  // Hcand btagging cuts
  int m_cf_boost;

  //
  // Histograms
  HbbISRHelperClasses::HbbISRInfoSwitch *m_details; //!

  HbbISRHists *hIncl; //!

  HbbISRHists *hM0to50;    //!
  HbbISRHists *hM50to100;  //!
  HbbISRHists *hM75to125;  //!
  HbbISRHists *hM100to150; //!
  HbbISRHists *hM125to175; //!
  HbbISRHists *hM150to200; //!
  HbbISRHists *hM175to225; //!
  HbbISRHists *hM200to250; //!
  HbbISRHists *hM225to275; //!
  HbbISRHists *hM250to300; //!

  HbbISRHists *hPt[10]; //!

  HbbISRHists *hBB=nullptr; //!
  HbbISRHists *hCC=nullptr; //!
  HbbISRHists *hLL=nullptr; //!
  HbbISRHists *hBC=nullptr; //!
  HbbISRHists *hBL=nullptr; //!
  HbbISRHists *hCL=nullptr; //!
  HbbISRHists *hB =nullptr; //!
  HbbISRHists *hC =nullptr; //!
  HbbISRHists *hL =nullptr; //!
  HbbISRHists *hXX=nullptr; //!

  //
  // TTree
  std::string m_treeName;
  std::string m_treeDirectory;

  TTree *m_tree =nullptr; //!
  float br_eventWeight;
  uint32_t br_mcChannelNumber;
  TLorentzVector br_Hcand_p4;
  

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

  //
  // Parsed configuration
  std::vector<std::string> m_triggers;

  // ISR selection
  float m_eventWeight;
  const xAH::FatJet *m_Hcand; //!
  const xAH::FatJet *m_nonHcand; //!

  // ttbar CR objects
  const xAH::Muon* ttbar_muon; //!

  //
  // functions
  virtual void initHcandCutflow() =0;
  virtual void initBtagCutflow() =0;
  virtual bool doHcandCutflow() =0;
  virtual bool doBtagCutflow() =0;

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  HbbISRHistsBaseAlgo ();

  // these are the functions inherited from Algorithm
  EL::StatusCode setupJob (EL::Job& job);
  EL::StatusCode histInitialize ();
  EL::StatusCode changeInput (bool firstFile);
  EL::StatusCode initialize ();
  EL::StatusCode execute ();
  EL::StatusCode histFill (float eventWeight);
  EL::StatusCode treeFill (float eventWeight);
  EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRHistsBaseAlgo, 1);
};

#endif // HbbISR_HbbISRHistsBaseAlgo_H
