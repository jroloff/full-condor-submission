#ifndef HbbISR_HbbISRTightLooseHistsAlgo_H
#define HbbISR_HbbISRTightLooseHistsAlgo_H

// algorithm wrapper
#include <HbbISR/HbbISRHistsBaseAlgo.h>

class HbbISRTightLooseHistsAlgo : public HbbISRHistsBaseAlgo
{
public:
  uint m_nTrackJetsCut = 2; // Minimum number of track jets to define the Higgs candidate
  bool m_sortD2        = false; // Sort large R jets by D2 instead of pT
  float m_trkjet1PtCut; // Minimum pT of subleading track jet
  xAH::Jet::BTaggerOP m_bTagWPtight; // Btagging working point. (tight)
  xAH::Jet::BTaggerOP m_bTagWPloose; // Btagging working point. (loose)

  int m_tightBtag0; // -1 fail tight, 0 no decision, 1 pass tight
  int m_tightBtag1; // -1 fail tight, 0 no decision, 1 pass tight

  int m_looseBtag0; // -1 fail loose, 0 no decision, 1 pass loose
  int m_looseBtag1; // -1 fail loose, 0 no decision, 1 pass loose

protected:
  virtual void initHcandCutflow();
  virtual void initBtagCutflow();
  virtual bool doHcandCutflow();
  virtual bool doBtagCutflow();

public:
  // this is a standard constructor
  HbbISRTightLooseHistsAlgo ();

private:
  //
  // cutflow
  int m_cf_nfatjets;
  int m_cf_hcand;

  int m_cf_btag0;
  int m_cf_btag1;

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRTightLooseHistsAlgo, 1);
};

#endif // HbbISR_HbbISRTightLooseHistsAlgo_H
