#ifndef HbbISR_HbbISRTruthHistsAlgo_H
#define HbbISR_HbbISRTruthHistsAlgo_H

// algorithm wrapper
#include <HbbISR/HbbISRHistsBaseAlgo.h>

class HbbISRTruthHistsAlgo : public HbbISRHistsBaseAlgo
{
public:
  enum HcandMode {Truth, LeadJet};

  //
  // Options
  HcandMode m_Hcand_mode = Truth;

protected:
  virtual void initHcandCutflow();
  virtual void initBtagCutflow();
  virtual bool doHcandCutflow();
  virtual bool doBtagCutflow();

public:
  // this is a standard constructor
  HbbISRTruthHistsAlgo ();

private:
  //
  // cutflow
  int m_cf_nfatjets;
  int m_cf_match;

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRTruthHistsAlgo, 1);
};

#endif // HbbISR_HbbISRTruthHistsAlgo_H
