#ifndef ZprimeDM_DebugHists_H
#define ZprimeDM_DebugHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <ZprimeDM/DijetISREvent.h>

class DebugHists : public HistogramManager
{
public:
  DebugHists(const std::string& name, const std::string& detailStr="");
  virtual ~DebugHists();

  StatusCode initialize();
  StatusCode execute(const xAH::Jet* reso0, const xAH::Jet* reso1, const xAH::Particle* ISR, float eventWeight);
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  TH2F *h_reso1_ptvsreso0_pt;
  TH2F *h_isr_ptvsreso0_pt;
  TH2F *h_isr_ptvsreso1_pt;

  TH2F *h_dPhijjvsreso0_pt;
  TH2F *h_dPhijjvsreso1_pt;
  TH2F *h_dPhijjvsisr_pt;
  TH2F *h_dPhijjvsptjj;

  TH2F *h_dPhijjvsasymjj;
  TH2F *h_dPhijjvsasymISR1;
  TH2F *h_dPhijjvsasymISR2;
  TH2F *h_dPhijjvsprojasymjj;

  TH2F *h_dPhijjvsdRISRclosej;

  TH2F *h_asymjjvsreso0_pt;
  TH2F *h_asymjjvsreso1_pt;
  TH2F *h_asymjjvsisr_pt;
  TH2F *h_asymjjvsptjj;

  TH2F *h_asymjjvsdRISRclosej;

  TH2F *h_projasymjjvsreso0_pt;
  TH2F *h_projasymjjvsreso1_pt;
  TH2F *h_projasymjjvsisr_pt;
  TH2F *h_projasymjjvsptjj;

  TH2F *h_projasymjjvsdRISRclosej;

  TH2F *h_mjjvsreso0_pt;
  TH2F *h_mjjvsreso1_pt;
  TH2F *h_mjjvsisr_pt;

  TH2F *h_mjjvsreso0_eta;
  TH2F *h_mjjvsreso1_eta;
  TH2F *h_mjjvsisr_eta;

  TH2F *h_mjjvsdPhijj;
  TH2F *h_mjjvsdRjj;
  TH2F *h_mjjvsptjj;

  TH2F *h_mjjvsdRISRclosej;

  TH2F *h_mjjvsasymjj;
  TH2F *h_mjjvsasymISR1;
  TH2F *h_mjjvsasymISR2;
  TH2F *h_mjjvsprojasymjj;

};

#endif
