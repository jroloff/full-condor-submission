#ifndef ZprimeDM_DijetISRHists_H
#define ZprimeDM_DijetISRHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/ZprimeHelperClasses.h>
#include <ZprimeDM/EventHists.h>
#include <ZprimeDM/JetHists.h>
#include <ZprimeDM/PhotonHists.h>
#include <ZprimeDM/ZprimeResonanceHists.h>
#include <ZprimeDM/ZprimeTruthResonanceHists.h>
#include <ZprimeDM/ZprimeISRHists.h>
#include <ZprimeDM/DebugHists.h>

class DijetISRHists : public EventHists
{
public:

  DijetISRHists(const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& photonDetailStr);
  virtual ~DijetISRHists() ;

  virtual void record(EL::IWorker *wk);

  StatusCode initialize();
  StatusCode execute(const DijetISREvent& event, const xAH::Jet* reso0, const xAH::Jet* reso1, const xAH::Particle* isr, float eventWeight);
  StatusCode finalize();
  using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

private:
  ZprimeHelperClasses::DijetISRInfoSwitch m_infoSwitch;

  // histograms
  std::string m_detailStr;
  ZprimeResonanceHists      *m_Zprime;
  ZprimeTruthResonanceHists *m_ZprimeTruth;
  ZprimeISRHists            *m_ZprimeISR;
  DebugHists                *m_dbg;

  std::string m_jetDetailStr;
  TH1F*     h_nJet;
  ZprimeDM::JetHists *m_jet0;
  ZprimeDM::JetHists *m_jet1;
  ZprimeDM::JetHists *m_jet2;
  ZprimeDM::JetHists *m_jet3;

  std::string m_photonDetailStr;
  TH1F*     h_nPhoton;
  ZprimeDM::PhotonHists *m_photon0;
};

#endif // ZprimeDM_DijetISRHists_H
