#ifndef ZprimeDM_DiphotonHistsAlgo_H
#define ZprimeDM_DiphotonHistsAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>

#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/DiphotonHists.h>

// ROOT include(s):
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TLorentzVector.h"

#include <sstream>
#include <vector>

using namespace std;



class DiphotonHistsAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
  bool m_debug;
  bool m_mc;

  // switches
  std::string m_jetDetailStr;
  std::string m_photonDetailStr;
  bool m_doTruthOnly;
  bool m_doPUReweight;           
  bool m_doCleaning;
  float m_jetPtCleaningCut;

  // trigger config
  bool m_doTrigger;  
  bool m_dumpTrig;
  std::string m_trigger;

  // Kinematic selection
  float m_photon0PtCut;
  float m_photon1PtCut;

  bool m_scalarCuts;
  float m_YStarCut;
private:

  //
  // Cutflow
  int m_cf_trigger;
  int m_cf_cleaning;
  // Kinematic cuts
  int m_cf_photon0;
  int m_cf_photon1;

  // Scalar cuts
  int m_cf_photon0maa;
  int m_cf_photon1maa;

  int m_cf_ystar;

  //
  // Triggers
  std::vector<std::string> m_triggers;

  //
  // Histograms
  DiphotonHists *hIncl;       //!
  DiphotonHists *hMaa600_700; //!
  DiphotonHists *hMaa700_800; //!
  DiphotonHists *hMaa800_900; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  DiphotonHistsAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(DiphotonHistsAlgo, 1);
};

#endif
