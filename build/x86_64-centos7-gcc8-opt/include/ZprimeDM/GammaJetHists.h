#ifndef ZprimeDM_GammaJetHists_H
#define ZprimeDM_GammaJetHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/EventHists.h>
#include <ZprimeDM/JetHists.h>
#include <ZprimeDM/PhotonHists.h>
#include <ZprimeDM/ResonanceHists.h>

class GammaJetHists : public EventHists
{
public:

  GammaJetHists(const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& photonDetailStr);
  virtual ~GammaJetHists() ;

  virtual void record(EL::IWorker *wk);

  virtual StatusCode initialize();
  virtual StatusCode execute(const DijetISREvent& event, const xAH::Photon* photon0, const xAH::Jet* jet0, float eventWeight);
  virtual StatusCode finalize();
  using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

private:
  //histograms
  ResonanceHists     *m_reso;

  std::string m_jetDetailStr;
  TH1F*     h_nJet;
  ZprimeDM::JetHists *m_jet0;
  ZprimeDM::JetHists *m_jet1;

  std::string m_photonDetailStr;
  TH1F*     h_nPhoton;
  ZprimeDM::PhotonHists *m_photon0;
};

#endif // ZprimeDM_GammaJetHists_H
