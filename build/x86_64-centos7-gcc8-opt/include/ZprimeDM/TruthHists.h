#ifndef ZprimeDM_TruthHists_H
#define ZprimeDM_TruthHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/DijetISREvent.h>

namespace ZprimeDM 
{
  class TruthHists : public HistogramManager
  {
  public:

    TruthHists(const std::string& name, const std::string& detailStr, const std::string& prefix="");
    virtual ~TruthHists() ;

    bool m_debug;
    virtual StatusCode initialize();

    StatusCode execute(const xAH::TruthPart* truth, float eventWeight);
    using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
    using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

  private:
    HelperClasses::TruthInfoSwitch m_infoSwitch;

    std::string m_prefix;

    //histograms

    // kinematic
    TH1F* h_pt;
    TH1F* h_pt_m;
    TH1F* h_pt_l;
    TH1F* h_eta;
    TH1F* h_phi;

    // always
    TH1F* h_pdgId;
  };
}

#endif // ZprimeDM_TruthHists_H
