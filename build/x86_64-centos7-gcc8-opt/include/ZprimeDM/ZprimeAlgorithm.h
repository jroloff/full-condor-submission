#ifndef ZprimeDM_ZprimeAlgorithm_H
#define ZprimeDM_ZprimeAlgorithm_H

// algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>

#include <TH1D.h>

class ZprimeAlgorithm : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

private:
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

  TH1D *m_cutflowHist;      //!
  TH1D *m_cutflowHistW;     //!

  TH1D *m_cutflowHistOrig;  //!
  TH1D *m_cutflowHistWOrig; //!
  int   m_cutflowBinsOrig;  //!
  int   m_cutflowBinsWOrig; //!

protected:
  EL::StatusCode copy_cutflow();
  int init_cutflow(const std::string& cutName);
  EL::StatusCode cutflow(int cut);
  EL::StatusCode write_cutflow(const std::string m_fileName, const std::string histName);

  float event_weight() const;

public:
  // this is a standard constructor
  ZprimeAlgorithm (const std::string &name);

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeAlgorithm, 1);
};

#endif
