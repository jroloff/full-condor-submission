#ifndef ZprimeDM_ZPRIMEHELPERCLASSES_H
#define ZprimeDM_ZPRIMEHELPERCLASSES_H

#include <map>
#include <iostream>

#include "TString.h"

/* stuff below is for templating getContainer */
#include <RootCoreUtils/ThrowMsg.h>
#include <AthContainers/ConstDataVector.h>

#include <xAODRootAccess/TEvent.h>
#include <xAODRootAccess/TStore.h>

#include <xAODEventInfo/EventInfo.h>

#include <xAODAnaHelpers/TruthPart.h>
#include <xAODAnaHelpers/HelperClasses.h>

namespace ZprimeHelperClasses 
{

  /*! \brief Settings for dijet+ISR histograms.
   */
  class DijetISRInfoSwitch : public HelperClasses::InfoSwitch 
  {
  public:
    bool m_mass;
    bool m_kinematic;
    bool m_deltas;
    bool m_rapidities;
    bool m_asymmetries;
    bool m_softerjets;
    bool m_truthz;
    bool m_2d;
    bool m_debug;
    DijetISRInfoSwitch(const std::string& configStr) : InfoSwitch(configStr) { initialize(); };
  protected:
    void initialize();
  };

  struct TruthSorter 
  {
    bool operator()(const xAH::TruthPart* a, const xAH::TruthPart* b)
    {   
      return a->p4.Pt() > b->p4.Pt();
    }

    static TruthSorter sort;
  };

} // close namespace ZprimeHelperClasses


# endif
