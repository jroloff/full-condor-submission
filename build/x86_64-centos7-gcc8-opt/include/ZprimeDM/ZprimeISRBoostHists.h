#ifndef ZprimeDM_ZprimeISRBoostHists_H
#define ZprimeDM_ZprimeISRBoostHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODJet/Jet.h>
#include <xAODAnaHelpers/ParticleContainer.h>
#include <xAODAnaHelpers/FatJetContainer.h>

#include <ZprimeDM/ZprimeHelperClasses.h>
#include <ZprimeDM/DijetISREvent.h>

class ZprimeISRBoostHists : public HistogramManager
{
public:
  ZprimeISRBoostHists(const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& fatjetDetailStr);
  virtual ~ZprimeISRBoostHists();

  StatusCode initialize();
  StatusCode execute(const DijetISREvent& event,const xAH::FatJet* fatjet, const xAH::Particle* ISR, float eventWeight);
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload
private:
  ZprimeHelperClasses::DijetISRInfoSwitch m_infoSwitch;
  bool m_fatjetDetails;
  bool m_jetDetails;

  // histograms testing
  TH2F* h_J_phiveta_block;
  TH1F* h_J_eta_etastudies_highpt_highmass_block;
  TH1F* h_J_eta_etastudies_highpt_lowmass_block;
  TH1F* h_J_eta_etastudies_lowpt_highmass_block;
  TH1F* h_J_eta_etastudies_lowpt_lowmass_block;

  TH1F* h_J_eta_etastudies_650pt_block;
  TH1F* h_J_eta_etastudies_550pt_block;
  TH1F* h_J_eta_etastudies_500pt_block;
  TH1F* h_J_eta_etastudies_450pt_block;
  TH1F* h_J_eta_etastudies_400pt_block;

  TH1F* h_smallj_eta_etastudies_highpt_highmass_block;
  TH1F* h_smallj_eta_etastudies_highpt_lowmass_block;
  TH1F* h_smallj_eta_etastudies_lowpt_highmass_block;
  TH1F* h_smallj_eta_etastudies_lowpt_lowmass_block;

  int entrycount=0;
  int entrycount1=0;
  int entrycount2=0;
  int entrycount3=0;
  int entrycount4=0;
  TH1F* h_J_eta_etastudies_11_13_block;
  TH1F* h_J_eta_etastudies_9_11_block;
  TH1F* h_J_eta_etastudies_7_9_block;
  TH1F* h_J_eta_etastudies_5_7_block;
  TH1F* h_J_phi_block_data;
  TH1F* h_J_eta_block_data;
  TH1F* h_J_phi_block_mc;
  TH1F* h_J_eta_block_mc;
  TH1F* h_NPV;
  TH1F* h_J_eta_etastudies_highpt;
 TH1F* h_smallj_eta_etastudies_highpt;
 TH1F* h_J_eta_etastudies_lowpt;
 TH1F* h_smallj_eta_etastudies_lowpt;
 TH1F* h_J_eta_etastudies_highmass;
 TH1F* h_smallj_eta_etastudies_highmass;
 TH1F* h_J_eta_etastudies_lowmass;
 TH1F* h_smallj_eta_etastudies_lowmass;
 TH1F* h_J_eta_etastudies_highpt_block;
 TH1F* h_smallj_eta_etastudies_highpt_block;
 TH1F* h_J_eta_etastudies_lowpt_block;
 TH1F* h_smallj_eta_etastudies_lowpt_block;
 TH1F* h_J_eta_etastudies_highmass_block;
 TH1F* h_smallj_eta_etastudies_highmass_block;
 TH1F* h_J_eta_etastudies_lowmass_block;
 TH1F* h_smallj_eta_etastudies_lowmass_block;
TH1F* h_J_tau21_block;
TH1F* h_J_tau21ddt_block;
TH1F* h_J_m_block;
TH1F* h_J_pt_block;
TH1F* h_smallj_m_block;
TH1F* h_smallj_pt_block;
TH1F* h_J_eta_block;
TH1F* h_J_phi_block;
TH1F* h_smallj_eta_block;
TH1F* h_smallj_phi_block;
  TH1F* h_J_tau21ddt_flat;
  TH1F* h_J_tau21_flat;
  TH1F* h_J_m_rescale2;
  TH1F* h_J_pt_rescale2;
  TH1F* h_J_eta_rescale2;
  TH1F* h_J_phi_rescale2;
  TH1F* h_smallj_m_rescale2;
  TH1F* h_smallj_pt_rescale2;
  TH1F* h_smallj_eta_rescale2;
  TH1F* h_smallj_phi_rescale2;
  TH1F* h_J_m_rescale2_flat;
  TH1F* h_J_pt_rescale2_flat;
  TH1F* h_J_eta_rescale2_flat;
  TH1F* h_J_phi_rescale2_flat;
  TH1F* h_smallj_m_rescale2_flat;
  TH1F* h_smallj_pt_rescale2_flat;
  TH1F* h_smallj_eta_rescale2_flat;
  TH1F* h_smallj_phi_rescale2_flat;
  TH1F* h_smallj_m_flat;
  TH1F* h_smallj_pt_flat;
  TH1F* h_smallj_eta_flat;
  TH1F* h_smallj_phi_flat;

  TH1F* h_J_m_flat;
  TH1F* h_J_pt_flat;
  TH1F* h_J_rho_flat;
  TH1F* h_J_eta_flat;
  TH1F* h_J_phi_flat;

  TH1F* h_J_m_negpeak;
  TH1F* h_J_pt_negpeak;
  TH1F* h_J_rho_negpeak;
  TH1F* h_J_eta_negpeak;
  TH1F* h_J_phi_negpeak;
    
    TH1F* h_J_dphi_negpeak;
    TH1F* h_J_dR_negpeak;
    TH2F* h_J_phiveta_negpeak;
    TH1F* h_smallj_m_negpeak;
    TH1F* h_smallj_pt_negpeak;
    TH1F* h_smallj_eta_negpeak;
    TH1F* h_smallj_phi_negpeak;

    
  TH1F* h_J_m_pospeak;
  TH1F* h_J_pt_pospeak;
  TH1F* h_J_rho_pospeak;
  TH1F* h_J_eta_pospeak;
  TH1F* h_J_phi_pospeak;
    
    TH1F* h_J_dphi_pospeak;
    TH1F* h_J_dR_pospeak;
    TH2F* h_J_phiveta_pospeak;
    TH1F* h_smallj_m_pospeak;
    TH1F* h_smallj_pt_pospeak;
    TH1F* h_smallj_eta_pospeak;
    TH1F* h_smallj_phi_pospeak;

  TH1F* h_J_m_valley;
  TH1F* h_J_pt_valley;
  TH1F* h_J_rho_valley;
  TH1F* h_J_eta_valley;
  TH1F* h_J_phi_valley;
    
    TH1F* h_J_dphi_valley;
    TH1F* h_J_dR_valley;
    TH2F* h_J_phiveta_valley;
    TH1F* h_smallj_m_valley;
    TH1F* h_smallj_pt_valley;
    TH1F* h_smallj_eta_valley;
    TH1F* h_smallj_phi_valley;

    TH1F* h_J_m_valleyplusout;
    TH1F* h_J_pt_valleyplusout;
    TH1F* h_J_rho_valleyplusout;
    TH1F* h_J_eta_valleyplusout;
    TH1F* h_J_phi_valleyplusout;

    TH1F* h_J_dphi_valleyplusout;
    TH1F* h_J_dR_valleyplusout;
    TH2F* h_J_phiveta_valleyplusout;
    TH1F* h_smallj_m_valleyplusout;
    TH1F* h_smallj_pt_valleyplusout;
    TH1F* h_smallj_eta_valleyplusout;
    TH1F* h_smallj_phi_valleyplusout;


  TH1F* h_actualMu;
  TH1F* h_averageMu;
  TH1F* h_correctedAndScaledMu;
  TH1F* h_correctedAndScaledAvgMu;

  TH1F* h_dR_selecZp_allsmall_dR1;
  TH1F* h_mJ_selecZp_allsmall_dR1_12;
  TH1F* h_mJ_selecZp_allsmall_dR12_all;

  TH1F* h_dR_selecZp_allsmall_dR1_200250;
  TH1F* h_mJ_selecZp_allsmall_dR1_12_200250;
  TH1F* h_mJ_selecZp_allsmall_dR12_all_200250;
  TH1F* h_dR_selecZp_allsmall_dR1_200450;
  TH1F* h_mJ_selecZp_allsmall_dR1_12_200450;
  TH1F* h_mJ_selecZp_allsmall_dR12_all_200450;
  TH1F* h_dR_selecZp_allsmall_dR1_450all;
  TH1F* h_mJ_selecZp_allsmall_dR1_12_450all;
  TH1F* h_mJ_selecZp_allsmall_dR12_all_450all;

  TH1F* h_mJ_450all;
  TH1F* h_mJ_200450;
  TH1F* h_mJ_200250;

  TH1F* h_overlap_mimmass;
  TH1F* h_overlap_highmass;
  TH1F* h_overlap_drisr;

  TH1F* h_overlap_mimmass_fail;
  TH1F* h_overlap_mimmass_success;

  TH1F* h_N_largeRJets_200250_u;
  TH1F* h_N_largeRJets_200250_w;
  TH1F* h_ptJ_1stlead_200250;
  TH1F* h_ptJ_2ndlead_200250;
  TH1F* h_ptJ_3rdlead_200250;
  TH1F* h_dPhi_200250;
  TH1F* h_dR_200250_Zp3lead;
  TH1F* h_N_smallRJets_200250_u;
  TH1F* h_N_smallRJets_200250_w;
  TH1F* h_ptsj_1stlead_200250;
  TH1F* h_ptsj_2ndlead_200250;
  TH1F* h_ptsj_3rdlead_200250;
  TH1F* h_200250_success_w; 
  TH1F* h_200250_success_u;
  TH1F* h_200250_fail_w;
  TH1F* h_200250_fail_u;
  TH1F* h_overlap_mimmass_200250;
  TH1F* h_overlap_mimmass_200250_fail;
  TH1F* h_overlap_mimmass_200250_success;
  TH1F* h_mJ_200250_success;
  TH1F* h_mJ_200250_fail;
  TH1F* h_mJ_200250_dR75Zprime;


 TH1F* h_overlap_mimmass_tau21ddtcut_100mp_50;
  TH1F* h_overlap_highmass_tau21ddtcut_100mp_50;
  TH1F* h_overlap_drisr_tau21ddtcut_100mp_50;

  TH2F* h_J_tau21ddtvpt;
  TH2F* h_J_tau21vpt;

  TH2F* h_J_tau21ddtvpt_roi;
  TH2F* h_J_tau21vpt_roi;

  TH1F* h_dRISRZprime;
  TH1F* h_success_w;
  TH1F* h_success_u;

  TH1F* h_fail_w;
  TH1F* h_fail_u;

  TH1F* h_mJ_success;
  TH1F* h_mJ_dR75Zprime;
  TH1F* h_mJ_fail;

  TH1F* h_success_ISR_w;
  TH1F* h_success_ISR_u;

  TH1F* h_fail_ISR_w;
  TH1F* h_fail_ISR_u;

  TH1F* h_mISR_success;
  TH1F* h_mISR_dR30ISR;
  TH1F* h_mISR_fail;

  TH1F* h_J_leadsmalljetcheck_DR1_Zprime_fail_no_overlap;
  TH1F* h_J_leadsmalljetcheck_DR1_Zprime_fail_no_overlap_m50;

  TH1F* h_mJ_roi_success;
  TH1F* h_mJ_roi_fail;
  TH1F* h_J_m_tau21cut_100mp_50_success;
  TH1F* h_J_m_tau21ddtcut_100mp_50_success;
  TH1F* h_J_m_tau21cut_100mp_50_fail;
  TH1F* h_J_m_tau21ddtcut_100mp_50_fail;
  TH1F* h_J_m_tau21cut_125mp_50_success;
  TH1F* h_J_m_tau21ddtcut_125mp_50_success;
  TH1F* h_J_m_tau21cut_125mp_50_fail;
  TH1F* h_J_m_tau21ddtcut_125mp_50_fail;
  TH1F* h_J_m_tau21cut_150mp_50_success;
  TH1F* h_J_m_tau21ddtcut_150mp_50_success;
  TH1F* h_J_m_tau21cut_150mp_50_fail;
  TH1F* h_J_m_tau21ddtcut_150mp_50_fail;
  TH1F* h_J_m_tau21cut_175mp_50_success;
  TH1F* h_J_m_tau21ddtcut_175mp_50_success;
  TH1F* h_J_m_tau21cut_175mp_50_fail;
  TH1F* h_J_m_tau21ddtcut_175mp_50_fail;
  TH1F* h_J_m_tau21cut_200mp_50_success;
  TH1F* h_J_m_tau21ddtcut_200mp_50_success;
  TH1F* h_J_m_tau21cut_200mp_50_fail;
  TH1F* h_J_m_tau21ddtcut_200mp_50_fail;
  TH1F* h_J_m_tau21cut_250mp_50_success;
  TH1F* h_J_m_tau21ddtcut_250mp_50_success;
  TH1F* h_J_m_tau21cut_250mp_50_fail;
  TH1F* h_J_m_tau21ddtcut_250mp_50_fail;

  TH1F* h_Zprime_pt;
  TH1F* h_ISR_truth_pt;
  TH1F* h_ISR_truth_m;

TH1F*  h_ptJ_drcut;
TH1F*  h_mJ_drcut;
TH2F*  h_J_mvpt_drcut;

TH2F* h_J_mvpt;

 TH1F* h_m_ROIdiff;
 TH1F* h_m_seleMatch;
 TH1F* h_m_seleDiff;

 TH1F* h_J_leadjetcheck;
 TH1F* h_J_leadjetcheck_DR1;
 TH1F* h_J_leadjetcheck_DR1_m50;

 TH1F* h_J_highmassjetcheck;
 TH1F* h_J_highmassjetcheck_DR1;
 TH1F* h_J_highmassjetcheck_DR1_m50;

 TH1F* h_J_lowtau21jetcheck;
 TH1F* h_J_lowtau21jetcheck_DR1;
 TH1F* h_J_lowtau21jetcheck_DR1_m50;

 TH1F* h_J_lowD2jetcheck;
 TH1F* h_J_lowD2jetcheck_DR1;
 TH1F* h_J_lowD2jetcheck_DR1_m50;

 TH1F* h_J_highdrISRjetcheck;
 TH1F* h_J_highdrISRjetcheck_DR1;
 TH1F* h_J_highdrISRjetcheck_DR1_m50;


 TH1F* h_J_leadsmalljetcheck_Zprime_DR03;

 TH1F* h_J_m_lead_all;
  TH1F* h_J_m_lead;
  TH1F* h_J_m_sublead;
  TH1F* h_J_ptratio_sub_lead_fatjet;

  TH1F* h_ptISRJ;
  TH1F* h_etaISRJ;
  TH1F* h_phiISRJ;
  TH1F* h_mISRJ;

  TH1F* h_ptISR;
  TH1F* h_etaISR;
  TH1F* h_phiISR;
  TH1F* h_mISR;

  TH1F* h_ptJ;
  TH1F* h_etaJ;
  TH1F* h_phiJ;
  TH1F* h_mJ;

  TH1F* h_dEtaISRJ;
  TH1F* h_dPhiISRJ;
  TH1F* h_dRISRJ;

  TH1F* h_yStarISRJ;
  TH1F* h_yBoostISRJ;
  TH1F* h_chiISRJ;

  TH1F* h_asymISRJ;
  TH1F* h_vecasymISRJ;
  TH1F* h_projasymISRJ;
  TH1F* h_simpasymISRJ;

  TH1F *h_ht;
  TH1F *h_ptJisr;
  TH1F *h_ANN_score;
  /*
  TH1F *h_pt1isr;
  TH1F *h_pt1pt0;
  */
  // 2d
  TH2F* h_mJ_vs_isr;
  TH2F* h_mJ_vs_ptJisr;
  TH2F* h_mJ_vs_yStarISRJ;

  //fatjet substructure
  TH1F* h_J_Split12   ; 
  TH1F* h_J_Split23   ;
  TH1F* h_J_tau21;
  TH1F* h_J_tau21ddt_nocuts;
  TH1F* h_J_tau21_roi; 
  TH1F* h_J_D2_roi;
 TH1F* h_J_tau32;
  TH1F* h_J_C2   ; 
  TH1F* h_J_D2   ; 
  TH1F* h_J_NTrimSubjets ;
  TH1F* h_J_nTracks   ; 
  TH1F* h_J_numConstituents   ; 
  //edits 
  TH2F* h_ptvm_rho;
  TH2F* h_J_tau21vrho;
  TH2F* h_J_tau21vrhoprime;
  TH2F* h_J_tau21vrho_roi;
  TH2F* h_J_mvrho;
  TH2F* h_J_D2vrho;
  TH2F*  h_J_tau21ddtvrho;
  TH2F*  h_J_tau21ddtvrho_roi;
  TH1F* h_J_tau21ddt;

  TH2F*  h_J_D2ddtvrho;
  TH1F* h_J_D2ddt;

  TH2F* h_J_tau21vm;
  TH2F* h_J_tau21vm_roi;
  TH2F* h_J_tau21ddtvm_roi;
  TH2F* h_J_D2vm_roi;
  TH2F* h_J_D2ddtvm_roi;

  TH1F* h_J_m_tau21cut_100mp_50;
  TH1F* h_J_m_tau21ddtcut_100mp_50;

  TH2F* h_J_tau21vm_100mp_50;
  TH2F* h_J_tau21ddtvm_100mp_50;

  TH1F* h_J_m_tau21cut_125mp_50;
  TH1F* h_J_m_tau21ddtcut_125mp_50;

  TH2F* h_J_tau21vm_125mp_50;
  TH2F* h_J_tau21ddtvm_125mp_50;

  TH1F* h_J_m_tau21cut_150mp_50;
  TH1F* h_J_m_tau21ddtcut_150mp_50;

  TH2F* h_J_tau21vm_150mp_50;
  TH2F* h_J_tau21ddtvm_150mp_50;

  TH1F* h_J_m_tau21cut_175mp_50;
  TH1F* h_J_m_tau21ddtcut_175mp_50;

  TH2F* h_J_tau21vm_175mp_50;
  TH2F* h_J_tau21ddtvm_175mp_50;


  TH1F* h_J_m_tau21cut_200mp_50;
  TH1F* h_J_m_tau21ddtcut_200mp_50;

  TH2F* h_J_tau21vm_200mp_50;
  TH2F* h_J_tau21ddtvm_200mp_50;

  TH1F* h_J_m_tau21cut_250mp_50;
  TH1F* h_J_m_tau21ddtcut_250mp_50;

  TH2F* h_J_tau21vm_250mp_50;
  TH2F* h_J_tau21ddtvm_250mp_50;

  TH1F* h_J_m_D2cut_100mp_50;
  TH1F* h_J_m_D2ddtcut_100mp_50;

  TH2F* h_J_D2vm_100mp_50;
  TH2F* h_J_D2ddtvm_100mp_50;

  TH1F* h_J_m_D2cut_125mp_50;
  TH1F* h_J_m_D2ddtcut_125mp_50;

  TH2F* h_J_D2vm_125mp_50;
  TH2F* h_J_D2ddtvm_125mp_50;

  TH1F* h_J_m_D2cut_150mp_50;
  TH1F* h_J_m_D2ddtcut_150mp_50;

  TH2F* h_J_D2vm_150mp_50;
  TH2F* h_J_D2ddtvm_150mp_50;

  TH1F* h_J_m_D2cut_200mp_50;
  TH1F* h_J_m_D2ddtcut_200mp_50;

  TH2F* h_J_D2vm_200mp_50;
  TH2F* h_J_D2ddtvm_200mp_50;

  TH1F* h_J_m_D2cut_250mp_50;
  TH1F* h_J_m_D2ddtcut_250mp_50;

  TH2F* h_J_D2vm_250mp_50;
  TH2F* h_J_D2ddtvm_250mp_50;

  TH1F* h_J_m_uncut;
  TH1F* h_ptJ_fatjet;
  TH1F* h_J_pt_uncut_110160;
  TH1F* h_J_pt_uncut_050100;
  TH2F* h_J_tau21vpt_uncut_050100;
  TH2F* h_J_tau21vpt_uncut_110160;
  TH2F* h_J_tau21ddtvpt_uncut_050100;
  TH2F* h_J_tau21ddtvpt_uncut_110160;


  TH1F*  h_J_pt_tau21cut_100mp_50_110160;
  TH1F*  h_J_pt_tau21ddtcut_100mp_50_110160;

  TH1F*  h_J_pt_tau21cut_100mp_50_050100;
  TH1F*  h_J_pt_tau21ddtcut_100mp_50_050100;

  TH2F* h_J_tau21vpt_100mp_50_050100;
  TH2F* h_J_tau21vpt_100mp_50_110160;


  TH2F* h_J_tau21ddtvpt_100mp_50_050100;
  TH2F* h_J_tau21ddtvpt_100mp_50_110160;


  TH1F*  h_J_pt_tau21cut_125mp_50_110160;
  TH1F*  h_J_pt_tau21ddtcut_125mp_50_110160;

  TH1F*  h_J_pt_tau21cut_150mp_50_110160;
  TH1F*  h_J_pt_tau21ddtcut_150mp_50_110160;

  TH1F*  h_J_pt_tau21cut_175mp_50_110160;
  TH1F*  h_J_pt_tau21ddtcut_175mp_50_110160;


  TH1F*  h_J_pt_tau21cut_200mp_50_110160;
  TH1F*  h_J_pt_tau21ddtcut_200mp_50_110160;

  TH1F*  h_J_pt_tau21cut_250mp_50_110160;
  TH1F*  h_J_pt_tau21ddtcut_250mp_50_110160;



  TH1F*  h_J_pt_D2cut_100mp_50_110160;
  TH1F*  h_J_pt_D2ddtcut_100mp_50_110160;

  TH1F*  h_J_pt_D2cut_125mp_50_110160;
  TH1F*  h_J_pt_D2ddtcut_125mp_50_110160;

  TH1F*  h_J_pt_D2cut_150mp_50_110160;
  TH1F*  h_J_pt_D2ddtcut_150mp_50_110160;

  TH1F*  h_J_pt_D2cut_200mp_50_110160;
  TH1F*  h_J_pt_D2ddtcut_200mp_50_110160;

  TH1F*  h_J_pt_D2cut_250mp_50_110160;
  TH1F*  h_J_pt_D2ddtcut_250mp_50_110160;

  //for ratio plots

  TH1F* h_J_m_tau21cut_1;
  TH1F* h_J_m_tau21cut_2;
  TH1F* h_J_m_tau21cut_3;
  TH1F* h_J_m_tau21cut_4;
  TH1F* h_J_m_tau21cut_5;
  TH1F* h_J_m_tau21cut_6;
  TH1F* h_J_m_tau21cut_7;
  TH1F* h_J_m_tau21cut_8;
  TH1F* h_J_m_tau21cut_9;

  TH1F* h_J_m_tau21ddtcut_1;
  TH1F* h_J_m_tau21ddtcut_2;
  TH1F* h_J_m_tau21ddtcut_3;
  TH1F* h_J_m_tau21ddtcut_4;
  TH1F* h_J_m_tau21ddtcut_5;
  TH1F* h_J_m_tau21ddtcut_6;
  TH1F* h_J_m_tau21ddtcut_7;
  TH1F* h_J_m_tau21ddtcut_8;
  TH1F* h_J_m_tau21ddtcut_9;

  TH1F* h_J_rho_uncut;
  TH1F* h_J_rho_uncut_110160;
  TH1F* h_J_rho_tau21cut_100mp_50_110160;
  TH1F* h_J_rho_tau21ddtcut_100mp_50_110160;
    
    TH2F* h_J_tau21vrho_inc;
    TH2F* h_J_tau21vm_inc;
    TH2F* h_J_tau21ddtvm_inc;
    TH1F* h_J_tau21_inc;
    TH2F* h_J_tau21ddtvrho_inc;
    TH1F* h_J_tau21ddt_inc;
    TH2F* h_J_tau21vpt_inc;
    TH2F* h_J_tau21ddtvpt_inc;
    TH2F* h_J_ptvm_inc;
    TH1F* h_J_m_inc;
    TH1F* h_J_pt_inc;
    TH1F* h_J_rho_inc;
    TH1F* h_J_eta_inc;
    TH1F* h_J_phi_inc;
    TH1F* h_J_D2_inc;
    
    TH1F* h_J_dphi_inc;
    TH1F* h_J_dR_inc;
    TH2F* h_J_phiveta_inc;
    TH2F* h_smallj_ptvm_inc;
    TH1F* h_smallj_m_inc;
    TH1F* h_smallj_pt_inc;
    TH1F* h_smallj_eta_inc;
    TH1F* h_smallj_phi_inc;
    
      TH2F* h_J_tau21vrho_inc_roi;
      TH2F* h_J_tau21vm_inc_roi;
      TH2F* h_J_tau21ddtvm_inc_roi;
      TH1F* h_J_tau21_inc_roi;
      TH2F* h_J_tau21ddtvrho_inc_roi;
      TH1F* h_J_tau21ddt_inc_roi;
      TH2F* h_J_tau21vpt_inc_roi;
      TH2F* h_J_tau21ddtvpt_inc_roi;
      TH2F* h_J_ptvm_inc_roi;
      TH1F* h_J_m_inc_roi;
      TH1F* h_J_pt_inc_roi;
      TH1F* h_J_rho_inc_roi;
      TH1F* h_J_eta_inc_roi;
      TH1F* h_J_phi_inc_roi;
    TH1F* h_J_D2_inc_roi;
    
    TH1F* h_J_dphi_inc_roi;
    TH1F* h_J_dR_inc_roi;
    TH2F* h_J_phiveta_inc_roi;
    TH2F* h_smallj_ptvm_inc_roi;
    TH1F* h_smallj_m_inc_roi;
    TH1F* h_smallj_pt_inc_roi;
    TH1F* h_smallj_eta_inc_roi;
    TH1F* h_smallj_phi_inc_roi;

    TH2F* h_J_tau21vrho_binbelowdr12;
    TH2F* h_J_tau21vm_binbelowdr12;
    TH2F* h_J_tau21ddtvm_binbelowdr12;
    TH1F* h_J_tau21_binbelowdr12;
    TH2F* h_J_tau21ddtvrho_binbelowdr12;
    TH1F* h_J_tau21ddt_binbelowdr12;
    TH2F* h_J_tau21vpt_binbelowdr12;
    TH2F* h_J_tau21ddtvpt_binbelowdr12;
    TH2F* h_J_ptvm_binbelowdr12;
    TH1F* h_J_m_binbelowdr12;
    TH1F* h_J_pt_binbelowdr12;
    TH1F* h_J_rho_binbelowdr12;
    TH1F* h_J_eta_binbelowdr12;
    TH1F* h_J_phi_binbelowdr12;
    TH1F* h_J_D2_binbelowdr12;
    
    TH1F* h_J_dphi_binbelowdr12;
    TH1F* h_J_dR_binbelowdr12;
    TH2F* h_J_phiveta_binbelowdr12;
    TH2F* h_smallj_ptvm_binbelowdr12;
    TH1F* h_smallj_m_binbelowdr12;
    TH1F* h_smallj_pt_binbelowdr12;
    TH1F* h_smallj_eta_binbelowdr12;
    TH1F* h_smallj_phi_binbelowdr12;
    
      TH2F* h_J_tau21vrho_binbelowdr12_roi;
      TH2F* h_J_tau21vm_binbelowdr12_roi;
      TH2F* h_J_tau21ddtvm_binbelowdr12_roi;
      TH1F* h_J_tau21_binbelowdr12_roi;
      TH2F* h_J_tau21ddtvrho_binbelowdr12_roi;
      TH1F* h_J_tau21ddt_binbelowdr12_roi;
      TH2F* h_J_tau21vpt_binbelowdr12_roi;
      TH2F* h_J_tau21ddtvpt_binbelowdr12_roi;
      TH2F* h_J_ptvm_binbelowdr12_roi;
      TH1F* h_J_m_binbelowdr12_roi;
      TH1F* h_J_pt_binbelowdr12_roi;
      TH1F* h_J_rho_binbelowdr12_roi;
      TH1F* h_J_eta_binbelowdr12_roi;
      TH1F* h_J_phi_binbelowdr12_roi;
    TH1F* h_J_D2_binbelowdr12_roi;
    
    TH1F* h_J_dphi_binbelowdr12_roi;
    TH1F* h_J_dR_binbelowdr12_roi;
    TH2F* h_J_phiveta_binbelowdr12_roi;
    TH2F* h_smallj_ptvm_binbelowdr12_roi;
    TH1F* h_smallj_m_binbelowdr12_roi;
    TH1F* h_smallj_pt_binbelowdr12_roi;
    TH1F* h_smallj_eta_binbelowdr12_roi;
    TH1F* h_smallj_phi_binbelowdr12_roi;
    
    TH2F* h_J_tau21vrho_binbelowISRpt;
    TH2F* h_J_tau21vm_binbelowISRpt;
    TH2F* h_J_tau21ddtvm_binbelowISRpt;
    TH1F* h_J_tau21_binbelowISRpt;
    TH2F* h_J_tau21ddtvrho_binbelowISRpt;
    TH1F* h_J_tau21ddt_binbelowISRpt;
    TH2F* h_J_tau21vpt_binbelowISRpt;
    TH2F* h_J_tau21ddtvpt_binbelowISRpt;
    TH2F* h_J_ptvm_binbelowISRpt;
    TH1F* h_J_m_binbelowISRpt;
    TH1F* h_J_pt_binbelowISRpt;
    TH1F* h_J_rho_binbelowISRpt;
    TH1F* h_J_eta_binbelowISRpt;
    TH1F* h_J_phi_binbelowISRpt;
    TH1F* h_J_D2_binbelowISRpt;
    
    TH1F* h_J_dphi_binbelowISRpt;
    TH1F* h_J_dR_binbelowISRpt;
    TH2F* h_J_phiveta_binbelowISRpt;
    TH2F* h_smallj_ptvm_binbelowISRpt;
    TH1F* h_smallj_m_binbelowISRpt;
    TH1F* h_smallj_pt_binbelowISRpt;
    TH1F* h_smallj_eta_binbelowISRpt;
    TH1F* h_smallj_phi_binbelowISRpt;
    
      TH2F* h_J_tau21vrho_binbelowISRpt_roi;
      TH2F* h_J_tau21vm_binbelowISRpt_roi;
      TH2F* h_J_tau21ddtvm_binbelowISRpt_roi;
      TH1F* h_J_tau21_binbelowISRpt_roi;
      TH2F* h_J_tau21ddtvrho_binbelowISRpt_roi;
      TH1F* h_J_tau21ddt_binbelowISRpt_roi;
      TH2F* h_J_tau21vpt_binbelowISRpt_roi;
      TH2F* h_J_tau21ddtvpt_binbelowISRpt_roi;
      TH2F* h_J_ptvm_binbelowISRpt_roi;
      TH1F* h_J_m_binbelowISRpt_roi;
      TH1F* h_J_pt_binbelowISRpt_roi;
      TH1F* h_J_rho_binbelowISRpt_roi;
      TH1F* h_J_eta_binbelowISRpt_roi;
      TH1F* h_J_phi_binbelowISRpt_roi;
    TH1F* h_J_D2_binbelowISRpt_roi;

    TH1F* h_J_dphi_binbelowISRpt_roi;
    TH1F* h_J_dR_binbelowISRpt_roi;
    TH2F* h_J_phiveta_binbelowISRpt_roi;
    TH2F* h_smallj_ptvm_binbelowISRpt_roi;
    TH1F* h_smallj_m_binbelowISRpt_roi;
    TH1F* h_smallj_pt_binbelowISRpt_roi;
    TH1F* h_smallj_eta_binbelowISRpt_roi;
    TH1F* h_smallj_phi_binbelowISRpt_roi;
    
      TH2F* h_J_tau21vrho_bin0;
      TH2F* h_J_tau21vm_bin0;
      TH2F* h_J_tau21ddtvm_bin0;
      TH1F* h_J_tau21_bin0;
      TH2F* h_J_tau21ddtvrho_bin0;
      TH1F* h_J_tau21ddt_bin0;
      TH2F* h_J_tau21vpt_bin0;
      TH2F* h_J_tau21ddtvpt_bin0;
      TH2F* h_J_ptvm_bin0;
      TH1F* h_J_m_bin0;
      TH1F* h_J_pt_bin0;
      TH1F* h_J_rho_bin0;
      TH1F* h_J_eta_bin0;
      TH1F* h_J_phi_bin0;
    TH1F* h_J_D2_bin0;
    
    TH1F* h_J_dphi_bin0;
    TH1F* h_J_dR_bin0;
    TH2F* h_J_phiveta_bin0;
    TH2F* h_smallj_ptvm_bin0;
    TH1F* h_smallj_m_bin0;
    TH1F* h_smallj_pt_bin0;
    TH1F* h_smallj_eta_bin0;
    TH1F* h_smallj_phi_bin0;

      TH2F* h_J_tau21vrho_bin0_roi;
      TH2F* h_J_tau21vm_bin0_roi;
      TH2F* h_J_tau21ddtvm_bin0_roi;
      TH1F* h_J_tau21_bin0_roi;
      TH2F* h_J_tau21ddtvrho_bin0_roi;
      TH1F* h_J_tau21ddt_bin0_roi;
      TH2F* h_J_tau21vpt_bin0_roi;
      TH2F* h_J_tau21ddtvpt_bin0_roi;
      TH2F* h_J_ptvm_bin0_roi;
      TH1F* h_J_m_bin0_roi;
      TH1F* h_J_pt_bin0_roi;
      TH1F* h_J_rho_bin0_roi;
      TH1F* h_J_eta_bin0_roi;
      TH1F* h_J_phi_bin0_roi;
    TH1F* h_J_D2_bin0_roi;
    
    TH1F* h_J_dphi_bin0_roi;
    TH1F* h_J_dR_bin0_roi;
    TH2F* h_J_phiveta_bin0_roi;
    TH2F* h_smallj_ptvm_bin0_roi;
    TH1F* h_smallj_m_bin0_roi;
    TH1F* h_smallj_pt_bin0_roi;
    TH1F* h_smallj_eta_bin0_roi;
    TH1F* h_smallj_phi_bin0_roi;
    
  TH2F* h_J_tau21vrho_bin1;
  TH2F* h_J_tau21vm_bin1;
  TH2F* h_J_tau21ddtvm_bin1;
  TH1F* h_J_tau21_bin1;
  TH2F* h_J_tau21ddtvrho_bin1;
  TH1F* h_J_tau21ddt_bin1;
  TH2F* h_J_tau21vpt_bin1;
  TH2F* h_J_tau21ddtvpt_bin1;
  TH2F* h_J_ptvm_bin1;
  TH1F* h_J_m_bin1;
  TH1F* h_J_pt_bin1;
  TH1F* h_J_rho_bin1;
  TH1F* h_J_eta_bin1;
  TH1F* h_J_phi_bin1;
    TH1F* h_J_D2_bin1;
    
    TH1F* h_J_dphi_bin1;
    TH1F* h_J_dR_bin1;
    TH2F* h_J_phiveta_bin1;
    TH2F* h_smallj_ptvm_bin1;
    TH1F* h_smallj_m_bin1;
    TH1F* h_smallj_pt_bin1;
    TH1F* h_smallj_eta_bin1;
    TH1F* h_smallj_phi_bin1;
    
    TH2F* h_J_tau21vrho_bin1_roi;
    TH2F* h_J_tau21vm_bin1_roi;
    TH2F* h_J_tau21ddtvm_bin1_roi;
    TH1F* h_J_tau21_bin1_roi;
    TH2F* h_J_tau21ddtvrho_bin1_roi;
    TH1F* h_J_tau21ddt_bin1_roi;
    TH2F* h_J_tau21vpt_bin1_roi;
    TH2F* h_J_tau21ddtvpt_bin1_roi;
    TH2F* h_J_ptvm_bin1_roi;
    TH1F* h_J_m_bin1_roi;
    TH1F* h_J_pt_bin1_roi;
    TH1F* h_J_rho_bin1_roi;
    TH1F* h_J_eta_bin1_roi;
    TH1F* h_J_phi_bin1_roi;
    TH1F* h_J_D2_bin1_roi;

    TH1F* h_J_dphi_bin1_roi;
    TH1F* h_J_dR_bin1_roi;
    TH2F* h_J_phiveta_bin1_roi;
    TH2F* h_smallj_ptvm_bin1_roi;
    TH1F* h_smallj_m_bin1_roi;
    TH1F* h_smallj_pt_bin1_roi;
    TH1F* h_smallj_eta_bin1_roi;
    TH1F* h_smallj_phi_bin1_roi;
    
    TH2F* h_J_tau21vrho_bin2;
    TH2F* h_J_tau21vm_bin2;
    TH2F* h_J_tau21ddtvm_bin2;
    TH1F* h_J_tau21_bin2;
    TH2F* h_J_tau21ddtvrho_bin2;
    TH1F* h_J_tau21ddt_bin2;
    TH2F* h_J_tau21vpt_bin2;
    TH2F* h_J_tau21ddtvpt_bin2;
    TH2F* h_J_ptvm_bin2;
    TH1F* h_J_m_bin2;
    TH1F* h_J_pt_bin2;
    TH1F* h_J_rho_bin2;
    TH1F* h_J_eta_bin2;
    TH1F* h_J_phi_bin2;
    TH1F* h_J_D2_bin2;
    
    TH1F* h_J_dphi_bin2;
    TH1F* h_J_dR_bin2;
    TH2F* h_J_phiveta_bin2;
    TH2F* h_smallj_ptvm_bin2;
    TH1F* h_smallj_m_bin2;
    TH1F* h_smallj_pt_bin2;
    TH1F* h_smallj_eta_bin2;
    TH1F* h_smallj_phi_bin2;
      
      TH2F* h_J_tau21vrho_bin2_roi;
      TH2F* h_J_tau21vm_bin2_roi;
      TH2F* h_J_tau21ddtvm_bin2_roi;
      TH1F* h_J_tau21_bin2_roi;
      TH2F* h_J_tau21ddtvrho_bin2_roi;
      TH1F* h_J_tau21ddt_bin2_roi;
      TH2F* h_J_tau21vpt_bin2_roi;
      TH2F* h_J_tau21ddtvpt_bin2_roi;
      TH2F* h_J_ptvm_bin2_roi;
      TH1F* h_J_m_bin2_roi;
      TH1F* h_J_pt_bin2_roi;
      TH1F* h_J_rho_bin2_roi;
      TH1F* h_J_eta_bin2_roi;
      TH1F* h_J_phi_bin2_roi;
    TH1F* h_J_D2_bin2_roi;
    
    TH1F* h_J_dphi_bin2_roi;
    TH1F* h_J_dR_bin2_roi;
    TH2F* h_J_phiveta_bin2_roi;
    TH2F* h_smallj_ptvm_bin2_roi;
    TH1F* h_smallj_m_bin2_roi;
    TH1F* h_smallj_pt_bin2_roi;
    TH1F* h_smallj_eta_bin2_roi;
    TH1F* h_smallj_phi_bin2_roi;

    TH2F* h_J_tau21vrho_bin3;
    TH2F* h_J_tau21vm_bin3;
    TH2F* h_J_tau21ddtvm_bin3;
    TH1F* h_J_tau21_bin3;
    TH2F* h_J_tau21ddtvrho_bin3;
    TH1F* h_J_tau21ddt_bin3;
    TH2F* h_J_tau21vpt_bin3;
    TH2F* h_J_tau21ddtvpt_bin3;
    TH2F* h_J_ptvm_bin3;
    TH1F* h_J_m_bin3;
    TH1F* h_J_pt_bin3;
    TH1F* h_J_rho_bin3;
    TH1F* h_J_eta_bin3;
    TH1F* h_J_phi_bin3;
    TH1F* h_J_D2_bin3;
    
    TH1F* h_J_dphi_bin3;
     TH1F* h_J_dR_bin3;
     TH2F* h_J_phiveta_bin3;
     TH2F* h_smallj_ptvm_bin3;
     TH1F* h_smallj_m_bin3;
     TH1F* h_smallj_pt_bin3;
     TH1F* h_smallj_eta_bin3;
     TH1F* h_smallj_phi_bin3;
      
      TH2F* h_J_tau21vrho_bin3_roi;
      TH2F* h_J_tau21vm_bin3_roi;
      TH2F* h_J_tau21ddtvm_bin3_roi;
      TH1F* h_J_tau21_bin3_roi;
      TH2F* h_J_tau21ddtvrho_bin3_roi;
      TH1F* h_J_tau21ddt_bin3_roi;
      TH2F* h_J_tau21vpt_bin3_roi;
      TH2F* h_J_tau21ddtvpt_bin3_roi;
      TH2F* h_J_ptvm_bin3_roi;
      TH1F* h_J_m_bin3_roi;
      TH1F* h_J_pt_bin3_roi;
      TH1F* h_J_rho_bin3_roi;
      TH1F* h_J_eta_bin3_roi;
      TH1F* h_J_phi_bin3_roi;
    TH1F* h_J_D2_bin3_roi;
    
    TH1F* h_J_dphi_bin3_roi;
     TH1F* h_J_dR_bin3_roi;
     TH2F* h_J_phiveta_bin3_roi;
     TH2F* h_smallj_ptvm_bin3_roi;
     TH1F* h_smallj_m_bin3_roi;
     TH1F* h_smallj_pt_bin3_roi;
     TH1F* h_smallj_eta_bin3_roi;
     TH1F* h_smallj_phi_bin3_roi;

    TH2F* h_J_tau21vrho_bin4;
    TH2F* h_J_tau21vm_bin4;
    TH2F* h_J_tau21ddtvm_bin4;
    TH1F* h_J_tau21_bin4;
    TH2F* h_J_tau21ddtvrho_bin4;
    TH1F* h_J_tau21ddt_bin4;
    TH2F* h_J_tau21vpt_bin4;
    TH2F* h_J_tau21ddtvpt_bin4;
    TH2F* h_J_ptvm_bin4;
    TH1F* h_J_m_bin4;
    TH1F* h_J_pt_bin4;
    TH1F* h_J_rho_bin4;
    TH1F* h_J_eta_bin4;
    TH1F* h_J_phi_bin4;
    TH1F* h_J_D2_bin4;
    
    TH1F* h_J_dphi_bin4;
     TH1F* h_J_dR_bin4;
     TH2F* h_J_phiveta_bin4;
     TH2F* h_smallj_ptvm_bin4;
     TH1F* h_smallj_m_bin4;
     TH1F* h_smallj_pt_bin4;
     TH1F* h_smallj_eta_bin4;
     TH1F* h_smallj_phi_bin4;
    
    TH2F* h_J_tau21vrho_bin4_roi;
     TH2F* h_J_tau21vm_bin4_roi;
     TH2F* h_J_tau21ddtvm_bin4_roi;
     TH1F* h_J_tau21_bin4_roi;
     TH2F* h_J_tau21ddtvrho_bin4_roi;
     TH1F* h_J_tau21ddt_bin4_roi;
     TH2F* h_J_tau21vpt_bin4_roi;
     TH2F* h_J_tau21ddtvpt_bin4_roi;
     TH2F* h_J_ptvm_bin4_roi;
     TH1F* h_J_m_bin4_roi;
     TH1F* h_J_pt_bin4_roi;
     TH1F* h_J_rho_bin4_roi;
     TH1F* h_J_eta_bin4_roi;
     TH1F* h_J_phi_bin4_roi;
    TH1F* h_J_D2_bin4_roi;
    
    TH1F* h_J_dphi_bin4_roi;
     TH1F* h_J_dR_bin4_roi;
     TH2F* h_J_phiveta_bin4_roi;
     TH2F* h_smallj_ptvm_bin4_roi;
     TH1F* h_smallj_m_bin4_roi;
     TH1F* h_smallj_pt_bin4_roi;
     TH1F* h_smallj_eta_bin4_roi;
     TH1F* h_smallj_phi_bin4_roi;
    
    TH2F* h_J_tau21vrho_bin5;
    TH2F* h_J_tau21vm_bin5;
    TH2F* h_J_tau21ddtvm_bin5;
    TH1F* h_J_tau21_bin5;
    TH2F* h_J_tau21ddtvrho_bin5;
    TH1F* h_J_tau21ddt_bin5;
    TH2F* h_J_tau21vpt_bin5;
    TH2F* h_J_tau21ddtvpt_bin5;
    TH2F* h_J_ptvm_bin5;
    TH1F* h_J_m_bin5;
    TH1F* h_J_pt_bin5;
    TH1F* h_J_rho_bin5;
    TH1F* h_J_eta_bin5;
    TH1F* h_J_phi_bin5;
    TH1F* h_J_D2_bin5;
      
    TH1F* h_J_dphi_bin5;
     TH1F* h_J_dR_bin5;
     TH2F* h_J_phiveta_bin5;
     TH2F* h_smallj_ptvm_bin5;
     TH1F* h_smallj_m_bin5;
     TH1F* h_smallj_pt_bin5;
     TH1F* h_smallj_eta_bin5;
     TH1F* h_smallj_phi_bin5;
    
      TH2F* h_J_tau21vrho_bin5_roi;
      TH2F* h_J_tau21vm_bin5_roi;
      TH2F* h_J_tau21ddtvm_bin5_roi;
      TH1F* h_J_tau21_bin5_roi;
      TH2F* h_J_tau21ddtvrho_bin5_roi;
      TH1F* h_J_tau21ddt_bin5_roi;
      TH2F* h_J_tau21vpt_bin5_roi;
      TH2F* h_J_tau21ddtvpt_bin5_roi;
      TH2F* h_J_ptvm_bin5_roi;
      TH1F* h_J_m_bin5_roi;
      TH1F* h_J_pt_bin5_roi;
      TH1F* h_J_rho_bin5_roi;
      TH1F* h_J_eta_bin5_roi;
      TH1F* h_J_phi_bin5_roi;
        TH1F* h_J_D2_bin5_roi;

    TH1F* h_J_dphi_bin5_roi;
     TH1F* h_J_dR_bin5_roi;
     TH2F* h_J_phiveta_bin5_roi;
     TH2F* h_smallj_ptvm_bin5_roi;
     TH1F* h_smallj_m_bin5_roi;
     TH1F* h_smallj_pt_bin5_roi;
     TH1F* h_smallj_eta_bin5_roi;
     TH1F* h_smallj_phi_bin5_roi;

  TH1F* h_J_m_tau21cut_bin1;
  TH1F* h_J_m_tau21cut_bin2;
  TH1F* h_J_m_tau21cut_bin3;
  TH1F* h_J_m_tau21cut_bin4;
  TH1F* h_J_m_tau21cut_bin5;
  TH1F* h_J_m_tau21cut_bin6;
  TH1F* h_J_m_tau21cut_bin7;
  TH1F* h_J_m_tau21cut_bin8;
  TH1F* h_J_m_tau21cut_bin9;

  TH1F* h_J_m_tau21ddtv1cut_bin1;
  TH1F* h_J_m_tau21ddtv1cut_bin2;
  TH1F* h_J_m_tau21ddtv1cut_bin3;
  TH1F* h_J_m_tau21ddtv1cut_bin4;
  TH1F* h_J_m_tau21ddtv1cut_bin5;
  TH1F* h_J_m_tau21ddtv1cut_bin6;
  TH1F* h_J_m_tau21ddtv1cut_bin7;
  TH1F* h_J_m_tau21ddtv1cut_bin8;
  TH1F* h_J_m_tau21ddtv1cut_bin9;

  TH1F* h_J_m_tau21ddtv2cut_bin1;
  TH1F* h_J_m_tau21ddtv2cut_bin2;
  TH1F* h_J_m_tau21ddtv2cut_bin3;
  TH1F* h_J_m_tau21ddtv2cut_bin4;
  TH1F* h_J_m_tau21ddtv2cut_bin5;
  TH1F* h_J_m_tau21ddtv2cut_bin6;
  TH1F* h_J_m_tau21ddtv2cut_bin7;
  TH1F* h_J_m_tau21ddtv2cut_bin8;
  TH1F* h_J_m_tau21ddtv2cut_bin9;
  TH1F* h_rankjet_lowest_t21;
  TH1F* h_rankjet_lowest_t21ddt;

  TH2F* h_J_tau21vrhop_250ptcut;
  TH2F* h_J_tau21vrhop_300ptcut; 
  TH2F* h_J_tau21vrhop_350ptcut;
  TH2F* h_J_tau21vrhop_400ptcut;
  TH2F* h_J_tau21vrhop_450ptcut;

  TH2F* h_J_tau21ddtvrhop_250ptcut;
  TH2F* h_J_tau21ddtvrhop_300ptcut;
  TH2F* h_J_tau21ddtvrhop_350ptcut;
  TH2F* h_J_tau21ddtvrhop_400ptcut;
  TH2F* h_J_tau21ddtvrhop_450ptcut;

  TH1F* h_J_tau21_roi_250ptcut;
  TH1F* h_J_tau21_roi_300ptcut;
  TH1F* h_J_tau21_roi_350ptcut;
  TH1F* h_J_tau21_roi_400ptcut;
  TH1F* h_J_tau21_roi_450ptcut;

  TH1F* h_J_tau21ddt_roi_250ptcut;
  TH1F* h_J_tau21ddt_roi_300ptcut;
  TH1F* h_J_tau21ddt_roi_350ptcut;
  TH1F* h_J_tau21ddt_roi_400ptcut;
  TH1F* h_J_tau21ddt_roi_450ptcut;

  TH1F* h_J_m_roi_250ptcut;
  TH1F* h_J_m_roi_300ptcut;
  TH1F* h_J_m_roi_350ptcut;
  TH1F* h_J_m_roi_400ptcut;
  TH1F* h_J_m_roi_450ptcut;

  TH1F* h_J_m_roi_250ptcut_100mp_ddtcut;
  TH1F* h_J_m_roi_300ptcut_100mp_ddtcut;
  TH1F* h_J_m_roi_350ptcut_100mp_ddtcut;
  TH1F* h_J_m_roi_400ptcut_100mp_ddtcut;
  TH1F* h_J_m_roi_450ptcut_100mp_ddtcut;

  TH1F* h_J_m_roi_250ptcut_125mp_ddtcut;
  TH1F* h_J_m_roi_300ptcut_125mp_ddtcut;
  TH1F* h_J_m_roi_350ptcut_125mp_ddtcut;
  TH1F* h_J_m_roi_400ptcut_125mp_ddtcut;
  TH1F* h_J_m_roi_450ptcut_125mp_ddtcut;

  TH1F* h_J_m_roi_250ptcut_150mp_ddtcut;
  TH1F* h_J_m_roi_300ptcut_150mp_ddtcut;
  TH1F* h_J_m_roi_350ptcut_150mp_ddtcut;
  TH1F* h_J_m_roi_400ptcut_150mp_ddtcut;
  TH1F* h_J_m_roi_450ptcut_150mp_ddtcut;

  TH1F* h_J_m_roi_250ptcut_175mp_ddtcut;
  TH1F* h_J_m_roi_300ptcut_175mp_ddtcut;
  TH1F* h_J_m_roi_350ptcut_175mp_ddtcut;
  TH1F* h_J_m_roi_400ptcut_175mp_ddtcut;
  TH1F* h_J_m_roi_450ptcut_175mp_ddtcut;

  TH1F* h_J_m_roi_250ptcut_200mp_ddtcut;
  TH1F* h_J_m_roi_300ptcut_200mp_ddtcut;
  TH1F* h_J_m_roi_350ptcut_200mp_ddtcut;
  TH1F* h_J_m_roi_400ptcut_200mp_ddtcut;
  TH1F* h_J_m_roi_450ptcut_200mp_ddtcut;

  TH1F* h_J_m_roi_250ptcut_250mp_ddtcut;
  TH1F* h_J_m_roi_300ptcut_250mp_ddtcut;
  TH1F* h_J_m_roi_350ptcut_250mp_ddtcut;
  TH1F* h_J_m_roi_400ptcut_250mp_ddtcut;
  TH1F* h_J_m_roi_450ptcut_250mp_ddtcut;

  TH1F* h_J_pt_tau21cut_100mp_50;
  TH1F* h_J_pt_tau21ddtcut_100mp_50;
  TH1F* h_J_pt_tau21cut_125mp_50;
  TH1F* h_J_pt_tau21ddtcut_125mp_50;
  TH1F* h_J_pt_tau21cut_150mp_50;
  TH1F* h_J_pt_tau21ddtcut_150mp_50;
  TH1F* h_J_pt_tau21cut_175mp_50;
  TH1F* h_J_pt_tau21ddtcut_175mp_50;
  TH1F* h_J_pt_tau21cut_200mp_50;
  TH1F* h_J_pt_tau21ddtcut_200mp_50;
  TH1F* h_J_pt_tau21cut_250mp_50;
  TH1F* h_J_pt_tau21ddtcut_250mp_50;

};

#endif
