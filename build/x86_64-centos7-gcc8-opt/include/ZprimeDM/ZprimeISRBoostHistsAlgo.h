#ifndef ZprimeDM_ZprimeISRBoostHistsAlgo_H
#define ZprimeDM_ZprimeISRBoostHistsAlgo_H

// algorithm wrapper
#include <ZprimeDM/ZprimeHistsBaseAlgo.h>
#include <ZprimeDM/ZprimeISRBoostHists.h>
#include <xAODAnaHelpers/FatJetContainer.h>
#include <xAODAnaHelpers/JetContainer.h>
#include <xAODAnaHelpers/PhotonContainer.h>
// our histogramming code                                                                                              
#include <ZprimeDM/DijetISREvent.h>
#include <ZprimeDM/BCIDBugChecker.h>
#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/DijetISRHists.h>

class ZprimeISRBoostHistsAlgo : public ZprimeHistsBaseAlgo
{
public:
  double m_minLeadingJetPt;          // LeadingPtCut - cut on the leading jet pt
  double m_minLeadingFatJetPt;       // LeadingPtCut - cut on the leading jet pt
  bool   m_doFatJets;
  double m_fatjetPtCleaningCut;
  double m_minANNwp;
  double m_mindRjJ;
  double m_yStarISRJCut;
  double m_mJCut;
  int counter_ISRCUTFLOW=0; //!

  std::string m_fatjetDetailStr;
protected:
  virtual void initISRCutflow();
  virtual bool doISRCutflow();
  const xAH::FatJet *m_fatjet;  

public:
  // this is a standard constructor
  ZprimeISRBoostHistsAlgo ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFill(float eventWeight, ZprimeISRBoostHists* systHist=0);
  virtual EL::StatusCode histFinalize ();
private:
  // cutflow
  int m_cf_njets;
  int m_cf_leadingjet;
  int m_cf_cleaning;
  int m_cf_nfatjets;
  int m_cf_leadingfatjet;
  int m_cf_fatjet;
  int m_cf_collimatedfatjet;
  int m_cf_minANNwp;
  int m_cf_mindRjJ;
  int m_cf_trigger;
  int m_cf_ystarisrJ;
  int m_cf_mJ;
  //                                                                  
  // Histograms                                                                          
  ZprimeISRBoostHists* hInclJ;
  ZprimeISRBoostHists *hMJ100_150; //!                                                        
  ZprimeISRBoostHists *hMJ100_200; //!                                                     
  ZprimeISRBoostHists *hMJ150_250; //!                                                               
  ZprimeISRBoostHists *hMJ200_300; //!                                                            
  ZprimeISRBoostHists *hMJ250_350; //!                                                                               
  ZprimeISRBoostHists *hMJ300_400; //!                                                                       
  ZprimeISRBoostHists *hMJ350_450; //!                                                                             
  ZprimeISRBoostHists *hMJ400_500; //!                                                                             
  ZprimeISRBoostHists *hMJ450_550; //!                                                                                
  ZprimeISRBoostHists *hMJ500_600; //!                                                                              
  ZprimeISRBoostHists *hMJ700_800; //!                                             
  //                                                                                                    
  // Histograms with varied b-tag                                                                           
  // weights for systematics                                                                                
  std::vector<ZprimeISRBoostHists*> m_bSFSystHists;


  BCIDBugChecker* m_myBCIDchecker;

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeISRBoostHistsAlgo, 1);
};

#endif
