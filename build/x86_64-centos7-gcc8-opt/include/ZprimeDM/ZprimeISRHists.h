#ifndef ZprimeDM_ZprimeISRHists_H
#define ZprimeDM_ZprimeISRHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODJet/Jet.h>

#include <ZprimeDM/ZprimeHelperClasses.h>
#include <ZprimeDM/DijetISREvent.h>

class ZprimeISRHists : public HistogramManager
{
public:
  ZprimeISRHists(const std::string& name, const std::string& detailStr="");
  virtual ~ZprimeISRHists();

  StatusCode initialize();
  StatusCode execute(const xAH::Particle* reso0, const xAH::Particle* reso1, const xAH::Particle* ISR, float eventWeight);
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  ZprimeHelperClasses::DijetISRInfoSwitch m_infoSwitch;

  // histograms
  TH1F* h_ptISRjj;
  TH1F* h_etaISRjj;
  TH1F* h_phiISRjj;
  TH1F* h_mISRjj;

  TH1F* h_dEtaISRjj;
  TH1F* h_dPhiISRjj;
  TH1F* h_dRISRjj;

  TH1F* h_yStarISRjj;
  TH1F* h_yBoostISRjj;
  TH1F* h_chiISRjj;

  TH1F* h_asymISRjj;
  TH1F* h_vecasymISRjj;
  TH1F* h_projasymISRjj;
  TH1F* h_simpasymISRjj;

  TH1F* h_dRISRclosej;
  TH1F* h_dEtaISRclosej;
  TH1F* h_dPhiISRclosej;
  TH1F* h_dRISRfarj;
  TH1F* h_dEtaISRfarj;
  TH1F* h_dPhiISRfarj;
  TH1F* h_asymISRclosej;

  TH1F* h_dRISRj0;
  TH1F* h_dEtaISRj0;
  TH1F* h_dPhiISRj0;
  TH1F* h_asymISRj0;
  TH1F* h_simpasymISRj0;

  TH1F* h_dRISRj1;
  TH1F* h_dEtaISRj1;
  TH1F* h_dPhiISRj1;
  TH1F* h_asymISRj1;
  TH1F* h_simpasymISRj1;

  TH1F *h_ht;
  TH1F *h_pt0isr;
  TH1F *h_pt1isr;
  TH1F *h_pt1pt0;

  // 2d
  TH2F* h_mjj_vs_isr;
  TH2F* h_mjj_vs_pt0isr;
  TH2F* h_mjj_vs_pt1isr;
  TH2F* h_mjj_vs_yStarISRjj;

  TH2F* h_asymISRj0_vs_dPhiISRj0;
  TH2F* h_asymISRj1_vs_dPhiISRj1;

  TH2F* h_dRISRclose_vs_dRjj;
  TH2F* h_asymISRclosefar_vs_asymISRjj;

};

#endif
