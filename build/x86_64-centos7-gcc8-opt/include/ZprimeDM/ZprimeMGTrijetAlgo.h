#ifndef ZprimeDM_ZprimeMGTrijetAlgo_H
#define ZprimeDM_ZprimeMGTrijetAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>

#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/TruthHists.h>
#include <ZprimeDM/ZprimeMGTrijetHists.h>
#include <ZprimeDM/ZprimeMGTrijet2DHists.h>

#include <sstream>
#include <vector>

using namespace std;


class ZprimeMGTrijetAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
//  bool m_debug;
  std::string m_truthDetailStr;

  // Kinematic selection
  float m_jetPtCut;
  float m_leadJetPtCut;
  float m_ystarCut;
  int   m_ystarJet0, m_ystarJet1;

private:

  //
  // Cutflow
  int m_cf_jets;
  int m_cf_jet0;
  int m_cf_ystar;

  //
  // Histograms
  ZprimeDM::TruthHists *h_jet_reso[3]; //!
  ZprimeDM::TruthHists *h_jet_isr [3]; //!

  ZprimeMGTrijet2DHists *h_hist2d; //!
  ZprimeMGTrijet2DHists *h_hist2d_m12; //!
  ZprimeMGTrijet2DHists *h_hist2d_m13; //!
  ZprimeMGTrijet2DHists *h_hist2d_m23; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  ZprimeMGTrijetAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeMGTrijetAlgo, 1);
};

#endif
