#ifndef ZprimeDM_ZprimeMiniTree_H
#define ZprimeDM_ZprimeMiniTree_H

#include "xAODAnaHelpers/HelpTreeBase.h"
#include "TTree.h"

class ZprimeMiniTree : public HelpTreeBase
{

  private:

    float m_weight;
    float m_weight_corr;
    float m_weight_xs;

    float m_Zprime_pt;
    float m_Zprime_eta;
    float m_Zprime_phi;
    float m_Zprime_m;
    int   m_Zprime_pdg;
  
    bool m_doTruth;

  public:

    ZprimeMiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, bool doTruthInfo = false);
    ~ZprimeMiniTree();

    void AddEventUser   ( const std::string detailStr = "" );
    void FillEventUser  ( const xAOD::EventInfo* eventInfo );
    void ClearEventUser ();
};
#endif
