#ifndef ZprimeDM_ZprimeNtupler_H
#define ZprimeDM_ZprimeNtupler_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>

//algorithm wrapper
#include "ZprimeDM/ZprimeAlgorithm.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include <ZprimeDM/ZprimeNtuplerContainer.h>
#include <ZprimeDM/ZprimeMiniTree.h>

class ZprimeNtupler : public ZprimeAlgorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  std::string m_eventDetailStr;         // event info add to tree
  std::string m_trigDetailStr;          // trigger info add to tree
  std::string m_trigSelForTree;         // triggers requested to be saved in tree
  bool m_truthLevelOnly;                // truthLevelOnly info
  std::string m_inputAlgo;              // input algo for when running systs
  bool m_isMC;                          // Is MC

  std::vector<ZprimeNtuplerContainer> m_containers; // list of containers to save

  // float cutValue;
  int m_eventCounter;     //!

  std::string m_name;
  float m_mcEventWeight;  //!

private:
  //configuration variables
  bool m_cleanPileup;
  std::string m_treeStream;

  int m_cf_cleanPileup;

  //#ifndef __CINT__
  EL::StatusCode getLumiWeights(const xAOD::EventInfo* eventInfo);
  std::map< std::string, ZprimeMiniTree* > m_myTrees; //!
  //#endif // not __CINT__

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!




  // this is a standard constructor
  ZprimeNtupler (const std::string& className="ZprimeNtupler");

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFinalize ();

  bool executeTruthAnalysis( const xAOD::EventInfo* eventInfo, 
			     const std::string& systName = "");

  bool executeAnalysis( const xAOD::EventInfo* eventInfo, 
			const xAOD::VertexContainer* vertices,	
			const std::string& systName = "");

  void AddTree( const std::string& name );

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeNtupler, 1);
};

#endif
