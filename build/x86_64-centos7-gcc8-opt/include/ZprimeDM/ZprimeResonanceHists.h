#ifndef ZprimeDM_ZprimeResonanceHists_H
#define ZprimeDM_ZprimeResonanceHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODJet/Jet.h>

#include <ZprimeDM/ZprimeHelperClasses.h>
#include <ZprimeDM/DijetISREvent.h>

class ZprimeResonanceHists : public HistogramManager
{
public:
  ZprimeResonanceHists(const std::string& name, const std::string& detailStr="");
  virtual ~ZprimeResonanceHists();

  StatusCode initialize();
  StatusCode execute(const xAH::Jet* reso0, const xAH::Jet* reso1, float eventWeight) ;
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  ZprimeHelperClasses::DijetISRInfoSwitch m_infoSwitch;

  //basic
  TH1F* h_ptjj;
  TH1F* h_ptjj_l;
  TH1F* h_etajj;
  TH1F* h_phijj;
  TH1F* h_mjj;
  TH1F* h_mjj_var;
  TH1F* h_mjj_fine;

  TH1F* h_dEtajj;
  TH1F* h_dPhijj;
  TH1F* h_dRjj;

  TH1F* h_yStarjj;
  TH1F* h_yBoostjj;
  TH1F* h_chijj;

  TH1F* h_asymjj;
  TH1F* h_vecasymjj;
  TH1F* h_projasymjj;

  TH1F* h_chargediff;
  TH1F* h_chargesum;

  // 2d
  TH2F* h_mjj_vs_reso0;
  TH2F* h_mjj_vs_reso1;

  TH2F* h_mjj_vs_dEtajj;
  TH2F* h_mjj_vs_dPhijj;
  TH2F* h_mjj_vs_dRjj;

  TH2F* h_dPhijj_vs_dEtajj;
};

#endif
