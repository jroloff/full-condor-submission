import ROOT

def selection(c,
              mc, prw,
              trkjet, fjpt, skipDoubleTrkJet, allowSingleTrkJet, skipBoost, skipVRcontain, d2sort, fatjetidx, ttbar, hidx, hpt, hm, nbtag, nbext, wp, lnot,
              JMRuncert=ROOT.JMRUncertaintyAlgo.NOMINAL,
              commonsel={}, prefix='hbbisr', nameSuffix=''):
    if skipDoubleTrkJet and not allowSingleTrkJet: return # Need at least one track jet
    wp_enum=getattr(ROOT.xAH.Jet,wp)
    if nbtag==0 and not nbext and wp_enum!=ROOT.xAH.Jet.None: return # b-tagging WP doesn't make sense if require no b-tags
    if nbtag==0 and nbext and wp_enum==ROOT.xAH.Jet.None: return # exact btagging does not make sense if no WP is specified
    if nbtag>0 and wp_enum==ROOT.xAH.Jet.None: return # need a b-tagging WP defined if want b-tags
    if hpt=='fjpt': hpt=fjpt

    name=[prefix]
    if not prw and mc: name.append('noprw')
    name.append(trkjet)
    doTrigger=fjpt>450
    if fjpt>0:
        name.append('fj0pt%d'%fjpt)
    name.append('fj1pt250')
    if not skipDoubleTrkJet:
        name.append('tjet2{}'.format('al1' if allowSingleTrkJet else ''))
    elif skipDoubleTrkJet and allowSingleTrkJet:
        name.append('tjet1ext')
    if skipBoost: name.append('skipBoost')
    if skipVRcontain: name.append('skipVRcontain')

    # Higgs candidate
    if d2sort: name.append('d2sort')
    else: name.append('ptsort')
    name[-1]+='%d'%fatjetidx

    # ttbar
    if ttbar=='CR':
        name.append('ttbarCR')
    elif ttbar=='veto':
        name.append('vetottbarCR')

    # Hcand kinematics
    if(hidx>=0):
        name.append('hidx%d'%hidx)
    if(hpt>0):
        name.append('hpt%d'%hpt)
    if(hm>0):
        name.append('hm%d'%hm)

    # Hcand b-tagging
    name.append('n%sbtag%din2%s'%('' if not lnot else 'lnot',nbtag,'ext' if nbext else ''))
    if nbtag>0 or nbext: 
        name.append(wp)

    # Prepare object
    namestr='_'.join(name)+nameSuffix
    config=commonsel.copy()
    config.update({"m_name"              : namestr,
                   "m_JMRuncert"         : JMRuncert,
                   "m_doPUReweight"      : prw,
                   "m_trkJet"            : trkjet,
                   "m_doTrigger"         : doTrigger,
                   "m_fatjet0PtCut"      : fjpt,
                   "m_fatjet1PtCut"      : 250,
                   "m_skipDoubleTrkJet"  : skipDoubleTrkJet,
                   "m_allowSingleTrkJet" : allowSingleTrkJet,
                   "m_skipBoostCut"      : skipBoost,
                   "m_skipVRContainCut"  : skipVRcontain,
                   "m_sortD2"            : d2sort,
                   "m_HcandIdx"          : fatjetidx,
                   "m_bTagWP"            : wp_enum,
                   "m_looseNoTight"      : lnot,
                   "m_nBTagsCut"         : nbtag,
                   "m_nBTagsCutExact"    : nbext,
                   "m_HcandIdxCut"       : hidx,
                   "m_HcandPtCut"        : hpt,
                   "m_HcandMCut"         : hm,
                   'm_doTTbarMuonCR'     : ttbar=='CR',
                   'm_vetoTTbarMuonCR'   : ttbar=='veto'
                   })
    print(config)
    c.algorithm("HbbISRTJetHistsAlgo", config )

