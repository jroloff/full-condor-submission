#!/usr/bin/env python

import sys
import time
import tempfile
import getpass
import subprocess
import os
import argparse
import itertools

commonflags={}
commonflags['bkg']='--isMC'
commonflags['sig']='--isMC'
commonflags['fastsim']='--isMC --isAFII'

#
# @brief Produce a list of filelist types in an option dictionary.
#
# Loops over all keys to find ones matching the format filelists_TYPENAME.
#
# @param option the options dictionary
#
# @return list of TYPENAME found
def parse_types(option):
    types=[]
    for optionkey in option.keys():
        if optionkey.startswith('filelists_'):
            types.append(optionkey[10:])
    return types

#
# @brief Create an argparse object with settings corresponding to available filelists
#
# The list of available filelists is created from an options structure. The options 
# structure contains keys, called KEY, that define sets of filelists. The value, called option, 
# is a dictionary indicating how to associate a set of filelists with an ntupler configuration. 
# There are special keys of the option dictionary formatted as filelists_TYPENAME. They specify
# a list of filelists that should be grouped under a single TYPENAME.
#
# Each KEY is added as an argument --KEY to argpase with an option being a comma-separated list
# of TYPENAMES. A special TYPENAME called "all" is possible to enable all TYPENAMES. There is
# no default value.
#
# Other options (--pretend, drivers, --campaigns...) are also added
#
# @parser options dictionary containing the different possible filelists
#
# @return argparse object
def create_argparser(options=None):
    #
    # Determine some useful defaults

    # If you want the output to go to a particular disk, set it here.
    # Set default localgroupdisk to look for
    # Add your username here if you like :)
    # If you add a localGroupDisk from the command line it will
    # overwrite the one here.
    destinations={}
    username = getpass.getuser()
    destinations['kpachal']="CA-SFU-T2_LOCALGROUPDISK"
    destinations['ecorriga']="SE-SNIC-T2_LUND_LOCALGROUPDISK"
    destinations['kkrizka']="MWT2_UC_LOCALGROUPDISK"

    #
    # Create the parser object
    parser = argparse.ArgumentParser(description='Launch the ntuple making process.')
    parser.add_argument('-p','--pretend',   action='store_true', help='Print only submission commands.')
    parser.add_argument('-c','--campaigns', type=str, metavar='mc16a,mc16d,mc16e', default='mc16a,mc16d,mc16e', help='Comma separated list of campaigns to run over.')
    parser.add_argument('-D', '--doDivision', nargs=2, type=int, metavar='N, Ntot', help='Break each task into subtasks of N events using xAH\'s --skip and --nevents. Ntot is the total number of events. Only works for direct mode!')

    # options for running
    if options!=None:
        for key,config in options.items():
            types=['all']
            for configkey in config.keys():
                if configkey.startswith('filelists_'):
                    types.append(configkey[10:])
            parser.add_argument('--{0}'.format(key), metavar=','.join(types), help='Run over listed {config} configurations.'.format(config=key))

    # Specify where to run jobs
    drivers_parser = parser.add_subparsers(prog='runntupler.py', title='drivers', dest='driver', description='specify where to run jobs')

    direct = drivers_parser.add_parser('direct',help='Run your jobs locally.')

    grid = drivers_parser.add_parser('grid',help='Run your jobs on the grid.')
    grid.add_argument('--destination', type=str, required=False, default=destinations.get(username,None))
    grid.add_argument('--tag', type=str, required=True)

    slurm = drivers_parser.add_parser('slurm',help='Run your jobs on SLURM.')

    return parser

#
# @brief Runs the ntupler over all filelists defined by options.
#
# Uses the output of create_argparser to 
# * determine what typenames to enable
# * determine what driver to use (and any driver specific settings)
# * determine campaigns to try
#
# The list of submitted filelists is a cartesian product of typenames
# enabled via arguments and the campaigns specified. The "None" campaign
# is always included. See generate_xAH_command for how filelists and
# campaigns are associated.
#
# Note if a filelist does not exist in a campaign, it is skipped. So
# specifying mc16a for a data filelist is OK.
# 
# @param options List of options containing filelists to run over
# @param package The package name that contains the filelists
def runntupler(options,package='ZprimeDM'):
    parser = create_argparser(options)
    args = parser.parse_args()

    #make sure Ntot > N for --doDivision
    if args.doDivision and args.doDivision[0] > args.doDivision[1]:
        print "argument ERROR: --doDivision: please ensure Ntot >= N."
        sys.exit(1)
        
    if args.doDivision and args.driver != 'direct':
        print "argument WARNING: --doDivision only affects direct jobs"


    campaigns=[None]+args.campaigns.split(',')
    
    #
    # Organization
    user = getpass.getuser()
    tag = time.strftime("%Y%m%d")


    #
    # Prepare output directory
    if args.driver=="grid":
        datadir = tempfile.mkdtemp(suffix="{0}_{1}_{2}_{3}".format(user,package,tag,args.driver))
    else :
        datadir = os.environ.get('DATADIR',os.getcwd())

    #
    # Prepare commands for running
    commands=[]

    enablealloptions=all([getattr(args,key)==None for key in options.keys()])
    for key,option in options.items():
        print("Beginning option {0}".format(key))

        # Skip any options diabled via argument
        enabled=getattr(args,key)
        if not enabled and not enablealloptions:
            print('\tskip...')
            continue

        # Get list of enabled types
        enabled=enabled.split(',') if not enablealloptions else ['all']
        types=parse_types(option)
        if 'all' not in enabled:
            enabled = filter(lambda x: x in enabled, types)
        else:
            enabled = types

        # Loop over types and prepare a command for each
        for typename,campaign in itertools.product(enabled,campaigns):

            # Job subdivision (--doDivision)
            if args.doDivision:
                N = args.doDivision[0] #this many events in each chunk
                Ntot = args.doDivision[1] #this many in total

                #how many subchunks do we have to do
                if Ntot%N==0:  
                    imax = Ntot//N
                else: 
                    imax = Ntot//N+1
                
                #do the subdivision
                for i in range(0, imax):

                    Nskip = (i)*N #skip this many in this subjob 
                    Ndo = N #do this many

                    divtag = str(Nskip)+'to'+str(Nskip+N) #tag to allow submissions of jobs with otherwise identical params

                    command=generate_xAH_command(option,typename,campaign,package,datadir,args.driver,args,Nskip,Ndo,divtag)
                    
                    if command==None: print('\t\tskip %s in campaign %s'%(typename,campaign))
                    else: commands.append(command)

            #without --doDivision, proceed as normally:
            else:
                command=generate_xAH_command(option,typename,campaign,package,datadir,args.driver,args)
                if command==None: print('\t\tskip %s in campaign %s'%(typename,campaign))
                else: commands.append(command)


    #
    # Run!
    for command in commands:
        print(command+"\n")
        if not args.pretend: subprocess.check_call(command,shell=True)
        
    #
    # Clean-up
    if args.driver=='grid': subprocess.check_call("rm -rf {0}".format(datadir),shell=True)

#
# @brief Generate xAH_run command that can be used to submit a job.
#
# Generates an xAH_run command for a given set of filelists in an option dictionary
# specified by typename. The filelists are searched for inside campaign. If a
# filelist does not exist, then it is skipped. The following path is assumed for a
# filelist:
# '../{package}/filelists/{campaign}/{filelist}.{derivation}.list'
# If campaign of "None" is specified, then {campaign} is set to ''.
#
# The jobs are submitted from a directory inside datadir. The name
# of the directory is 'OUT_{option}_{typename}_{campaign}_ntuple'. The
# '_{campaign}' is not included in case of None campaign.
#
# For the grid driver, the output sample name follows the format
#  user.%nickname%.{campaign}.%in:name[2]%.%in:name[3]%.{selection}.{tag}/
# with {tag} taken from argument to the grid driver, selection taken from the
# option dictionary and campaign being the campaign. In case of None campaign,
# %in:name[1]% is used.
#
# @param option The option dictionary with filelist information
# @param typename The typename of the filelist set in the option dictionary to submit
# @param campaign Campaign for which to search the filelist in
# @param package Package containing the filelists
# @param datadir Location in which the submission directory will be create
# @param driver EventLoop driver to use
# @param args Settings to configure driver
#
# @return string containing the xAH_run and the corresponding options. In case
# of a problem (no matching filelists), None is returned.
#
def generate_xAH_command(option,typename,campaign,package,datadir,driver,args,Nskip=0,Nevts=0,divtag=''):
    isPDSF = ('pdsf' in os.uname()[1] or 'cori' in os.uname()[1]) and driver!='grid'

    #
    # Declare options for running with different drivers
    if driver=="direct":
#        runcode ="--nevents=10000 direct"
        runcode ="--nevents=1000 direct"
#        runcode ="--nevents=5 direct"
#        runcode ="direct"
    elif driver=="grid":
        dest = ""
        if args.destination!=None:
            dest += "--optGridDestSE={0} ".format(args.destination)
        outputsample='user.%nickname%.{campaign}.%in:name[2]%.%in:name[3]%.SELECTION.{tag}/'.format(campaign=campaign if campaign!=None else '%in:name[1]%',tag=args.tag)
        runcode ="prun {dest} --optGridOutputSampleName='{outputsample}'".format(dest=dest,outputsample=outputsample)
        #runcode ="prun {dest} --optGridOutputSampleName='{outputsample}' --optSubmitFlags='--allowTaskDuplication'".format(dest=dest,outputsample=outputsample)
    elif driver=="slurm":
        pdsfslurm='--optBatchSharedFileSystem=1 --optSlurmAccount="atlas" --optSlurmPartition="long" --optSlurmRunTime="48:00:00" --optSlurmExtraConfigLines="#SBATCH --image=custom:pdsf-chos-sl64:v4 --export=NONE" --optSlurmWrapperExec="export TMPDIR=\${SLURM_TMP}; hostname; shifter --volume=/global/project:/project --volume=/global/projecta:/projecta /bin/bash " '
        runcode ="slurm --optBatchWait {pdsf}".format(pdsf=pdsfslurm)
    else :
        print("Unrecognized mode: {0}".format(driver))
        return

    #
    # Extra flags to pass to config flags
    # Prepare commands for running
    extraOptions = []
    if 'extraflags' in option.keys() :
        extraOptions+=option['extraflags']

    # Add campaign to the extra options
    # This is because xAH default for run numbers is still mc16c and we are using mc16d now.
    extraOptions.append('--mcCampaign={0}'.format(campaign))

    # Add whether to do systematics to extra options:
    # true for signal, false otherwise
    if 'sig' in typename and not "TRUTH" in option['derivation']:
        extraOptions.append('--doSyst')

    standardConfig = "../{package}/data/config_{selection}_ntuple.py".format(selection=option['selection'],
                                package=package)
    if "TRUTH" in option['derivation'] :
        standardConfig = "../{package}/data/config_{selection}_ntuple_truth.py".format(selection=option['selection'],
                                package=package)

    commandFormat = "xAH_run.py --config {config} --files {files} --inputList {flags} --submitDir {datadir}/{outname} --extraOptions='{extraOptions}' --force {runcode} "

    #
    # Prepare the filelist
    filelistdir='../'+package+'/filelists' if campaign==None else '../'+package+'/filelists/'+campaign
    filelists=['{filelistdir}/{filelist}.{derivation}.list'.format(filelistdir=filelistdir,
                                                                   filelist=filelist,
                                                                   derivation=option['derivation'])
               for filelist in option['filelists_'+typename]]

    
    # For any additional options I add down the road, use "." as separator. ";" crashes and I
    # want to reserve "," for lists of options (e.g. mc16a,mc16d)

    # Remove non-existent files (not found in the given campaign)
    filelists=[filelist for filelist in filelists if os.path.exists(filelist)]
    if not filelists: return

    if isPDSF: # PDSF hack
        pdsffilelists=[]
        for filelist in filelists:
            fh=open(filelist)
            for line in fh:
                line=line.strip()
                if line=='': continue
                pdsffilelists.append('../filelists/'+line+'.txt')
            fh.close()
        filelists=pdsffilelists

    files=' '.join(filelists)
    flags=commonflags.get(typename,'')
    if not isPDSF: flags+=' --inputRucio'

    if args.doDivision:
        flags+=' --skip {0} --nevents {1} '.format(Nskip,Nevts)

    outname='OUT_{option}_{typename}{campaign}_ntuple{div}'.format(option=option['selection'],
                                                              typename=typename,
                                                              campaign='_'+campaign if campaign!=None else '',
                                                              div = divtag)
    if "TRUTH" in option['derivation'] :
      outname = outname.replace("ntuple","truth_ntuple")
    runcode2=runcode.replace('SELECTION',option['selection'])

    return commandFormat.format(config=standardConfig,
                                files=files,
                                flags=flags,
                                datadir=datadir,
                                outname=outname,
                                extraOptions=' '.join(extraOptions),
                                runcode=runcode2
                                )
