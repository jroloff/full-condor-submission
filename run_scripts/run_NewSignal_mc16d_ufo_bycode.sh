dir=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/FullRun2_Boosted/dijet/mc16d_dijet_signal
user=group.phys-exotics.
outdir=/eos/user/n/nlopezca/hbbisr_projv2/outputs_afs/mc16d_ufo_new_sg_jetJET_v2isrdrhighpt_400pt_v117_correct_ddtcalc_mcdata
configdir=/eos/user/n/nlopezca/hbbisr_projv2/ZprimeDM/data

for file in ${dir}/${user}*mc16d*
do
    STR=$file
    SUBSTR=$(echo $file | cut -d'.' -f 5)
    echo $SUBSTR

    ls $file/* > tmplist.list

    xAH_run.py --files tmplist.list --inputList -f --submitDir $outdir/OUT_Signal_$SUBSTR --config $configdir/config_jetJet_drloop.py --treeName outTree --isMC direct

    rm tmplist.list
    #P= $(( ${#dir} + ${#user} ))
    #echo $P
    #echo $file | cut -c$P-(( $P + 6 ))
    echo " -------------"
done

#reweigth histograms by DISD code                                                                      
echo "Starting reweighting" 
for file in $outdir/OUT_Signal*
do
    applyMiniTreeEventCountWeight.py $file --extra 45000
done

#merge histograms          
#DO NOT MERGE DIFFERENT SIGNALS!
#echo "Merge output"  
#hadd $outdir/hist-Signal_dijet.dijet.NTUP.root $outdir/OUT_Signal_*/hist-*.root

