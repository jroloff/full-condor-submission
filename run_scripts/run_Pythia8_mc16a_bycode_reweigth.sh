#reweigth histograms by DISD code
for file in /eos/user/d/dmelini/ISRZprime/mc16a/ByCode/OUT_Pythia8* 
do
    applyMiniTreeEventCountWeight.py $file
done

#merge histograms
hadd /eos/user/d/dmelini/ISRZprime/mc16a/ByCode/hist-reweighted-merged.root /eos/user/d/dmelini/ISRZprime/mc16a/ByCode/OUT_Pythia8_3647*/hist-tmplist.root
