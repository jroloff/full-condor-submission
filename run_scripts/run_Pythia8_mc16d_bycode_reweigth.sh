#reweigth histograms by DISD code
for file in /eos/user/n/nlopezca/hbbisr_projv2/outputs/mc16d_bg_v5_m300cut/OUT_Pythia8*
do
    applyMiniTreeEventCountWeight.py $file
done

#merge histograms
hadd /eos/user/n/nlopezca/hbbisr_projv2/outputs/mc16d_bg_v5_m300cut/hist-reweighted-merged.root /eos/user/n/nlopezca/hbbisr_projv2/outputs/mc16d_bg_v5_m300cut/OUT_Pythia8_3647*/hist-tmplist.root
