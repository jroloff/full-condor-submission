dir=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/FullRun2_Boosted/mc16a_dijet
user=user.cdelitzs.
outdir=/eos/user/d/dmelini/ISRZprime/mc16a/ByCode
configdir=/afs/cern.ch/work/d/dmelini/ISR_Dijet/DijetISRCode_Organizer/ZprimeDM/data

for file in ${dir}/${user}*Sherpa* 
do
    STR=$file
    SUBSTR=$(echo $file | cut -d'.' -f 3)
    echo ${outdir}

    ls $file/* > tmplist.list


    xAH_run.py --files tmplist.list --inputList -f --submitDir ${outdir}/OUT_Sherpa_$SUBSTR --config ${configdir}/config_jetJet.py  --treeName outTree --isMC  direct

    rm tmplist.list
    echo " -------------"
done

#reweigth histograms by DISD code                                                                      
echo "Starting reweighting" 
for file in ${outdir}/OUT_Sherpa*
do
    applyMiniTreeEventCountWeight.py $file
done

#merge histograms          
echo "Merge output"  
hadd ${outdir}/hist-Sherpa_dijet.dijet.NTUP.root ${outdir}/OUT_Sherpa_*/hist-*.root

