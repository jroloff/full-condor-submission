dir=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/FullRun2_Boosted/mc16d_dijet
user=user.cdelitzs.
outdir=/eos/user/n/nlopezca/hbbisr_projv2/outputs/mc16_Wjets_isrdrmasscut5th_v54_ptbins
configdir=/eos/user/n/nlopezca/hbbisr_projv2/ZprimeDM/data

for file in ${dir}/${user}*Sherpa*Wqq* 
do
    STR=$file
    SUBSTR=$(echo $file | cut -d'.' -f 3)
    echo $SUBSTR

    ls $file/* > tmplist.list

    xAH_run.py --files tmplist.list --inputList -f --submitDir $outdir/OUT_Sherpa_$SUBSTR --config $configdir/config_jetJet_drloop.py --treeName outTree --isMC  direct

    rm tmplist.list
    #P= $(( ${#dir} + ${#user} ))
    #echo $P
    #echo $file | cut -c$P-(( $P + 6 ))
    echo " -------------"
done

#reweigth histograms by DISD code                                                                      
echo "Starting reweighting" 
for file in $outdir/OUT_Sherpa*
do
    applyMiniTreeEventCountWeight.py $file
done

#merge histograms          
echo "Merge output"  
hadd $outdir/hist-Sherpa_dijet.dijet.NTUP.root $outdir/OUT_Sherpa_*/hist-*.root

