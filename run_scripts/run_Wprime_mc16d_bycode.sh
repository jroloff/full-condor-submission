dir=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/FullRun2_Boosted/mc16d_wprime
user=user.cdelitzs.
outdir=/eos/user/d/dmelini/ISRZprime/mc16d/ByCode
configdir=/afs/cern.ch/work/d/dmelini/ISR_Dijet/DijetISRCode_Organizer/ZprimeDM/data

for file in ${dir}/${user}*WprimeWZ* 
do
    STR=$file
    SUBSTR=$(echo $file | cut -d'.' -f 3)
    echo $SUBSTR

    ls $file/* > tmplist.list

    xAH_run.py --files tmplist.list --inputList -f --submitDir $outdir/OUT_WprimeWZ_$SUBSTR --config $configdir/config_jetJet.py --treeName outTree --isMC  direct

    rm tmplist.list
    #P= $(( ${#dir} + ${#user} ))
    #echo $P
    #echo $file | cut -c$P-(( $P + 6 ))
    echo " -------------"
done

#reweigth histograms by DISD code                                                                      
echo "Starting reweighting" 
for file in $outdir/OUT_WprimeWZ*
do
    applyMiniTreeEventCountWeight.py $file
done

#merge histograms          
#DO NOT MERGE DIFFERENT SIGNALS!
#echo "Merge output"  
#hadd $outdir/hist-Signal_dijet.dijet.NTUP.root $outdir/OUT_Signal_*/hist-*.root

