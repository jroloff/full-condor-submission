dir=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/FullRun2_Boosted/data15_dijet
user=user.cdelitzs.
outdir=/eos/user/n/nlopezca/hbbisr_projv2/outputs/data15_bg_jetJET_v2isrdrhighpt_400pt_v108_block_rescale_algo
configdir=/eos/user/n/nlopezca/hbbisr_projv2/ZprimeDM/data

for file in ${dir}/${user}data15*
do
    STR=$file
    SUBSTR=$(echo $file | cut -d'.' -f 4)
    echo $SUBSTR

    ls $file/* > tmplist.list

    xAH_run.py --files tmplist.list --inputList -f --submitDir $outdir/OUT_data15_$SUBSTR --config $configdir/config_jetJet_drloop.py --treeName outTree --isMC  direct

    rm tmplist.list
    #P= $(( ${#dir} + ${#user} ))
    #echo $P
    #echo $file | cut -c$P-(( $P + 6 ))
    echo " -------------"
done

#reweigth histograms by DISD code            

#merge histograms          
echo "Merge output"  
hadd $outdir/hist-data15_dijet.dijet.NTUP.root $outdir/OUT_data15_*/hist-*.root

